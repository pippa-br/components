import styles from "./styles.module.scss";
import { useRouter } from "next/router";
import { currencyMask } from "../../../../core/v2/util/mask";
import { firstLevel1Variant, productInstallments, productPrice } from "../../../../core/v2/product/product.util";

// firstLevel1Variant: Caso de produtos relacionados
export function ProductItem({ sale, product }: any) {
	const router = useRouter();
	const query = useRouter().query;

	return (
		<div
			className={styles.container}
			onClick={() => router.push(`/produto/${product?.slug}?color=${product?.levelVariant?.label || firstLevel1Variant(product).label}`)}
		>
			<div className={styles.productImage}>
				{sale && <span>Promo</span>}
				<img
					src={
						product?.images?.data?.[`${query.color}`]?.images[0]?._url ||
						product?.images?.data?.[product?.variant[0]?.items[0]?.value]
							?.images[0]?._url
					}
					alt={product?.name}
				/>
			</div>
			<p className={styles.productName}>{product?.name}</p>

			<p className={styles.productInstallments}>
				{productInstallments(product, 6, 90)}
			</p>

			<div className={styles.productColor}>
				{product?.variant[0]?.items?.map((color: any) => (
					<p key={color.id}>{color?.label}</p>
				))}
			</div>

			{product?.priceTable && (
				<p className={styles.productPrice}>
					R$ {productPrice(product)},00
				</p>
			)}
		</div>
	);
}
