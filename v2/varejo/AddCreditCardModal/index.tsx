import styles from "./styles.module.scss";
import { useRouter } from "next/router";
import InputMask from "react-input-mask";
import { useForm } from "react-hook-form";
import {
  AiOutlineExclamationCircle,
  AiOutlineCloseCircle,
} from "react-icons/ai";
import { useState } from "react";
import { toast } from "react-hot-toast";
import ccValid from "card-validator";
import { validateCpf } from "../../../../core/v2/util/validate";
import { buscaCep } from "../../../../core/v2/util/util";
import { AnimatedLoading } from "../../../v1/varejo/AnimatedLoading";

type AddressProps = {
  bairro: string;
  cep: string;
  complemento: string;
  localidade: string;
  logradouro: string;
  uf: string;
};

type CreditCardProps = {
  cardnumber: string;
  owner: string;
  cvv: string;
  expirydate: string;
  _cpf: string;
  address: {
    city: string;
    complement: string;
    district: string;
    housenumber: string;
    state: string;
    street: string;
    zipcode: string;
  };
  referencePath?: string;
};

export const AddCreditCardModal = ({ setOpenModal, userData }: any) => {
  const [address, setAddress] = useState<AddressProps>();
  const [loadingUserCreditCard, setLoadingUserCreditCard] = useState(false);

  const router = useRouter();

  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm();

  const onSubmit = async (data: any) => {
    const newData = {
      data: {
        client: {
          referencePath: userData.referencePath,
        },
        creditCard: {
          cardnumber: data.cardnumber,
          owner: data.owner,
          cvv: data.cvv,
          expirydate: data.expirydate,
          docType: {
            value: "cpf",
            label: "CPF",
            id: "cpf",
            type: "individual",
          },
          _cpf: data.cpf,
          address: {
            zipcode: data.cep,
            street: data.street,
            housenumber: data.housenumber,
            complement: data.complement,
            district: data.district,
            city: data.city,
            state: data.state,
            country: { id: "br", label: "Brasil", value: "br" },
          },
        },
      },
    };

    await setLoadingUserCreditCard(true);
    // await creditCardPlugin.addDocumentFront(newData);
    router.reload();
    toast.success("Cartão salvo com sucesso!");
  };

  return (
    <div className={styles.addAddressModal} onClick={() => setOpenModal(false)}>
      <div className={styles.content} onClick={(e) => e.stopPropagation()}>
        <AiOutlineCloseCircle onClick={() => setOpenModal(false)} />
        <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
          <div className={styles.formItem}>
            <label>Nome do titular do cartão</label>
            <input
              type="text"
              autoComplete="new-off"
              placeholder="Ex: José da Silva"
              {...register("owner", { required: "Este campo é obrigatório!" })}
            />
            {errors.owner && (
              <span className={styles.errorMessage}>
                <AiOutlineExclamationCircle />
                {errors.owner?.message}
              </span>
            )}
          </div>
          <div className={styles.formItem}>
            <label>CPF</label>
            <InputMask
              {...register("cpf", {
                validate: (value) => validateCpf(value) || "CPF inválido!",
              })}
              autoComplete="new-off"
              mask="999.999.999-99"
              placeholder="Ex: 123.456.789-10"
              maskChar=""
            />
            {errors.cpf && (
              <span className={styles.errorMessage}>
                <AiOutlineExclamationCircle />
                {errors.cpf?.message}
              </span>
            )}
          </div>
          <div className={styles.formItem}>
            <label>Número do cartão</label>
            <InputMask
              mask="9999999999??????"
              formatChars={{
                "9": "[0-9]",
                "?": "[0-9]",
              }}
              autoComplete="new-off"
              maskChar=""
              placeholder="Ex: 1234567809101112"
              {...register("cardnumber", {
                validate: (value) =>
                  ccValid.number(value.toString()).isValid === true ||
                  "Insira um número de cartão válido!",
              })}
            />
            {errors.cardnumber && (
              <span className={styles.errorMessage}>
                <AiOutlineExclamationCircle />
                {errors.cardnumber?.message}
              </span>
            )}
          </div>
          <div className={styles.formItem}>
            <label>Data de expiração</label>
            <InputMask
              mask="99/99"
              placeholder="Ex: 03/24"
              autoComplete="new-off"
              maskChar=""
              {...register("expirydate", {
                validate: (value) =>
                  value.length >= 5 ||
                  "Sua data de expiração deve ser do tipo DD/AA",
              })}
            />
            {errors.expirydate && (
              <span className={styles.errorMessage}>
                <AiOutlineExclamationCircle />
                {errors.expirydate?.message}
              </span>
            )}
          </div>
          <div className={styles.formItem}>
            <label>CVV</label>
            <InputMask
              mask="999?"
              placeholder="Ex: 123"
              formatChars={{
                "9": "[0-9]",
                "?": "[0-9]",
              }}
              autoComplete="new-off"
              maskChar=""
              {...register("cvv", {
                validate: (value) =>
                  value.length >= 3 ||
                  "Seu CVV tem que possuir no mínimo 3 números",
              })}
            />
            {errors.cvv && (
              <span className={styles.errorMessage}>
                <AiOutlineExclamationCircle />
                {errors.cvv?.message}
              </span>
            )}
          </div>
          <p className={styles.formItem}>Endereço de cobrança</p>
          <div className={styles.formItem}>
            <label>CEP</label>
            <InputMask
              mask="99999-999"
              placeholder="Ex: 12345-678"
              autoComplete="new-off"
              maskChar=""
              onKeyUp={(e: any) =>
                e.target.value.length === 9 &&
                buscaCep(e.target.value, setAddress, setValue)
              }
              {...register("cep", {
                validate: (value) =>
                  value.length >= 9 || "Seu CEP deve possuir 8 números",
              })}
            />

            {errors.cep && (
              <span className={styles.errorMessage}>
                <AiOutlineExclamationCircle />
                {errors.cep?.message}
              </span>
            )}
          </div>

          <div className={styles.formItem}>
            <label>Rua</label>
            <input
              defaultValue={address?.logradouro}
              type="text"
              placeholder="Ex: Rua Guaranabara"
              autoComplete="new-off"
              {...register("street", { required: "Este campo é obrigatório!" })}
            />
            {errors.street && (
              <span className={styles.errorMessage}>
                <AiOutlineExclamationCircle />
                {errors.street?.message}
              </span>
            )}
          </div>
          <div className={styles.formItem}>
            <label>Número</label>
            <input
              type="text"
              autoComplete="new-off"
              placeholder="Ex: 17"
              {...register("housenumber", {
                required: "Este campo é obrigatório!",
              })}
            />
            {errors.housenumber && (
              <span className={styles.errorMessage}>
                <AiOutlineExclamationCircle />
                {errors.housenumber?.message}
              </span>
            )}
          </div>
          <div className={styles.formItem}>
            <label>Complemento</label>
            <input
              type="text"
              autoComplete="new-off"
              placeholder="Ex: Ap 15B"
              {...register("complement", {})}
            />
            {errors.complement && (
              <span className={styles.errorMessage}>
                <AiOutlineExclamationCircle />
                {errors.complement?.message}
              </span>
            )}
          </div>
          <div className={styles.formItem}>
            <label>Bairro</label>
            <input
              defaultValue={address?.bairro}
              type="text"
              autoComplete="new-off"
              placeholder="Ex: Dom Bosco"
              {...register("district", {
                required: "Este campo é obrigatório!",
              })}
            />
            {errors.district && (
              <span className={styles.errorMessage}>
                <AiOutlineExclamationCircle />
                {errors.district?.message}
              </span>
            )}
          </div>
          <div className={styles.formItem}>
            <label>Cidade</label>
            <input
              defaultValue={address?.localidade}
              type="text"
              placeholder="Ex: São Paulo"
              autoComplete="new-off"
              {...register("city", { required: "Este campo é obrigatório!" })}
            />
            {errors.city && (
              <span className={styles.errorMessage}>
                <AiOutlineExclamationCircle />
                {errors.city?.message}
              </span>
            )}
          </div>
          <div className={styles.formItem}>
            <label>Estado</label>
            <input
              defaultValue={address?.uf}
              type="text"
              placeholder="Ex: São Paulo"
              autoComplete="new-off"
              {...register("state", { required: "Este campo é obrigatório!" })}
            />
            {errors.state && (
              <span className={styles.errorMessage}>
                <AiOutlineExclamationCircle />
                {errors.state?.message}
              </span>
            )}
          </div>

          <button
            className={styles.submitPerfilButton}
            type="button"
            disabled={loadingUserCreditCard && true}
            onClick={handleSubmit(onSubmit)}
          >
            {loadingUserCreditCard ? <AnimatedLoading /> : "Salvar"}
          </button>
        </form>
      </div>
    </div>
  );
};
