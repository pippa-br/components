import styles from "./styles.module.scss";
import { GetStaticProps } from "next";
import withHeader from "../../../../utils/withHeader";
import { BannerSlider } from "../BannerSlider";
import { CategoryCarousel } from "../CategoryCarousel";
import { FeaturesColumns } from "../FeaturesColumns";
import { NewestProductsSlider } from "../NewestProductsSlider";
import { calls } from "../../../../core/v2/util/call.api";
import { collectionProduct } from "../../../../core/v2/product/product.api";
import { HOME_PAGE_SETTING, PAGE_SETTING, PRODUCT_SETTING } from "../../../../setting/setting";
import { getDocument } from "../../../../core/v2/document/document.api";

const HomePage = ({ categories, newestProducts, homePage }: any) => 
{
	return (
		<div className={styles.homePage}>
			<div className={styles.content}>
				<BannerSlider sliderMobile sliderArray={homePage?.bannersHome} />
				<FeaturesColumns features={homePage?.features} />
				<CategoryCarousel categories={categories} />
				{/* <BestsellerMasonry products={bestsellerProducts} /> */}
				<NewestProductsSlider products={newestProducts} />
				{/* <SaleBanner /> */}
				{/* <Journal /> */}
				{/* <Newsletter />  */}
			</div>
		</div>
	);
}

const getStaticProps : GetStaticProps = () => withHeader(async () => 
{
	let [newestProducts, homePage] = await calls(collectionProduct(PRODUCT_SETTING.merge(
		{
			perPage: 12,
			where: [
				{
				field: "published",
				operator: "==",
				value: true,
				},
			],
		})),
		getDocument(HOME_PAGE_SETTING));
	
		return {
			revalidate : 60,
			props: {
				newestProducts : newestProducts?.collection || [],
				homePage	   : homePage?.data || [],
			},
		};
});

export { getStaticProps as GetStaticProps, HomePage}