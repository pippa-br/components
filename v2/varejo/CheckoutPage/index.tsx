import withHeader from "../../../../utils/withHeader";
import styles from "./styles.module.scss";
import { CheckoutForm } from "../../general/CheckoutForm";
import { GetStaticProps } from "next";
import { getDocument } from "../../../../core/v2/document/document.api";
import { OPTION_SETTING } from "../../../../setting/setting";

const CheckoutPage = ({ account, paymentMethods }: any) =>
{
	return (
		<div className={styles.checkoutPage}>
			<CheckoutForm account={account} 
						  logo="/assets/logo.png" 
						  redirectUrl="/pedido/" 
						  typeDoc="cpf"
						  paymentMethods={paymentMethods}/>	
		</div>		
	);
}

const getStaticProps : GetStaticProps = () => withHeader(async () => 
{
	const paymentMethods = await getDocument(OPTION_SETTING.merge({
		document : {
			referencePath : 'default/option/documents/kdEtgv8BEwVRju3W5gmD'
		}
	}));

	return {
		revalidate : 1,
		props: {
			paymentMethods : paymentMethods.data.items
		},
	};
});

export { getStaticProps as GetStaticProps, CheckoutPage }