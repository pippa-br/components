import { useRouter } from "next/router";
import { AiOutlineCloseCircle, AiOutlineHome } from "react-icons/ai";
import { BsLightning, BsQuestionLg } from "react-icons/bs";
import { IoIosArrowDown, IoIosArrowForward, IoIosInformationCircleOutline } from "react-icons/io";
import { GiLargeDress, GiTrousers, GiWorld } from "react-icons/gi";
import { FaRegUserCircle } from "react-icons/fa";
import styles from "./styles.module.scss";
import { useState } from "react";
import IUser from "../../../../core/v2/interface/i.user";
import { onEvent } from "../../../../core/v2/util/use.event";

export const MobileMenu = ({ handleModal, categories }: any) => 
{
    const [clothesSubMenu, setClothesSubMenu] = useState(false);
    const router = useRouter();

    const [user, setUser] = useState<IUser>();

    onEvent("changeUser", (event:any) => 
    {
        setUser(event);		
    });	

    return (
        <div className={styles.container} onClick={() => handleModal(false)}>
            <div className={styles.content} onClick={(e) => e.stopPropagation()}>
              <AiOutlineCloseCircle
                onClick={() => handleModal(false)}
                className={styles.closeMobileMenuButton}
              />
              <div
                onClick={() => {
                  router.push("/perfil");
                  handleModal(false);
                }} className={styles.user}>
                <FaRegUserCircle className={styles.Iconuser} />
                {user ? (                  
                  <p>{user.name}</p>
                ) 
                : 
                (
                  <p>Fazer Login</p>
                )}
              </div>
              <div className={styles.mobileMenu}>
                <div
                  className={styles.mobileItem}
                  onClick={() => {
                    router.push("/");
                    handleModal(false);
                  }}
                >
                  <p>
                    <AiOutlineHome className={styles.Icon} />
                    Home
                  </p>
                </div>
                <div className={styles.divisor}></div>

                <div
                  className={styles.mobileItem}
                  onClick={() => {
                    router.push("/novidades?orderBy=postdate");
                    handleModal(false);
                  }}
                >
                  <p>
                    <BsLightning />
                    Novidades
                  </p>
                </div>
                <div className={styles.divisor}></div>

                <div className={styles.mobileItem}>
                  
                  <p onClick={() => setClothesSubMenu(!clothesSubMenu)}>
                    <GiLargeDress />
                    Roupas
                    <IoIosArrowForward className={styles.Arrow} />
                  </p>

                  <div className={styles.divisor}></div>

                  {clothesSubMenu && (
                    <div className={styles.subMenu}>
                      {categories &&
                        categories.map((category: any, index: any) => (
                          <p
                            key={index}
                            onClick={() => {
                              router.push(`/categoria/${category.name}`);
                              handleModal(false);
                            }}
                          >
                            {category.name}
                          </p>
                        ))}
                    </div>
                  )}
                </div>
                <div className={styles.divisor}></div>

                {/* <p
                  onClick={() => {
                    router.push(`/perfil`);
                    handleModal(false);
                  }}
                >
                  Conta
                </p>
                <p
                  onClick={() => {
                    router.push("/lista-de-desejos");
                    handleModal(false);
                  }}
                >
                  Lista de desejos
                </p> */}
                <div className={styles.mobileItem}>
                  <p
                    onClick={() => {
                      router.push("/loja/duvidas");
                      handleModal(false);
                    }}
                  >
                    <BsQuestionLg />
                    Dúvidas
                  </p>
                </div>

                <div className={styles.divisor}></div>

                <div className={styles.mobileItem}>
                  <p
                    onClick={() => {
                      router.push("/loja/sobre-nos");
                      handleModal(false);
                    }}
                  >
                    <IoIosInformationCircleOutline />
                    Sobre Nós
                  </p>
                </div>

                <div className={styles.divisor}></div>

                <div className={styles.mobileItem}>
                  <p
                    onClick={() => {
                      router.push("/loja/sustentabilidade");
                      handleModal(false);
                    }}
                  >
                    <GiWorld />
                    Sustentabilidade
                  </p>
                </div>
                <div className={styles.divisor}></div>
              </div>
            </div>
        </div>
    );
};
