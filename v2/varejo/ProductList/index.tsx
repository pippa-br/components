import styles from "./styles.module.scss";
import { useState } from "react";
import { ProductItem } from "../ProductItem";
import { ProductItemSkeleton } from "../ProductItemSkeleton";

export const ProductList = ({ products, loadingProducts }: any) => {

  return (

    <div className={styles.productList}>
      {loadingProducts ? (
        <>
          {Array.from({ length: 24 }).map((product, index) => (
            <ProductItemSkeleton key={`productSkeleton-${index}`} />
          ))}
        </>
      ) : (
        products && products.length > 0 ? (
          products.map((product: any) => (
            <ProductItem key={product.sku} product={product} />
          ))
        ) : (
          <p className={styles.noProducts}>Não foram encontrados produtos.</p>
        )
      )}

    </div>
  );
};
