import styles from "./styles.module.scss";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { AiFillEdit, AiFillDelete } from "react-icons/ai";
import { useState } from "react";
import { toast } from "react-hot-toast";
import withHeader from "../../../../utils/withHeader";
import { GetServerSideProps } from "next";
import { PageTitle } from "../../general/PageTitle";
import { AddAddressModal } from "../AddAddressModal";

function AddressPage({ user }: any) {
  const [openModal, setOpenModal] = useState(false);
  const [openEditModal, setEditOpenModal] = useState(false);
  const [addressToEdit, setAddressToEdit] = useState();

  const router = useRouter();

  const logout = async () => {
    // await api.post("/api/authV2/logout");
    // dispatch(getUserData());
  };

  const toggleEditModal = async (address: any) => {
    setEditOpenModal(true);
    setAddressToEdit(address);
  };

  const deleteAddress = async (addressReference: any) => {
    // await dispatch(deleteUserAddress(addressReference));
    // toast.success("Endereço deletado com sucesso!");
    // await dispatch(getUserData());
  };

  return (
    <>
      <div className={styles.addressPage}>
        <div className={styles.content}>
          <PageTitle name="Endereços" />
          <div className={styles.perfilData}>
            <div className={styles.perfilMenu}>
              <a href="/perfil">Meus Pedidos</a>
              <a href="/perfil/detalhes-da-conta">Detalhes da conta</a>
              <a href="/perfil/enderecos" className={styles.active}>
                Endereços
              </a>
              <a href="/perfil/cartoes">Cartões</a>
              <a href="/" onClick={() => logout()}>
                Logout
              </a>
            </div>
            <div className={styles.perfilContent}>
              {user?.address?.length >= 1 ? (
                <div className={styles.addressGrid}>
                  {user?.address?.map((address: any) => (
                    <div
                      key={address.referencePath}
                      className={styles.addressItem}
                    >
                      <div className={styles.addressItemActions}>
                        <AiFillEdit
                          onClick={() => toggleEditModal(address)}
                          title="Editar endereço"
                        />
                        <AiFillDelete
                          onClick={() => deleteAddress(address.referencePath)}
                          title="Deletar endereço"
                        />
                      </div>

                      <span>{address.name}</span>
                      <span>
                        {address.address.street},{address.address.housenumber}
                      </span>
                      <span>{address.address.complement}</span>
                      <span>
                        {address.address.city}, {address.address.state}
                      </span>
                    </div>
                  ))}
                </div>
              ) : (
                <span className={styles.noAddress}>
                  Você não possui endereços cadastrados.
                </span>
              )}
              <button
                type="button"
                className={styles.addAddressButton}
                onClick={() => setOpenModal(true)}
              >
                Cadastrar endereço
              </button>
            </div>
          </div>
        </div>
      </div>
      {openModal && <AddAddressModal setOpenModal={setOpenModal} />}
      {openEditModal && (
        <AddAddressModal
          setOpenModal={setEditOpenModal}
          edit={true}
          addressData={addressToEdit}
        />
      )}
    </>
  );
}

export default AddressPage;

export const getServerSideProps: GetServerSideProps = ({ req, res }) =>
  withHeader({ req, res, protected: false }, async () => {
    return {
      props: {},
    };
  });
