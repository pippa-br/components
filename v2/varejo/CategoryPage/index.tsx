import styles from "./styles.module.scss";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { GetStaticProps } from "next";
import withHeader from "../../../../utils/withHeader";
import { calls } from "../../../../core/v2/util/call.api";
import { collectionDocument, getDocument } from "../../../../core/v2/document/document.api";
import { CATEGORY_SETTING, PRODUCT_SETTING, COLOR_VARIANT_SETTING, SIZE_VARIANT_SETTING } from "../../../../setting/setting";
import { tagManager } from "../../../../core/v2/util/TagManager";
import { productParseQuery } from "../../../../core/v2/product/product.util";
import { collectionProduct } from "../../../../core/v2/product/product.api";
import { Filters } from "../Filters";
import { ProductList } from "../ProductList";
import { PageTitle } from "../../general/PageTitle";

const CategoryPage = ({ category, variants }: any) =>
{
	const [products, setProducts] = useState();
	const router = useRouter();
	const query = useRouter().query;
	const [loadingProducts, setLoadingProducts] = useState(true);

	async function handleFilteredProducts() 
	{
		const result: any = await collectionProduct(PRODUCT_SETTING.merge(
		{
			orderBy: query?.orderBy,
			perPage: 24,
			where: [
			{
				field: "published",
				operator: "==",
				value: true,
			},
			...productParseQuery(
				router.query,
				[category],
				variants.colors.items,
				variants.sizes.items
			),
			],
		}));

		setProducts(result.collection);
		setLoadingProducts(false)

		tagManager.addProducts(router.asPath, result.collection);
	}

	useEffect(() => {		
		handleFilteredProducts();
	}, [router.asPath]);

	return (
		<div className={styles.container}>
			<div className={styles.categoryBanner}>
				<img
				className={styles.desktop}
				src={category?.image?._url}
				alt="Banner Category"
				/>
				<img
				className={styles.mobile}
				src={category?.mobile?._url}
				alt="Banner Category"
				/>
			</div>
			<div className={styles.content}>
				<PageTitle name={query.category} />
				<Filters filters={variants} />
				<ProductList loadingProducts={loadingProducts} products={products} />
			</div>
		</div>
	);
}

const getStaticProps : GetStaticProps = ({params}) => withHeader(async () => 
{
	let [categories, sizesVariant, colorsVariant] = await calls(
		collectionDocument(CATEGORY_SETTING.merge(
		{
			orderBy : 'order', 
			where : [{
				field 	 : 'name',
				operator : '==',
				value    : params?.category
			}]
		})),
		getDocument(COLOR_VARIANT_SETTING),
		getDocument(SIZE_VARIANT_SETTING)
	);

	return {
		revalidate : 60,
		props: {
			category : categories?.collection.length > 0 ? categories.collection[0] : null,
			variants: {
				sizes	: sizesVariant?.data || [],
				colors  : colorsVariant?.data || [],
			},
		},
	};
});

const getStaticPaths = async () =>
{
	const categories = await collectionDocument(CATEGORY_SETTING.merge({
		perPage : 100,
	}));
  
	const paths = categories.collection.map((item:any) => ({
	  	params : { category : item.name },
	}));
  
	return { paths, fallback: 'blocking' }
}

export { getStaticProps as GetStaticProps, getStaticPaths as GetStaticPaths, CategoryPage}