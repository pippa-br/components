import styles from "./styles.module.scss";
import { useEffect, useState } from "react";
import router, { useRouter } from "next/router";
import { MobileMenu } from "../MobileMenu";
import {
	AiOutlineHeart,
	AiOutlineShoppingCart,
	AiOutlineUser,
} from "react-icons/ai";
import { clearEvent, dispatchEvent, onEvent } from "../../../../core/v2/util/use.event";
import { tagManager } from "../../../../core/v2/util/TagManager";
import { getLoggedAuth } from "../../../../core/v2/auth/auth.api";
import { getCart } from "../../../../core/v2/cart/cart.api";
import { AUTH_SETTING, CART_SETTING, MENU_HEADER_STATIC } from "../../../../setting/setting";
import ICart from "../../../../core/v2/interface/i.cart";

export const Header = ({ categories, account, loadingProducts }: any) => {
	const [stickyHeader, setStickyHeader] = useState("normal");
	const [isMobileMenuOpen, setIsMobileMenuOpen] = useState(false);
	const location = useRouter().pathname;
	const [cart, setCart] = useState<ICart>();
	const [user, setUser] = useState();

	// IMPORTANTE: RESET OS SUBSCRIBERS DO ON EVENT
	clearEvent();

	getLoggedAuth(AUTH_SETTING, (data: any) => {
		dispatchEvent('changeUser', data);
	});

	getCart(CART_SETTING, (data: any) => {
		dispatchEvent('changeCart', data);
	});

	onEvent("changeCart", (event: any) => {
		setCart(event);
	});

	onEvent("changeUser", (event: any) => {
		setUser(event);
	});

	useEffect(() => {
		if (account && account.gtm) {
			tagManager.initialize(account.gtm);
		}

		window.addEventListener("scroll", () => {
			if (window.scrollY >= 300) {
				setStickyHeader("sticky");
			}
			else {
				setStickyHeader("normal");
			}
		});
	}, []);

	return (
		<>
			<div
				className={styles.header}
				style={
					location == "/"
						? {
							position: "absolute",
							background: "transparent",
							color: "#fff",
						}
						: {
							color: "#000",
						}
				}
			>
				<div className={styles.content}>
					<div className={styles.top}>
						<li onClick={() => router.push("/")}>
							{location == "/" ? (
								<>
									<img
										className={styles.logo}
										src="/assets/logo_white.png"
										alt=""
									/>
									<img
										className={styles.logoColor}
										src="/assets/logo.png"
										alt=""
									/>
								</>
							) : (
								<img
									className={styles.logoOthers}
									src="/assets/logo.png"
									alt=""
								/>
							)}
						</li>
						<div className={styles.otherPages}>
							{MENU_HEADER_STATIC && (MENU_HEADER_STATIC.map((item: any) =>
							(
								<li key={item.id} onClick={() => router.push(item.url)}>{item.label}</li>
							)))}
						</div>
					</div>
					<div className={styles.bottom}>
						<div className={styles.nav}>
							<nav>
								<li onClick={() => router.push("/novidades?orderBy=postdate")}>
									Novidades
								</li>
								<li className={styles.subMenu}>
									Roupas
									<ul>
										{categories &&
											categories.map((category: any) => (
												<li
													onClick={() =>
														router.push(`/categoria/${category.name}`)
													}
													key={category.id}
												>
													{category.name}
												</li>
											))}
									</ul>
								</li>
							</nav>
							{/* <span className={styles.separator}>|</span>
					<div className={styles.pages}>
					<a href="/best">Best</a>
					<a href="/evento">Evento</a>
					<a href="/blog">Blog</a>
					<a href="/lookbook">LookBook</a>
					</div> */}
						</div>
						<div className={styles.menu}>
							<li onClick={() => router.push("/lista-de-desejos")}>
								<AiOutlineHeart />
							</li>

							<li
								className={styles.cartIcon}
								onClick={() => router.push("/carrinho")}
							>
								<AiOutlineShoppingCart />
								<span>{cart?.items?.length || 0}</span>
							</li>
							<li onClick={() => router.push("/perfil")}>
								<AiOutlineUser />
							</li>
						</div>
					</div>
					<div className={styles.mobileMenu}>
						<li onClick={() => router.push("/")}>
							<img
								className={styles.mobileHeaderLogo}
								src="/assets/logo.png"
								alt=""
							/>
						</li>
						<li
							className={styles.mobileCart}
							onClick={() => router.push("/carrinho")}
						>
							<AiOutlineShoppingCart />
							<span>{cart?.items?.length || 0}</span>
						</li>
						<img
							onClick={() => setIsMobileMenuOpen(true)}
							className={styles.mobileMenuNav}
							src="/assets/icons/bars-solid.svg"
							alt=""
						/>
					</div>
				</div>
			</div>
			{stickyHeader != "normal" && (
				<div className={styles.headerSticky}>
					<div className={styles.content}>
						<li onClick={() => router.push("/")}>
							<img className={styles.logo} src="/assets/logo.png" alt="" />
						</li>
						<div className={styles.nav}>
							<nav>
								<li onClick={() => router.push("/novidades?orderBy=postdate")}>
									Novidades
								</li>
								<li className={styles.subMenu}>
									Roupas
									<ul>
										{categories &&
											categories.map((category: any) => (
												<li
													onClick={() =>
														router.push(`/categoria/${category.name}`)
													}
													key={category.id}
												>
													{category.name}
												</li>
											))}
									</ul>
								</li>
							</nav>
							{/* <span className={styles.separator}>|</span>
				<div className={styles.pages}>
					<a href="/best">Best</a>
					<a href="/evento">Evento</a>
					<a href="/blog">Blog</a>
					<a href="/lookbook">LookBook</a>
				</div> */}
						</div>
						<div className={styles.menu}>
							<li onClick={() => router.push("/lista-de-desejos")}>
								<AiOutlineHeart />
							</li>
							<li
								className={styles.cartIcon}
								onClick={() => router.push("/carrinho")}
							>
								<AiOutlineShoppingCart />
								<span>{cart?.items?.length || 0}</span>
							</li>
							<li onClick={() => router.push("/perfil")}>
								<AiOutlineUser />
							</li>
						</div>
						<div className={styles.mobileMenu}>
							<li onClick={() => router.push("/")}>
								<img
									className={styles.mobileHeaderLogo}
									src="/assets/logo.png"
									alt=""
								/>
							</li>
							<li
								className={styles.mobileCart}
								onClick={() => router.push("/carrinho")}
							>
								<AiOutlineShoppingCart />
								<span>{cart?.items?.length || 0}</span>
							</li>
							<img
								onClick={() => setIsMobileMenuOpen(true)}
								className={styles.mobileMenuNav}
								src="/assets/icons/bars-solid.svg"
								alt=""
							/>
						</div>
					</div>
				</div>
			)}
			{isMobileMenuOpen && (
				<MobileMenu categories={categories} handleModal={setIsMobileMenuOpen} />
			)}
		</>
	);
};
