import styles from "./styles.module.scss";
import { collectionFavorite } from "../../../../core/v2/favorite/favorite.api";
import { FAVORITE_SETTING } from "../../../../setting/setting";
import { useState } from "react";
import { PageTitle } from "../../general/PageTitle";
import { ProductItem } from "../ProductItem";
import { GetStaticProps } from "next";
import withHeader from "../../../../utils/withHeader";

const FavoritePage = () =>
{
    const [favorites, setFavorites] = useState<any>();
  
    collectionFavorite(FAVORITE_SETTING, (collection:any) => 
    {
        setFavorites(collection);
    });

    return (
      <div className={styles.wishlistPage}>
        <div className={styles.content}>
          <PageTitle name="Lista de desejos" />

          {favorites && favorites.length >= 1 ? (
            <div className={styles.favoriteProducts}>
              {favorites.map((product: any) => (
                <ProductItem
                  key={product.document.id}
                  product={product.document}
                />
              ))}
            </div>
          ) : (
            <p className={styles.noFavorite}>Não há produtos em seu favorito.</p>
          )}
        </div>
      </div>
    );
}

const getStaticProps : GetStaticProps = () => withHeader(async () => 
{
	return {
		props: {
		},
	};
});

export { FavoritePage, getStaticProps as GetStaticProps };