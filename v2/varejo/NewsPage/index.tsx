import styles from "./styles.module.scss";
import { PRODUCT_SETTING, COLOR_VARIANT_SETTING, SIZE_VARIANT_SETTING } from "../../../../setting/setting";
import { GetStaticProps } from "next";
import withHeader from "../../../../utils/withHeader";
import { calls } from "../../../../core/v2/util/call.api";
import { getDocument } from "../../../../core/v2/document/document.api";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { collectionProduct } from "../../../../core/v2/product/product.api";
import { productColorVariant, productParseQuery } from "../../../../core/v2/product/product.util";
import { PageTitle } from "../../general/PageTitle";
import { Filters } from "../Filters";
import { ProductList } from "../ProductList";

const NewsPage = ({variants}:any) =>
{
    const [products, setProducts] = useState();
	const router 				  = useRouter();
	const [loadingProducts, setLoadingProducts] = useState(true);

	async function handleFilteredProducts() 
	{
		const result: any = await collectionProduct(PRODUCT_SETTING.merge(
		{
			orderBy: router.query.orderBy,
			perPage: 24,
			levelVariant : productColorVariant(router, variants.colors.items),
			where: [
			{
				field: "published",
				operator: "==",
				value: true,
			},
			...productParseQuery(
				router.query,
				[],
				variants.colors.items,
				variants.sizes.items
			),
			],
		}));

		setProducts(result.collection);
		setLoadingProducts(false)
	}

	useEffect(() => 
	{

		handleFilteredProducts();

	}, [router.query]);

	return (
		<div className={styles.newsPage}>
			<div className={styles.content}>
				<PageTitle name={"Loja"} />
				<Filters filters={variants} querys={router.query} />
				<ProductList loadingProducts={loadingProducts} products={products} />
			</div>
		</div>
	);
}

const getStaticProps : GetStaticProps = ({params}) => withHeader(async () => 
{
    let [colorsVariant, sizesVariant] = await calls(
		getDocument(COLOR_VARIANT_SETTING),
		getDocument(SIZE_VARIANT_SETTING)
	);

	return {
		revalidate : 60,
		props: {
			variants: {
				sizes	: sizesVariant?.data || [],
				colors  : colorsVariant?.data || [],
			},
		},
	};
});

export { getStaticProps as GetStaticProps, NewsPage}