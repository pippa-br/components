import styles from "./styles.module.scss";
import { Box, Skeleton } from "@material-ui/core";

export const ProductItemSkeleton = () => {
    return (
        <Box sx={{ width: 300 }} className={styles.productItemSkeleton}>
            <div className={styles.productImage}>
                <Skeleton animation="wave" variant="rectangular" style={{ height: 330 }} />
            </div>
            <div className={styles.productActions}>
                <Skeleton animation="wave" variant="text" width={"100%"} height={30} />
            </div>
            <div className={styles.productName}>
                <Skeleton animation="wave" variant="text" width={"100%"} height={30} />
            </div>
            <div className={styles.actionButton}>
                <Skeleton
                    animation="wave"
                    variant="rectangular"
                    width={"100%"}
                    style={{ borderRadius: "4px", height: 36 }}
                />
            </div>
        </Box>
    );
}