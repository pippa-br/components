import styles from "./styles.module.scss";

export const SaleBanner = ({}) => {
  return (
    <div className={styles.saleBanner}>
      <div className={styles.content}>
        <div className={styles.image}>SALE</div>
        <div className={styles.info}>
          <p className={styles.title}>Últimas Peças</p>
          <p className={styles.text}>Promoção até 70%</p>
          <p className={styles.shopButton}>
            Shop Now
            <img
              className={styles.nextIcon}
              src="../../assets/icons/angle-right-solid.svg"
              alt=""
            />
          </p>
        </div>
      </div>
    </div>
  );
};
