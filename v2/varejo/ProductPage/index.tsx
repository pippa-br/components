import styles from "./styles.module.scss";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { GetStaticProps } from "next";
import withHeader from "../../../../utils/withHeader";
import { calls } from "../../../../core/v2/util/call.api";
import { collectionDocument } from "../../../../core/v2/document/document.api";
import { FAVORITE_SETTING, PRODUCT_SETTING } from "../../../../setting/setting";
import { tagManager } from "../../../../core/v2/util/TagManager";
import IUser from "../../../../core/v2/interface/i.user";
import { onEvent } from "../../../../core/v2/util/use.event";
import { getFavorite } from "../../../../core/v2/favorite/favorite.api";
import { ProductDetails } from "../ProductDetails";
import { ProductItem } from "../ProductItem";
import { getProduct } from "../../../../core/v2/product/product.api";

const ProductPage = ({ product }: any) =>
{
	const router = useRouter();
	const query  = router.query;

	const [user, setUser] = useState<IUser>();
	const [hasFavorite, setHasFavorite] = useState<any>(false);

	//console.log(product);

	onEvent("changeUser", (event:any) => 
	{
		setUser(event);		
	});	

	getFavorite(FAVORITE_SETTING.merge({
		document: {
			referencePath: product?.referencePath,
		}
	}),
	(status:boolean) => 
	{
		setHasFavorite(status);
	});


	useEffect(() => {
		tagManager.addProduct(router.asPath, product);	
	}, [router.asPath]);

	return (
		<div className={styles.productPage}>
			<div className={styles.content}>

				<ProductDetails
					user={user}
					hasFavorite={hasFavorite}
					color={query.color}
					product={product}
				/>

				{product?.relatedProducts?.length > 0 && <>
					<p className={styles.title}>Produtos Relacionados</p>
					<div className={styles.relatedProducts}>
					{product?.relatedProducts?.map((product: any) => (
						(product.slug && <ProductItem key={product.id} product={product} />)
					))}
					</div>
				</>}				
			</div>
		</div>
	);
}

const getStaticProps : GetStaticProps = ({params}) => withHeader(async () => 
{
	const [product] = await calls(
		getProduct(PRODUCT_SETTING.merge({
			slug : params?.slug,
		}))
	); 

	return {
		revalidate : 60,
		props: {
			product: product?.data || [],
		},
	};
});

const getStaticPaths = async () =>
{
	const categories = await collectionDocument(PRODUCT_SETTING.merge({
		perPage : 1000,
		noCache : true,
	}));
  
	const paths = categories.collection.map((item:any) => ({
	  	params : { slug : item.slug },
	}));
  
	return { paths, fallback: 'blocking' }
}

export { getStaticProps as GetStaticProps, getStaticPaths as GetStaticPaths, ProductPage}