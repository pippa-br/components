import styles from "./styles.module.scss";
import { GetStaticProps } from "next";
import { useState } from "react";
import ICart from "../../../../core/v2/interface/i.cart";
import { onEvent } from "../../../../core/v2/util/use.event";
import { PageTitle } from "../../general/PageTitle";
import CartProductsTable from "../../general/CartProductsTable";
import withHeader from "../../../../utils/withHeader";

const CartPage = () =>
{
	const [cart, setCart] = useState<ICart>();
	const [loadingProducts, setLoadingProducts] = useState(true);

	onEvent("changeCart", (event:any) => 
	{
		setCart(event);
	});

	onEvent("startLoadingCart", (event:any) => 
	{
		setLoadingProducts(true);
	});
	
	onEvent("endLoadingCart", (event:any) => 
	{
		setLoadingProducts(false);
	});

	return (
		<div className={styles.cartPage}>
			<div className={styles.content}>
				<PageTitle name="Carrinho" />
				<CartProductsTable cart={cart} loadingProducts={loadingProducts} />
			</div>
		</div>
	);
}

const getStaticProps : GetStaticProps = () => withHeader(async () => 
{
	return {
		revalidate : 1,
		props: {
		},
	};
});

export { getStaticProps as GetStaticProps, CartPage}