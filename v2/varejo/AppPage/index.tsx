import { GetStaticProps } from "next";
import withHeader from "../../../../utils/withHeader";
import NextNProgress from "nextjs-progressbar";
import { Header } from "../Header";
import { Footer } from "../Footer";
import { Toaster } from "react-hot-toast";
import { WhatsAppIcon } from "../../general/WhatsAppIcon";
import CookieConsent from "react-cookie-consent";
import { AppProps } from "next/app";
import { INFO_STATIC } from "../../../../setting/setting";
import { disableReactDevTools } from "../../../../core/v2/util/disableReactDevTools";
import { useEffect } from "react";

const AppPage = ({ Component, pageProps }: any) =>
{
	useEffect(() => 
	{
		disableReactDevTools();
		
	}, []);

	return (
		<>		
		<NextNProgress
			color="var(--theme-color);"
			startPosition={0.3}
			stopDelayMs={200}
			height={4}
			showOnShallow={true}
			options={{ easing: "ease", speed: 500 }}
		/>
		<Header
			categories={pageProps.categories}
			cart={pageProps.cart}
			account={pageProps.account}
		/>
		<Component {...pageProps} />
		<Footer account={pageProps.account} />
		<Toaster
			position="top-center"
			reverseOrder={true}
			containerStyle={{
			fontSize: 16,
			}}
			toastOptions={{
			duration: 2500,
			}}
		/>
		<WhatsAppIcon account={pageProps.account} />
		<CookieConsent
			location="bottom"
			buttonText="Aceitar"
			cookieName="aodenim1"
			style={{ background: "#2B373B", fontSize: "14px", textAlign:"center" }}
			buttonStyle={{ color: "#4e503b", fontSize: "14px" }}
			>
			<div dangerouslySetInnerHTML={{ __html: INFO_STATIC.COOKIECONSENT }}></div>
			</CookieConsent>
		</>
	);
}

export { AppPage}