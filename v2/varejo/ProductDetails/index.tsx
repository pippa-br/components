import styles from "./styles.module.scss";
import Slider from "react-slick";
import Select from "react-select";
import { useEffect, useState } from "react";
import InputMask from "react-input-mask";
import { AiFillHeart, AiOutlineCloseCircle, AiOutlineHeart, AiOutlinePlus } from "react-icons/ai";
import { toast } from "react-hot-toast";
import { useRouter } from "next/router";
import { hasStockVariantColor, hasStockVariantSize, productHasPromotional, productImagesVariant, productInstallments, productPrice, productPromotionalPrice, productVariantByLabel, productVariantByValue, quantityStockVariantSize } from "../../../../core/v2/product/product.util";
import { ImageSet } from "../../general/ImageSet";
import { calculateShipping } from "../../../../core/v2/shipping/shipping.api";
import { CART_SETTING, FAVORITE_SETTING, SHIPPING_SETTING } from "../../../../setting/setting";
import { tagManager } from "../../../../core/v2/util/TagManager";
import { setItemCart } from "../../../../core/v2/cart/cart.api";
import { dispatchEvent } from "../../../../core/v2/util/use.event";
import { setFavorite } from "../../../../core/v2/favorite/favorite.api";
import VariantProps from "../../../../core/v2/interface/i.variant";
import { AnimatedLoading } from "../../general/AnimatedLoading";
import { currencyMask } from "../../../../core/v2/util/mask";
import { SizeTableModal } from "../../general/SizeTableModal";
import { IoIosClose } from "react-icons/io";
import { BsCheck, BsFillCheckCircleFill } from "react-icons/bs";
import { BiCloset } from "react-icons/bi";
import { FaChevronCircleLeft, FaChevronCircleRight } from "react-icons/fa";

export function ProductDetails({ product, color, user, hasFavorite }: any)
{
	//console.log(product);

	const [productSizeTable, setProductSizeTable] = useState(false);

	const router = useRouter();

	const [productQuantity, setProductQuantity] = useState(1);
	const [destination, setDestination] = useState("");
	const [loadingShipping, setLoadingShipping] = useState(false);
	const [loadingAddToCart, setLoadingAddToCart] = useState(false);
	const [shippingMethods, setShippingMethods] = useState([]);
	const [variantColor, setVariantColor] = useState<VariantProps>();
	const [variantSize, setVariantSize] = useState<VariantProps>();
	const [variantImages, setVariantImages] = useState([]);

	function handleIncrementProductQuantity()
	{
		if(quantityStockVariantSize(product, variantColor, variantSize) > productQuantity)
		{
			setProductQuantity(productQuantity + 1);
		}		
	}

	function handleDecrementProductQuantity()
	{
		if(productQuantity > 1)
		{
			setProductQuantity(productQuantity - 1);
		}
	}

	const sliderOptions =
	{
		customPaging: function (i: any)
		{
			return (
				<div className="thumb-img">
					<ImageSet image={variantImages[i]}/>
				</div>
			);
		},
		dots: true,
		dotsClass: "slick-dots slick-thumb",
		infinite: true,
		speed: 500,
		slidesToShow: 1,
		slidesToScroll: 1,
		nextArrow: <FaChevronCircleRight />,
		prevArrow: <FaChevronCircleLeft />
	};

	async function handleChangeColor(color:any)
	{
		setProductQuantity(1);
		setVariantColor(color);
		setVariantImages(productImagesVariant(product, color?.value));
	}

	async function handleChangeSize(size:any)
	{
		if(hasStockVariantSize(product, variantColor, size))
		{
			setProductQuantity(1);
			setVariantSize(size);
		}		
	}

	async function handleCalculateShipping()
	{
		setLoadingShipping(true);

		const result = await calculateShipping(SHIPPING_SETTING.merge({
			destination : destination,
			weight		: product.weight,
		}));

		if(result.status)
		{
			setShippingMethods(result.collection);
		}

		setLoadingShipping(false);
	}

	const handleAddToCart = async () =>
	{
		const newData = {
			data: {
				product: {
					referencePath: product.referencePath,
				},
				variant: [variantColor, variantSize],
				quantity: productQuantity,
			},
		};

		tagManager.addToCart(router.asPath, product, 1, user);

		setLoadingAddToCart(true);

		const result = await setItemCart(CART_SETTING.merge(newData));

		setLoadingAddToCart(false);

		if(!result.status)
		{
			return toast.error(result.error,
			{
				duration: 2000,
			});
		}

		dispatchEvent("changeCart", result.data);

		toast.success("Produto adiconado com sucesso!");

		router.push("/carrinho");
	};

	async function handleFavoriteProduct(product: any)
	{
		const newData = {
			document: {
				referencePath: product?.referencePath,
			},
		};

		const result = await setFavorite(FAVORITE_SETTING.merge(newData));

		if(!result.status)
		{
			return toast.error("Oops! Ocorreu um erro. Tente novamente mais tarde  ");
		}

		toast.success("Produto favoritado!");
	}

	useEffect(() => {

		if(product && color)
		{
			const variantColor = productVariantByLabel(product, color);

			if(variantColor)
			{
				setVariantColor(variantColor);			
				setVariantImages(productImagesVariant(product, variantColor.value));
	
				for(const item of product?.variant[1]?.items)
				{
					if(hasStockVariantSize(product, variantColor, item))
					{
						setVariantSize(item);
						break;
					}				
				}
			}			
		}

	}, [product, color]);

	return (
		<>
		<div className={styles.productDetails}>
			<div className={styles.productCarousel}>
				<Slider {...sliderOptions} className="productSlider">
					{variantImages?.map(
					(image: any) => (
						<div key={image?.id} className="productSlide">
							<ImageSet image={image}/>
						</div>
					))}
				</Slider>
			</div>
			<div className={styles.productInfo}>
				<p className={styles.productName}>{product?.name}</p>
				<div className={styles.productPrice}>
					{productHasPromotional(product) ? (
					<>
						<span className={styles.sale}>
							{productPrice(product)}
						</span>
						<span className={styles.price}>
							{productPromotionalPrice(product)}
						</span>
					</>
					) : (
					<span className={styles.price}>
						R$ {productPrice(product)},00
					</span>
					)}
				</div>
			<p className={styles.productInstallments}>
				{productInstallments(product,6,90)}
			</p>
			<div
				className={styles.productDescription}
				dangerouslySetInnerHTML={{ __html: product?.description }}
			/>
			<p className={styles.composition}>
				{product.composition}
			</p>
			<div className={styles.productColorSize}>
				<div className={styles.productColor}>
					<span>Cor:</span>
					<div className={styles.ballsColor}>
						{product?.variant[0]?.items.map((item: any, index: any) => (
							<p key={index} onClick={(e) => handleChangeColor(item)}>
								{item && item.image && <ImageSet image={item.image} aspectRatio="1 / 1" />}
								{variantColor && variantColor.value == item.value && <BsCheck />}
							</p>
						))}
					</div>
				</div>
				<div className={styles.productSize}>
					<span>Tamanho:</span>
					<div className={styles.numberSize}>
						{product.variant[1].items.map((item: any, index: any) => (
							(hasStockVariantSize(product, variantColor, item) ? 
							(
								<p key={index} onClick={(e:any) => handleChangeSize(item)}>
								{item.label}
								{variantSize && variantSize.value == item.value && <BsCheck />}
							</p>
							) : 
							(
								<p key={index} className={styles.variantDisabled}>
									{item.label} <IoIosClose />
								</p>
							))
						))}
					</div>
				</div>
				<div className={styles.productQuantity}>
					<span>Quantidade:</span>
					<div className={styles.inputQuantity}>
						<span onClick={handleDecrementProductQuantity}>-</span>
						<span className={styles.value}>{productQuantity}</span>
						<span onClick={handleIncrementProductQuantity}>+</span>
					</div>
				</div>
			</div>
			{/* <div className={styles.productStock}>
			<p>Estoque:</p>
			<p className={styles.warning}>
				Disponível - <span>Última peça! Aproveite agora.</span>
			</p>
			</div> */}
			<div className={styles.productActions}>
				<div>
				{user && (
					<>
					<span>Lista de desejos</span>
					{hasFavorite ? (
						<AiFillHeart
						onClick={() => handleFavoriteProduct(product)}
						/>
					) : (
						<AiOutlineHeart
						onClick={() => handleFavoriteProduct(product)}
						/>
					)}
					</>
				)}
				</div>
				<button
				type="button"
				disabled={loadingAddToCart}
				onClick={() => handleAddToCart()}
				>
				{loadingAddToCart ? <AnimatedLoading /> : "Adicionar ao carrinho"}
				</button>
			</div>
			<div className={styles.productShipping}>
				<InputMask
				mask="99999-999"
				maskChar=""
				onChange={(e: any) => setDestination(e.target.value)}
				placeholder="CEP 00000-000"
				/>

				<button
				type="button"
				disabled={loadingShipping}
				onClick={() => handleCalculateShipping()}
				>
				{loadingShipping ? <AnimatedLoading /> : "Entrar"}
				</button>
				<div className={styles.shippingMethods}>
				{shippingMethods?.length >= 1 &&
					shippingMethods?.map(
					(shippingMethod: any) =>
						shippingMethod?.type?.label != "Retirada no Local" && (
						<div className={styles.shipping} key={shippingMethod.id}>
							<p>{shippingMethod?.name}:</p>
							<p>{shippingMethod?.label}</p>
							<p>{currencyMask(shippingMethod?.value)}</p>
						</div>
						)
					)}
				</div>
			</div>
			<div
				className={styles.productLink}
				onClick={() => setProductSizeTable(true)}
			>
				<span>Tabela de medidas</span>
				<AiOutlinePlus />
			</div>
			<div
				className={styles.productLink}
				onClick={() => router.push("/loja/politica-de-privacidade")}
			>
				<span>Política de troca</span>
				<AiOutlinePlus />
			</div>
			{/* <div className={styles.productText}>
			<span>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
				eiusmod tempor incididunt ut labore et dolore magna aliqua.
			</span>
			</div> */}
			</div>
		</div>
		{productSizeTable && product?.measures?.table && (
			<SizeTableModal
			table={product?.measures?.table}
			setModal={setProductSizeTable}
			/>
		)}
		</>
	);
}
