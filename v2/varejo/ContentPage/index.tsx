import styles from "./styles.module.scss";
import { GetServerSideProps, GetStaticProps } from "next";
import withHeader from "../../../../utils/withHeader";
import { PageTitle } from "../../general/PageTitle";
import { innerHTML } from "../../../../core/v2/util/util";
import { PAGE_SETTING } from "../../../../setting/setting";
import { collectionDocument, getDocument } from "../../../../core/v2/document/document.api";

const ContentPage = ({page}:any) =>
{
	return (
		<div className={styles.contentPage}>
			<div className={styles.content}>
				<PageTitle name={page.name} />
				<div className={styles.pageContent} dangerouslySetInnerHTML={innerHTML(page.content)}></div>
			</div>
		</div>
	);
}

const getStaticProps : GetStaticProps = ({params}) => withHeader(async () => 
{
	const pageResult = await getDocument(PAGE_SETTING.merge({
		slug : params?.slug
	}))

	return {		
		props: {
			page : pageResult.data,
		},
		revalidate : 60,
	};
});

const getStaticPaths = async () =>
{
	const pages = await collectionDocument(PAGE_SETTING.merge({
		perPage : 100,
	}));
		
	const paths = pages.collection.map((item:any) => ({
	  	params : { slug : item.slug || '' },
	}));
  
	return { paths, fallback: 'blocking' }
}

export { getStaticProps as GetStaticProps, getStaticPaths as GetStaticPaths, ContentPage}