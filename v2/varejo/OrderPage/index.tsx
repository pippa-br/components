import styles from "./styles.module.scss";
import { ORDER_SETTING, REORDER_REFUND_OPTION, REORDER_TYPE_OPTION } from "../../../../setting/setting";
import { GetStaticProps } from "next";
import withHeader from "../../../../utils/withHeader";
import { OrderViewer } from "../../general/OrderViewer";
import { calls } from "../../../../core/v2/util/call.api";
import { collectionDocument, getDocument } from "../../../../core/v2/document/document.api";
import { getTrackCorreios } from "../../../../core/v2/shipping/shipping.api";
import { PageTitle } from "../../general/PageTitle";

const OrderPage = ({order, orderTrack, reorderType, reorderRefund}:any) =>
{
    return (
        <div className={styles.orderPage}>
            <div className={styles.content}>
                <PageTitle parents={[{url:'/perfil', name:'Meus Pedidos'}]} name="Pedido" />
                <OrderViewer order={order} orderTrack={orderTrack} reorderType={reorderType} reorderRefund={reorderRefund}/>
            </div>
        </div>
    );
}

const getStaticProps : GetStaticProps = ({params}) => withHeader(async () => 
{
    const [order, reorderType, reorderRefund] = await calls(
        getDocument(ORDER_SETTING.merge({
            id : params?.id,
        })),
        getDocument(REORDER_TYPE_OPTION),
        getDocument(REORDER_REFUND_OPTION)
    ); 

    let orderTrack : any;

    if(order.data?.trackingCode) 
    {
        orderTrack = await getTrackCorreios(ORDER_SETTING.merge({
            code : order.data?.trackingCode
        }));
    }

    return {
        revalidate : 60,
        props: {
            order         : order?.data || {},
            orderTrack    : orderTrack?.collection[0] || {},
            reorderType   : reorderType?.data || {},
            reorderRefund : reorderRefund?.data || {},
        },
    };
});

const getStaticPaths = async () =>
{
    const categories = await collectionDocument(ORDER_SETTING.merge({
        perPage : 100,
    }));

    let paths = [];

    if(categories.collection)
    {
        paths = categories.collection.map((item:any) => ({
            params : { id : item.id },
        }));    
    }

    return { paths, fallback: 'blocking' }
}

export { getStaticProps as GetStaticProps, getStaticPaths as GetStaticPaths, OrderPage}