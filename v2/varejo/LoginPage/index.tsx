import { GetStaticProps } from "next";
import withHeader from "../../../../utils/withHeader";
import LoginForm from "../../general/LoginForm";

const LoginPage = () =>
{
	return (
		<LoginForm/>
	);
}

const getStaticProps : GetStaticProps = () => withHeader(async () => 
{
	return {
		props: {
		},
	};
});

export { getStaticProps as GetStaticProps, LoginPage}