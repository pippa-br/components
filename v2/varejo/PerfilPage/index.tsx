import styles from "./styles.module.scss";
import { GetStaticProps } from "next";
import { useRouter } from "next/router";
import { useState } from "react";
import { logoutAuth } from "../../../../core/v2/auth/auth.api";
import { collectionDocument } from "../../../../core/v2/document/document.api";
import { onEvent } from "../../../../core/v2/util/use.event";
import { AUTH_SETTING, ORDER_SETTING } from "../../../../setting/setting";
import withHeader from "../../../../utils/withHeader";
import { toast } from "react-hot-toast";
import { PageTitle } from "../../general/PageTitle";
import { currencyMask } from "../../../../core/v2/util/mask";

const PerfilPage = () =>
{
	const [user,   setUser]   = useState<any>();
	const [orders, setOrders] = useState([]);
	const router 			  = useRouter();

	onEvent("changeUser", async (user:any) => 
	{
		// SET ORDERS
		const result = await collectionDocument(
			ORDER_SETTING.merge({
				where: [
					{
						field: "client",
						operator: "==",
						value: {
							referencePath: user?.referencePath,
						},
					},
				],
			})
		);

		setOrders(result.collection);
		setUser(user);
	});

	const handleLogout = async () => 
	{
		await logoutAuth(AUTH_SETTING);
		//router.push("/");
		router.reload();
		toast.success("Logout feito com sucesso!");
	};

	return (
		<div className={styles.perfilPage}>
			<div className={styles.content}>
			<PageTitle name="Meus Pedidos" />
			<div className={styles.perfilData}>
				<div className={styles.perfilMenu}>
				<p
					onClick={() => router.push("/perfil")}
					className={styles.active}
				>
					Meus Pedidos
				</p>
				<p onClick={() => router.push("/perfil/detalhes-da-conta")}>
					Detalhes da conta
				</p>
				<p
					onClick={() => { handleLogout(); }}
				>
					Logout
				</p>
				</div>
				<div className={styles.perfilContent}>
				<div className={styles.top}>
					<p>
					<span>{user?.name && user?.name + ","}</span> Seja Bem vindo.
					</p>
					{orders?.length <= 0 && (
					<span>Você ainda não possui pedidos.</span>
					)}
				</div>
				<div className={styles.perfilOrders}>
					{orders &&
					orders?.map((order: any) => (
						<div
						className={styles.perfilOrder}
						key={`${order.id}`}
						onClick={() => router.push(`/pedido/${order.id}`)}
						>
						<p className={styles.orderItem}>
							Número do pedido: <span>{order._sequence}</span>
						</p>
						<p className={styles.orderItem}>
							Status do pagamento: <span>{order.statusPayment}</span>
						</p>
						<p className={styles.orderItem}>
							Tipo de pagamento:{" "}
							<span>{order.paymentMethod?.label}</span>
						</p>
						<p className={styles.orderItem}>
							Método de entrega:{" "}
							<span>{order.shipping?.type.label}</span>
						</p>
						<p className={styles.orderItem}>
							Total do pedido:{" "}
							<span>{currencyMask(order.total)}</span>
						</p>
						</div>
					))}
				</div>
				</div>
			</div>
			</div>
		</div>
	);
}

const getStaticProps : GetStaticProps = () => withHeader(async () => 
{
	return {
		revalidate : 1,
		props: {
		},
	};
});

export { getStaticProps as GetStaticProps, PerfilPage}