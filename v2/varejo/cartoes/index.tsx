import styles from "./styles.module.scss";
import { useRouter } from "next/router";
import { AiFillDelete } from "react-icons/ai";
import { useState } from "react";
import { toast } from "react-hot-toast";
import { PageTitle } from "../../general/PageTitle";
import { AddCreditCardModal } from "../AddCreditCardModal";


function CreditCardsPage({ userCreditCards, userData }: any) {
  const [openModal, setOpenModal] = useState(false);
  const router = useRouter();

  async function handleLogout() {
    //await authPlugin.logoutFront();
    router.push("/");
    toast.success("Logout feito com sucesso!");
  }

  const deleteCreditCard = async (creditCardReference: any) => {
    toast.success("Cartão deletado com sucesso!");
    const newData = {
      document: {
        referencePath: creditCardReference,
      },
    };
    //await creditCardsPlugin.cancelDocumentFront(newData);
    router.reload();
  };

  return (
    <>
      <div className={styles.creditCardsPage}>
        <div className={styles.content}>
          <PageTitle name="Endereços" />
          <div className={styles.perfilData}>
            <div className={styles.perfilMenu}>
              <a href="/perfil">Meus Pedidos</a>
              <a href="/perfil/detalhes-da-conta">Detalhes da conta</a>
              <a href="/perfil/enderecos">Endereços</a>
              <a href="/perfil/cartoes" className={styles.active}>
                Cartões
              </a>
              <a onClick={() => handleLogout()}>Logout</a>
            </div>
            <div className={styles.perfilContent}>
              {userCreditCards?.length >= 1 ? (
                <div className={styles.addressGrid}>
                  {userCreditCards.map((creditcard: any) => (
                    <div
                      key={creditcard.referencePath}
                      className={styles.addressItem}
                    >
                      <div className={styles.addressItemActions}>
                        <AiFillDelete
                          onClick={() =>
                            deleteCreditCard(creditcard.referencePath)
                          }
                          title="Deletar cartão"
                        />
                      </div>

                      <span>{creditcard.cardnumber}</span>
                      <span>{creditcard.owner}</span>
                      <span>{creditcard.expirydate}</span>
                    </div>
                  ))}
                </div>
              ) : null}
              <span className={styles.noCreditCards}>
                Você não possui cartões cadastrados.
              </span>
              <button
                type="button"
                className={styles.addAddressButton}
                onClick={() => setOpenModal(true)}
              >
                Cadastrar cartão
              </button>
            </div>
          </div>
        </div>
      </div>

      {openModal && (
        <AddCreditCardModal userData={userData} setOpenModal={setOpenModal} />
      )}
    </>
  );
}

export default CreditCardsPage;
