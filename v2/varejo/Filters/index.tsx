import styles from "./styles.module.scss";
import Select from "react-select";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

export const Filters = ({ filters }: any) => 
{
	const [defaultColorFilter, setDefaultColorFilter] = useState();
	const [defaultSizeFilter, setDefaultSizeFilter]   = useState();
	const router 									  = useRouter();
	const querys									  = useRouter().query;

	const customSelectStyles: any = 
	{
		control: (base: any, state: any) => ({
		...base,
		borderRadius: "none",
		borderColor: state.isFocused
			? "var(--theme-color)"
			: "var(--theme-color)",
		boxShadow: state.isFocused ? "none" : null,
		"&:hover": {
			borderColor: "var(--theme-color)",
		},
		color: "#000",
		backgroundColor: state.hasValue ? "var(--theme-color)" : null,
		}),
		singleValue: (styles: any) => ({ ...styles, color: "#fff" }),
		indicatorsContainer: (styles: any, state: any) => ({
		...styles,

		color: state.hasValue ? "#fff" : "#000",
		}),
		indicatorSeparator: () => null,
	};

	useEffect(() => 
	{
		if(querys.color || querys.size) 
		{
			setDefaultColorFilter(
				filters.colors.items.filter((color: any) => color.value == querys.color)
			);
			setDefaultSizeFilter(
				filters.sizes.items.filter((size: any) => size.value == querys.size)
			);
		} 
		else 
		{
			setDefaultColorFilter(undefined);
			setDefaultSizeFilter(undefined);
		}
	}, [router]);

	return (
		<div className={styles.container}>
			<Select
				placeholder="Cores"
				options={filters.colors.items}
				styles={customSelectStyles}
				isClearable
				value={defaultColorFilter || null}
				onChange={(e: any) =>
				e
					? router.push({ query: { ...querys, color: e.value } })
					: router.push({ query: { ...querys, color: undefined } })
				}
			/>
			<Select
				placeholder="Tamanhos"
				options={filters.sizes.items}
				styles={customSelectStyles}
				isClearable
				value={defaultSizeFilter || null}
				onChange={(e: any) =>
				e
					? router.push({ query: { ...querys, size: e.value } })
					: router.push({ query: { ...querys, size: undefined } })
				}
			/>
			{/* <Select
				placeholder="Tipo"
				options={typeOptions}
				styles={customSelectStyles}
				isClearable
			/>
			<Select
				placeholder="Preço"
				options={priceOptions}
				styles={customSelectStyles}
				isClearable
			/> */}
		</div>
	);
};
