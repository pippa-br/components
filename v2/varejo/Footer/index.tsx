import router from "next/router";
import { AiOutlineFacebook, AiOutlineInstagram } from "react-icons/ai";
import { INFO_STATIC, MENU_FOOTER_STATIC } from "../../../../setting/setting";
import styles from "./styles.module.scss";

export const Footer = ({ account }: any) => (
    <footer className={styles.footer}>
        <div className={styles.content}>
            <div className={styles.left}>
              <div className={styles.logoContent}>
                <img src="/assets/logo_white.png" alt="Logo" className={styles.logo} />
                <div dangerouslySetInnerHTML={{ __html: INFO_STATIC.FOOTER_INFO1 }}></div>
              </div>              
              <div dangerouslySetInnerHTML={{ __html: INFO_STATIC.FOOTER_INFO2 }}></div>   
            </div>
            <div className={styles.right}>
                {MENU_FOOTER_STATIC && (MENU_FOOTER_STATIC.map((item:any) => 
                (
                    <div key={item.id} className={styles.column}>
                      <p className={styles.title}>{item.label}</p>
                      {item.children.map((item2:any) => (
                          <p key={item2.id} className={styles.item} onClick={() => router.push(item2.url)}>
                              {item2.label}
                          </p>
                      ))}                                           
                    </div>
                )))}                          
                <div className={styles.column}>
                  <p className={styles.title}>Conta</p>
                  <p className={styles.item} onClick={() => router.push("/perfil")}>Minha conta</p>
                  <p className={styles.item} onClick={() => router.push("/perfil")}>
                    Meus Pedidos
                  </p>
                  <p
                    className={styles.item}
                    onClick={() => router.push("/lista-de-desejos")}
                  >
                    Meus Desejos
                  </p>
                  <p className={styles.item}>
                      <AiOutlineFacebook
                        onClick={() => window.open(account?.facebook, "_blank")}
                      />
                      <AiOutlineInstagram
                        onClick={() => window.open(account?.instagram, "_blank")}
                      />
                    </p>
                </div>
            </div>            
        </div>
        <div className={styles.contentImg}>
            <div className={styles.left}>
                <img src="/assets/tipos-de-pagamento.png" alt="Tipos de pagamento" />
            </div>
            <div className={styles.right}>
                <img src="/assets/seguranca.png" alt="Segurança" />
            </div>
        </div>
    </footer>
);
