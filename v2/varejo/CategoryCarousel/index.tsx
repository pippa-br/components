import styles from "./styles.module.scss";
import Slider from "react-slick";
import { useRef } from "react";
import { useRouter } from "next/router";

export const CategoryCarousel = ({ categories }: any) => {
  const customSlider = useRef<any>();

  const history = useRouter();

  var sliderOpts = {
    infinite: true,
    arrows: false,
    speed: 500,
    swipeToSlide: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 720,
        settings: {
          slidesToShow: 2,
          swipeToSlide: true,
          slidesToScroll: 1,
          infinite: true,
        },
      },
    ],
  };

  const nextSliderHandle = () => {
    customSlider.current.slickNext();
  };

  const prevSliderHandle = () => {
    customSlider.current.slickPrev();
  };

  return (
    <div className={styles.categorySlider}>
      <div className={styles.content}>
        <div className={styles.top}>
          <p className={styles.title}>Categorias</p>
          <div className={styles.sliderArrows}>
            <img
              className={styles.backIcon}
              onClick={prevSliderHandle}
              src="/assets/icons/angle-left-solid.svg"
              alt=""
            />
            <img
              className={styles.nextIcon}
              onClick={nextSliderHandle}
              src="/assets/icons/angle-right-solid.svg"
              alt=""
            />
          </div>
        </div>

        <Slider {...sliderOpts} className="categoryCarousel" ref={customSlider}>
          {categories.map((category: any) => (
            <div className="slide" key={category.id}>
              <img
                src={category?.mobile?._url}
                className="image"
                alt="Categoria"
                onClick={() => history.push(`/categoria/${category.name}`)}
              ></img>
              <p className="name">{category.name}</p>
            </div>
          ))}
        </Slider>
      </div>
    </div>
  );
};
