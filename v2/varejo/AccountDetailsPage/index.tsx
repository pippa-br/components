import styles from "./styles.module.scss";
import { GetStaticProps } from "next";
import InputMask from "react-input-mask";
import Select from "react-select";
import { useRouter } from "next/router";
import { useRef, useState } from "react";
import { useForm } from "react-hook-form";
import toast from "react-hot-toast";
import { getUserAuth, logoutAuth, setUserAuth } from "../../../../core/v2/auth/auth.api";
import { TDate } from "../../../../core/v2/model/TDate";
import { validateCpf } from "../../../../core/v2/util/validate";
import { AUTH_SETTING } from "../../../../setting/setting";
import withHeader from "../../../../utils/withHeader";
import LoginForm from "../../general/LoginForm";
import { AiOutlineExclamationCircle } from "react-icons/ai";
import { AnimatedLoading } from "../../general/AnimatedLoading";
import { Checkbox } from 'pretty-checkbox-react';
import { PageTitle } from "../../general/PageTitle";

const AccountDetailsPage = () =>
{
	const [loadingUserData, setLoadingUserData] 		= useState(false);
	const [loadingUserPassword, setLoadingUserPassword] = useState(false);
	const [hasNewsletter, setHasNewsletter] 			= useState(false);
	const [user, setUser] 								= useState<any>();
	const [gender, setGender] 							= useState<any>();
	const router 										= useRouter();

	getUserAuth(AUTH_SETTING, (data:any) => 
	{
		delete data.password;
		delete data.confirmPassword;
		data.birthday = new TDate(data.birthday).format('DD/MM/YYYY');

		setUser(data);
		setGender(data.gender);

		reset(data);
		reset(data); // BUG: INPUTMASK

		setHasNewsletter(data.newsletter);
	});

	const genderOptions = [
		{ label: "Masculino", value: "M", id: "yfcqOV15" },
		{ label: "Feminino", value: "F", id: "BoXeFDWZ" },
	];

	const customSelectStyles: any = 
	{
		control: (base: any, state: any) => ({
			...base,
			borderRadius: "4px",
			borderColor: state.isFocused
				? "var(--border-color)"
				: "var(--border-color)",
			boxShadow: state.isFocused ? "none" : null,
			"&:hover": {
				borderColor: "var(--border-color)",
			},
			width: "100%",
			color: "#000",
		}),
		menu: (styles: any) => ({ ...styles, color: "#000", fontSize: "16px" }),
		placeholder: (styles: any) => ({ ...styles, fontSize: "16px" }),
		singleValue: (styles: any) => ({ ...styles, color: "#000", fontSize: "16px" }),
		indicatorSeparator: () => null,
	};

	const {
		register,
		handleSubmit,
		reset,
		formState: { errors, isSubmitted },
	} = useForm();

	const {
		register: register2,
		formState: { errors: errors2 },
		watch: watch2,
		handleSubmit: handleSubmit2,
	} = useForm({
		mode: "onBlur",
	});

	const password = useRef({});
	password.current = watch2("password", "");

	function handleChangeGender(e: any) 
	{
		register("gender", { value : e });
		setGender(e);
	}

	const onSubmit = async (data: any) => 
	{
		if(!validateCpf(data.cpf)) 
		{
			return toast.error("CPF inválido!");
		}

		const birthday = new TDate(data.birthday, 'DD/MM/YYYY', true);

		if(!birthday.isValid())
		{
			return toast.error("Data de aniversário inválida!");
		}

		if(!gender)
		{
			return toast.error("Gênero requirido");
		}

		const newData = {
			document: {
				referencePath: user.referencePath,
			},
			data : {
				name       : data.name,
				email      : data.email,
				cpf        : data.cpf,
				birthday   : birthday.toDate(),
				phone      : data.phone,
				gender     : data.gender,
				newsletter : hasNewsletter,
			},
		};

		await setLoadingUserData(true);

		const result: any = await setUserAuth(AUTH_SETTING.merge(newData));

		await setLoadingUserData(false);

		if(result.status === false) 
		{			
			return toast.error(result.error);
		}

		toast.success("Dados atualizados com sucesso!");
	};

	const changePassword = async (data: any) => 
	{
		if(data.password.length <= 0 && data.confirmPassword.length <= 0) 
		{
			return toast.error("Preencha os campos de senha!");
		}

		const newData = {
			document: {
				referencePath: user.referencePath,
			},
			data : {
				password     	: data.password,
				confirmPassword : data.confirmPassword,
			},
		};

		await setLoadingUserPassword(true);

		const result: any = await setUserAuth(AUTH_SETTING.merge(newData));

		await setLoadingUserPassword(false);

		if(result.status === false) 
		{
			return toast.error(result.error);
		}

		toast.success("Senha atualizada com sucesso!");
	};

	const handleLogout = async () => 
	{
		await logoutAuth(AUTH_SETTING);
		//router.push("/");
		router.reload();
		toast.success("Logout feito com sucesso!");
	};

	const onNewsletter = async () => 
	{
		setHasNewsletter(!hasNewsletter);
	};	

	return (
		<div className={styles.accountDetailsPage}>
			<div className={styles.content}>
				<PageTitle name="Detalhes da conta" />
				<div className={styles.perfilData}>
				<div className={styles.perfilMenu}>
					<p onClick={() => router.push("/perfil")}>Meus Pedidos</p>
					<p
					onClick={() => router.push("/perfil/detalhes-da-conta")}
					className={styles.active}
					>
					Detalhes da conta
					</p>
					<p
					onClick={() => {
						handleLogout();
					}}
					>
					Logout
					</p>
				</div>
				<div className={styles.perfilContent}>
					<form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
						<div className={styles.formItem}>
							<label>Nome completo</label>
							<input
							type="text"
							autoComplete="new-password"
							{...register("name", {
								validate: (value) =>
								value.length >= 8 ||
								"Seu nome deve possuir 8 caracteres no mínimo!",
							})}
							/>
							{errors.name && (
							<span className={styles.errorMessage}>
								<AiOutlineExclamationCircle />
								{errors.name?.message}
							</span>
							)}
						</div>

						<div className={styles.formItem}>
							<label>E-mail</label>
							<input
							type="email"
							required
							autoComplete="new-password"
							{...register("email", {
								required: "Digite um e-mail válido!",
							})}
							/>
							{errors.email && (
							<span className={styles.errorMessage}>
								<AiOutlineExclamationCircle />
								{errors.email?.message}
							</span>
							)}
						</div>

						<div className={styles.formItem}>
							<label>CPF</label>
							<InputMask
							{...register("cpf")}
							autoComplete="new-password"
							mask="999.999.999-99"
							maskChar=""
							/>
							{errors.cpf && (
							<span className={styles.errorMessage}>
								<AiOutlineExclamationCircle />
								{errors.cpf?.message}
							</span>
							)}
						</div>

						<div className={styles.formItem}>
							<label>Data de nascimento</label>
							<InputMask
							mask="99/99/9999"
							autoComplete="new-password"
							maskChar=""
							{...register("birthday", {
								validate: (value) =>
								value.length >= 10 || "Verifique sua data de nascimento!",
							})}
							/>
							{errors.birthday && (
							<span className={styles.errorMessage}>
								<AiOutlineExclamationCircle />
								{errors.birthday?.message}
							</span>
							)}
						</div>

						<div className={styles.formItem}>
							<label>Celular</label>
							<InputMask
							mask="(99) 99999-9999"
							maskChar=""
							autoComplete="new-password"
							{...register("phone", {
								validate: (value) =>
								value.length >= 15 || "Verifique seu celular!",
							})}
							/>
							{errors.phone && (
							<span className={styles.errorMessage}>
								<AiOutlineExclamationCircle />
								{errors.phone?.message}
							</span>
							)}
						</div>

						<div className={styles.formItem}>
							<label>Gênero</label>
							<Select
								placeholder="Escolha seu gênero"
								options={genderOptions}
								styles={customSelectStyles}
								isClearable
								value={gender}
								onChange={handleChangeGender}
							/>
							{isSubmitted && !gender && (
							<span className={styles.errorMessage}>
								<AiOutlineExclamationCircle />
								Campo required!
							</span>
							)}
						</div>

					<button
							className={styles.submit}
							type="button"
							disabled={loadingUserData && true}
							onClick={handleSubmit(onSubmit)}
						>
							{loadingUserData ? (
							<AnimatedLoading />
							) : (
							"Atualizar meus dados cadastrais"
							)}
						</button>
						
					</form>

					<form className={styles.form}>
						<div className={styles.formItem}>
							<label>Newsletter</label>
							<span className={styles.subtitle}>
								Deseja receber e-mails com promoções? Marque esta opção e atualize seus dados cadastrais.
							</span>
							<div>
								{/* <input
									autoComplete="new-password"
									type="checkbox"
									defaultChecked={user?.newsletter}
									{...register("newsletter")}
								/> */}
								<Checkbox color="success" checked={hasNewsletter} onChange={onNewsletter}>Quero receber e-mails com promoções</Checkbox>
							</div>
						</div>
					</form>
					<form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
					<div className={styles.formItem}>
						<label>Senha</label>
						<span className={styles.subtitle}>
						Crie/Altere sua senha preenchendo os campos abaixo
						</span>
						<div className={styles.perfilPasswords}>
						<input
							type="password"
							autoComplete="new-password"
							placeholder="Digite sua senha"
							{...register2("password")}
						/>
						<input
							type="password"
							autoComplete="new-password"
							placeholder="Confirme sua senha"
							{...register2("confirmPassword", {
							validate: (value) =>
								value === password.current ||
								"As senhas precisam ser iguais!",
							})}
						/>
						{errors2.confirmPassword && (
							<span className={styles.errorMessage}>
							<AiOutlineExclamationCircle />
							{errors2.confirmPassword.message}
							</span>
						)}
						<button
							className={styles.submit}
							type="button"
							disabled={loadingUserPassword && true}
							onClick={handleSubmit2(changePassword)}
						>
							{loadingUserPassword ? (
							<AnimatedLoading />
							) : (
							"Atualizar senha"
							)}
						</button>
						</div>
					</div>
					</form>
				</div>
				</div>
			</div>
		</div>
	);
}

const getStaticProps : GetStaticProps = () => withHeader(async () => 
{
	return {
		props: {
		},
	};
});

export { getStaticProps as GetStaticProps, AccountDetailsPage}