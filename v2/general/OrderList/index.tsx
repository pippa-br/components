import styles from "./styles.module.scss";
import { useRouter } from "next/router";
import { useState } from "react";
import { onEvent } from "../../../../core/v2/util/use.event";
import { firstImageItemCart } from "../../../../core/v2/cart/cart.util";
import { currencyMask } from "../../../../core/v2/util/mask";
import { collectionDocument } from "../../../../core/v2/document/document.api";
import { ORDER_SETTING } from "../../../../setting/setting";
import { ImageSet } from "../ImageSet";

export const OrderList = () => 
{
	const router = useRouter();
	const [orders, setOrders] = useState([]);

	onEvent("changeUser", (async (user:any) => 
	{	
		const result = await collectionDocument(ORDER_SETTING.merge({
			where: [
			  {
				field: "client",
				operator: "==",
				value: {
					  referencePath: user?.referencePath,
				},
			  }]
		}));	
		
		setOrders(result.collection);
  	}));

	return (
		<div className={styles.orderList}>
		<div className={styles.content}>
			<div className={styles.orders}>
			<p className={styles.title}>Meus pedidos</p>
			{orders && orders.length > 0 ? (
				<>
				{orders.map((order: any) => (
					<>
					<div className={styles.order}>
						<p className={styles.title}>Pedido #{order?._sequence}</p>
						<p className={styles.subtitle}>
						{currencyMask(order?.total)} | {order?.items?.length}{" "}
						Produto(s)
						</p>

						<div className={styles.itens}>
						{order?.items?.map((item: any) => (
							<div key={item.id}>
							<ImageSet image={firstImageItemCart(item)}/>
							</div>
						))}

						<button
							type="button"
							onClick={() =>
							router.push(`/meus-pedidos/pedido/${order.id}`)
							}
						>
							Ver detalhes
						</button>
						</div>
						<div className={styles.line}></div>
					</div>
					</>
				))}
				</>
			) : (
				<p className={styles.noOrders}>
				Você não possui pedidos no momento.
				</p>
			)}
			</div>
		</div>
		</div>
	);
};