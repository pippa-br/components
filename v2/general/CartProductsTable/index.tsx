import styles from "./styles.module.scss";
import { BiTrashAlt } from "react-icons/bi";
import { toast } from "react-hot-toast";
import router from "next/router";
import { tagManager } from "../../../../core/v2/util/TagManager";
import { clearCart, delItemCart, setItemCart } from "../../../../core/v2/cart/cart.api";
import { CART_SETTING } from "../../../../setting/setting";
import ICart from "../../../../core/v2/interface/i.cart";
import { useEffect, useState } from "react";
import { dispatchEvent } from "../../../../core/v2/util/use.event";
import { currencyMask } from "../../../../core/v2/util/mask";
import { firstImageItemCart, hasVariantItemCart, variantItemCart } from "../../../../core/v2/cart/cart.util";
import { ImageSet } from "../../general/ImageSet";
import { CartProductsItem } from "../CartProductsItem";
import { ProductItemSkeletonCart } from "../../varejo/ProductItemSkeletonCart";

function CartProductsTable({ cart, user, productsUrl = '/novidades', loadingProducts }: any) {
	const [newCart, setNewCart] = useState<ICart>(cart);

	async function handleIncrementQuantity(product: any, variant: any, quantity: any) {
		const newData: any = {
			data: {
				product: {
					referencePath: product.referencePath,
				},
				quantity: quantity + 1,
			},
		};

		// PRODUCT VARIANT
		if (variant) {
			newData.variant = [...variant]
		}

		tagManager.addToCart(router.asPath, product, 1, user);

		const result = await setItemCart(CART_SETTING.merge(newData));

		if (result.status) {
			toast.success("Quantidade alterada com sucesso!");
			setNewCart(result.data);
		}
		else {
			return toast.error(result.error, {
				duration: 3000,
			});
		}
	}

	async function handleDecrementQuantity(product: any, variant: any, quantity: any) {
		const newData: any = {
			data: {
				product: {
					referencePath: product.referencePath,
				},
				quantity: quantity - 1,
			},
		};

		// PRODUCT VARIANT
		if (variant) {
			newData.variant = [...variant]
		}

		tagManager.removeFromCart(router.asPath, product, 1);

		const result = await setItemCart(CART_SETTING.merge(newData));

		if (result.status) {
			toast.success("Quantidade alterada com sucesso!");
			setNewCart(result.data);
		}
		else {
			return toast.error(result.error, {
				duration: 3000,
			});
		}
	}

	async function handleDeleteProduct(product: any, variant: any) {
		const filteredCartItems = newCart.items.filter(
			(item: any) => item.id != product.id
		);

		setNewCart({ ...newCart, items: filteredCartItems || [] });

		toast.success("Produto excluído com sucesso!");

		tagManager.removeFromCart(router.asPath, product, product.quantity);

		const newData: any = {
			data: {
				product: {
					referencePath: product.referencePath,
				},
			},
		};

		// PRODUCT VARIANT
		if (variant) {
			newData.variant = [...variant]
		}

		const result = await delItemCart(CART_SETTING.merge(newData));

		if (filteredCartItems.length >= 1) {
			return setNewCart(result.data);
		}
		else {
			return router.reload();
		}
	}

	async function handleCheckoutCart() {
		// setLoadingCheckoutOrder(true);
		// if (!validateCpf(userData?.cpf)) {
		//   setLoadingCheckoutOrder(false);
		//   return toast.error(
		//     "CPF inválido! Verifique se seus dados cadastrais estão completos.",
		//     {
		//       autoClose: 3000,
		//     }
		//   );
		// }
		// if (hasSubProduct() && newCart.attachment == null) {
		//   setLoadingCheckoutOrder(false);
		//   return toast.error(
		//     "Receita é obrigatória para a compra de Lentes com Grau!"
		//   );
		// }
		// if (cantBuySubProduct()) {
		//   setLoadingCheckoutOrder(false);
		//   return toast.error("Não é possível fechar o pedido somente com Lentes.");
		// }
		// const result = await cartPlugin.checkoutCart();
		// if (result.status == false) {
		//   toast.error(result.error);
		// } else {
		//   toast.success("Pedido sendo processado! Estamos te redirecionando...", {
		//     autoClose: 5000,
		//   });
		//   router.push(`/pedido/${result.data.id}`);
		// }
		// setLoadingCheckoutOrder(false);
	}

	async function handleClearCart() {
		// toast.success("Carrinho sendo limpo!");
		toast.success("Produto excluído com sucesso!");

		newCart.items.forEach((product: any) => {
			tagManager.removeFromCart(router.asPath, product, product.quantity);
		});

		dispatchEvent("changeCart", null);

		const result = await clearCart(CART_SETTING);

		setNewCart(result.data);
	}

	useEffect(() => {
		setNewCart(cart);
	}, [cart])

	return (
		<div className={styles.cartProductsTable}>
			{newCart && newCart?.items?.length > 0 ? (
				<>
					{loadingProducts ? (
						<>
							{Array.from({ length: 24 }).map((product, index) => (
								<ProductItemSkeletonCart key={`productSkeleton-${index}`} />
							))}
						</>
					) : (
						<>
							<CartProductsItem cart={cart} user={user} />
							<div className={styles.total}>
								<p>Total</p>
								<p>R$ {currencyMask(newCart?.totalItems)},00</p>
							</div>
						</>
					)}
				</>
			) : (
				<p className={styles.noProducts}>
					Você não possui produtos no carrinho!
				</p>
			)}

			<div className={styles.buttons}>
				{/* <button type="button" onClick={() => router.push("/produtos")}>
				</button> */}
				<button className={styles.cinza} type="button" onClick={() => router.push(productsUrl)}>
					Continuar comprando
				</button>
				{newCart?.items?.length > 0 && (
					<>
						<button className={styles.cinza} type="button" onClick={() => handleClearCart()}>
							Limpar Carrinho
						</button>
						{/* <button className={styles.black} type="button" onClick={() => router.push("/checkout")}>
							Concluir compra 
						</button> */}
						<button className={styles.submit} type="button" onClick={() => router.push("/checkout")}>
							Concluir compra
						</button>
					</>
				)}
			</div>
		</div>
	);
}

export default CartProductsTable;
