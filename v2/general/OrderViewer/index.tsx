import styles from "./styles.module.scss";
import { useRouter } from "next/router";
import { useState } from "react";
import { onEvent } from "../../../../core/v2/util/use.event";
import { firstImageItemCart, variantItemCart } from "../../../../core/v2/cart/cart.util";
import { currencyMask } from "../../../../core/v2/util/mask";
import { collectionDocument } from "../../../../core/v2/document/document.api";
import { ORDER_SETTING } from "../../../../setting/setting";
import { TrackingModal } from "../TrackingModal";
import { ImageSet } from "../ImageSet";
import { ReOrderModal } from "../../varejo/ReOrderModal";
import Order from "../../../../core/v2/model/order";

export const OrderViewer = ({order, orderTrack, reorderType, reorderRefund}:any) => 
{
	const [openModal, setOpenModal] = useState(false);
    const [reOrderModal, setReOrderModal] = useState(false);
    const router = useRouter();
    order = new Order(order);

	return (
		<>
			<div className={styles.orderViewer}>
				<div className={styles.content}>
				<p className={styles.title}>
					Pedido <span>Nº{order._sequence}</span>
				</p>
				<div className={styles.subtitle}>
					<p>Status do pagamento:</p>
					<span
					className={
						order.statusPayment == "Aguardando"
						? `${styles.status} ${styles.aguardando}`
						: order.statusPayment == "Recusado"
						? `${styles.status} ${styles.recusado}`
						: order.statusPayment == "Cancelado"
						? `${styles.status} ${styles.recusado}`
						: order.statusPayment == "Error"
						? `${styles.status} ${styles.recusado}`
						: `${styles.status}`
					}
					>
					{order.statusPayment}
					</span>
				</div>
				<div className={styles.subtitle}>
					<p>Forma de pagamento:</p>
					<span>{order.paymentMethod.label}</span>
				</div>
				{order.statusPayment != "Pago" &&
					order.paymentMethod.label == "Boleto" && (
					<>
						<div className={styles.subtitle}>
						<p>Boleto para pagamento:</p>
						<a href={order?.boletoUrl} target="_blank" rel="noopener noreferrer">
							Link para o boleto
						</a>
						<span>
							copie e cole o código: <br></br> <br></br>
							{order?.boletoBarcode}
						</span>
						</div>
					</>
					)}
				{order?.statusPayment != "Pago" &&
					order?.paymentMethod?.label == "PIX" &&
					order?.pixQrCode && (
					<>
						<div className={styles.subtitle}>
						<p>QR code para pagamento PIX:</p>
						<img
							src={`http://api.qrserver.com/v1/create-qr-code/?size=100x100&data=${order?.pixQrCode}`}
							alt="PIX"
						/>
						<p>copie e cole o código:</p>
						<span className={styles.pixCode}>
							{order?.pixQrCode?.split(`"`)}
						</span>
						</div>
					</>
					)}
				{order?.paymentMethod?.label == "Cartão de Credito" && (
					<>
					<div className={styles.subtitle}>
						<p>Cartão utilizado:</p>
						<span>
						Nome do titular: <strong>{order?.creditCard?.owner}</strong>
						</span>
						<span>
						Número do cartão:{" "}
						<strong>{order?.creditCard?.cardnumber}</strong>{" "}
						</span>
						<span>
						Expiração: <strong>{order?.creditCard?.expirydate}</strong>{" "}
						</span>
					</div>
					</>
				)}
				<div className={styles.subtitle}>
					<p>Tipo de entrega:</p>
					<span>
					{order?.shipping?.type?.label == "Correios"
						? order?.shipping?.type?.label + " - " + order?.shipping?.label
						: order?.shipping?.type?.label}
					</span>
				</div>
				{order?.trackingCode && (
					<div className={styles.subtitle}>
					<button
						onClick={() => setOpenModal(true)}
						type="button"
						style={{ width: "fit-content" }}
					>
						Código de rastreio - {order?.trackingCode}
					</button>
					</div>
				)}

				<div className={styles.subtitle}>
					<p>Endereço de entrega:</p>
					{order?.shipping?.type?.label == "Retirada no Local" ? (
					<span>{order?.shipping?.label}</span>
					) : (
					<>
						<span>
						{order?.address?.street} nº {order?.address?.housenumber},{" "}
						{order?.address?.district}, {order?.address?.city} -{" "}
						{order?.address?.state} / CEP: {order?.address?.zipcode}
						</span>
					</>
					)}
				</div>
				<div className={styles.subtitle}>
					<p>Produtos</p>
				</div>
				<div className={styles.products}>
					{order?.items &&
					order?.items.map((item: any) => (
						<div key={item.id} className={styles.item}>
						<div className={styles.product}>
							<div className={styles.img}>
							<ImageSet image={firstImageItemCart(item)}/>
							</div>
							<div>
							<div className={styles.productName}>{item?.name}</div>
							<div className={styles.productDetails}>
								{item?.variant[0]?.label}
							</div>
							</div>
						</div>
						<div className={styles.quantity}>
							<span className={styles.value}>
							Quantidade: {item.quantity}
							</span>
						</div>
						<div className={styles.price}>
							{currencyMask(item?.total)}
						</div>
						</div>
					))}
				</div>
				<div className={styles.total}>
					<p>
					Subtotal: <span>{currencyMask(order.totalItems)}</span>
					</p>
					<p>
					Desconto: <span>{currencyMask(order.totalDiscount || 0)}</span>
					</p>
					<p>
					Frete: <span>{currencyMask(order.totalShipping)}</span>
					</p>
					<p>
					Total: <span>{currencyMask(order.total)}</span>
					</p>
				</div>
				{order.validateReorder() && (
					<button
					className={styles.reOrderButton}
					type="button"
					onClick={() => setReOrderModal(true)}
					>
					Solicitar Troca/Devolução
					</button>
				)}
				</div>
			</div>
			{order?.trackingCode && orderTrack && openModal && (
				<TrackingModal
				trackingCode={order.trackingCode}
				trackingStatus={orderTrack}
				setModal={setOpenModal}
				/>
			)}
			{reOrderModal && (
				<ReOrderModal
				reorderType={reorderType}
				reorderRefund={reorderRefund}
				order={order}
				setModal={setReOrderModal}
				/>
			)}
			</>
	);
};