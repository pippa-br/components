import styles from "./styles.module.scss";
import { useState } from "react";
import { AiOutlineCheckCircle } from "react-icons/ai";
import { BsCircle } from "react-icons/bs";
import router from "next/router";
import { AnimatedLoading } from "../AnimatedLoading";

export const CheckoutShipping = ({ userCart, goBack, loadingCart, onSubmit }: any) => 
{
	const [selectedShippingMethod, setSelectedShippingMethod] = useState(
		userCart.shipping || {
		label: "",
		type: { label: "" },
		}
	);

	const [selectedShippingRetirada, setSelectedShippingRetirada] = useState(
		userCart.shipping || {
		label: "",
		type: { label: "" },
		}
	);

	const [selectedFinalShipping, setSelectedFinalShipping] = useState(
		userCart.shipping || {
		label: "",
		type: { label: "" },
		}
	);

	const shippingTransportadora = userCart?.shippingMethods?.full?.filter(
		(shipping: any) => shipping?.type?.value == "transportadora"
	)[0];

	const shippingOnibus = userCart?.shippingMethods?.full?.filter(
		(shipping: any) => shipping?.type?.value == "onibus"
	)[0];

	const [onibus, setOnibus] = useState({});

	function drawShippingMethod(shipping:any)
	{
		switch(shipping.type.value)
		{			
			case 'correios':
				return <p
					onClick={() => {
					setSelectedFinalShipping(shipping);
					setSelectedShippingMethod(shipping);
					}}
					className={
					shipping.label == selectedShippingMethod.label
						? `${styles.shippingOption} ${styles.active}`
						: styles.shippingOption
					}
				>
					<BsCircle className={styles.noCheck} />
					<AiOutlineCheckCircle className={styles.check} />{" "}
					{shipping?.label}
				</p>
			case 'melhorenvio':
				return <p
					onClick={() => {
					setSelectedFinalShipping(shipping);
					setSelectedShippingMethod(shipping);
					}}
					className={
					shipping.label == selectedShippingMethod.label
						? `${styles.shippingOption} ${styles.active}`
						: styles.shippingOption
					}
				>
					<BsCircle className={styles.noCheck} />
					<AiOutlineCheckCircle className={styles.check} />{" "}
					{shipping?.label}
				</p>
			case 'onibus' : 
				return <p
					onClick={() =>
						setSelectedShippingMethod(shipping)
					}
					className={
						selectedShippingMethod.type.value == "onibus"
						? `${styles.shippingOption} ${styles.active}`
						: styles.shippingOption
					}
					>
					<BsCircle className={styles.noCheck} />
					<AiOutlineCheckCircle className={styles.check} /> Entrega em ônibus
				</p>
			case 'retirada' : 
				return <p
					onClick={() =>
						setSelectedShippingMethod({
						label: "",
						type: { label: "Retirada no Local" },
						})
					}
					className={
						selectedShippingMethod.type.label == "Retirada no Local"
						? `${styles.shippingOption} ${styles.active}`
						: styles.shippingOption
					}
					>
					<BsCircle className={styles.noCheck} />
					<AiOutlineCheckCircle className={styles.check} /> Retirar no local - Após 24h da confirmação do pedido
				</p>
			case 'transportadora' : 
				return <p
						onClick={() =>
							setSelectedShippingMethod({
							label: "",
							type: { label: "Transportadora" },
							})
						}
						className={
							selectedShippingMethod.type.label == "Transportadora"
							? `${styles.shippingOption} ${styles.active}`
							: styles.shippingOption
						}
						>
						<BsCircle className={styles.noCheck} />
						<AiOutlineCheckCircle className={styles.check} /> Transportadora
					</p>
			default : return null
		}
	}

	return (
		<div className={styles.checkoutShipping}>
			<div className={styles.shippingOption}>
				<p>
				<b>Enviar para:</b> {userCart.address.street},Nº{" "}
				{userCart.address.housenumber}, {userCart.address.district},{" "}
				{userCart.address.city}, {userCart.address.state}. CEP:{" "}
				{userCart.address.zipcode}{" "}
				</p>
			</div>
			<p className={styles.title}>Selecione o método de entrega:</p>
			<div className={styles.shippingMethods}>
				{userCart?.shippingMethods?.full?.map((shipping: any, index: any) => (
					drawShippingMethod(shipping)
				))}
			</div>

			{selectedShippingMethod?.type?.label == "Retirada no Local" && (
				<>
				<p className={styles.title}>Selecione o local de retirada:</p>
				<div className={styles.shippingMethods}>
					{userCart?.shippingMethods?.full?.map(
					(shipping: any, index: any) =>
						shipping?.type?.label == "Retirada no Local" && (
						<p
							onClick={() => {
							setSelectedFinalShipping(shipping);
							setSelectedShippingRetirada(shipping);
							}}
							className={
							shipping.label == selectedShippingRetirada.label
								? `${styles.shippingOption} ${styles.active}`
								: styles.shippingOption
							}
							key={`${index}-shippingRetirada`}
						>
							<BsCircle className={styles.noCheck} />
							<AiOutlineCheckCircle className={styles.check} />
							{shipping?.name}
						</p>
						)
					)}
				</div>
				</>
			)}

			{selectedShippingMethod?.type?.label == "Transportadora" && (
				<>
				<p className={styles.title}>Informe o nome da transportadora:</p>
				<div className={styles.shippingMethods}>
					<input
					placeholder="Ex: Expresso"
					onChange={(e) => {
						setSelectedFinalShipping({
						...shippingTransportadora,
						label: e.target.value,
						});
						setSelectedShippingRetirada({
						...shippingTransportadora,
						label: e.target.value,
						});
					}}
					/>
				</div>
				</>
			)}

			{selectedShippingMethod?.type?.value == "onibus" && (
				<>
				<p className={styles.title}>Insira os dados abaixo:</p>
				<div className={styles.shippingMethods}>
					<input
					placeholder="Estacionamento"
					style={{ marginBottom: 30 }}
					onChange={(e) => {
						setOnibus({
						...onibus,
						estacionamento: e.target.value,
						});
					}}
					/>
					<input
					placeholder="Nome da empresa de ônibus"
					style={{ marginBottom: 30 }}
					onChange={(e) => {
						setOnibus({
						...onibus,
						nomeDaEmpresaOnibus: e.target.value,
						});
					}}
					/>
					<input
					placeholder="Nome do guia"
					style={{ marginBottom: 30 }}
					onChange={async (e) => {
						setOnibus({
						...onibus,
						nomeDoGuia: e.target.value,
						});
					}}
					/>
				</div>
				</>
			)}

			<div className={styles.buttons}>
				{!loadingCart && (
				<button
					className={styles.cinza}
					type="button"
					onClick={() => router.push("/")}
				>
					Voltar a loja
				</button>
				)}
				{loadingCart ? null : (
				<button
					className={styles.cinza}
					type="button"
					onClick={() => goBack()}
				>
					Voltar
				</button>
				)}
				<button
				disabled={loadingCart && true}
				className={styles.submit}
				type="button"
				onClick={() => onSubmit({ ...selectedFinalShipping, ...onibus })}
				>
				{loadingCart ? <AnimatedLoading /> : "Próximo"}
				</button>
			</div>

		</div>
	);
};
