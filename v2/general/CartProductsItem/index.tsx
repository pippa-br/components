import styles from "./styles.module.scss";
import { BiTrashAlt } from "react-icons/bi";
import { toast } from "react-hot-toast";
import router from "next/router";
import { tagManager } from "../../../../core/v2/util/TagManager";
import { clearCart, delItemCart, setItemCart } from "../../../../core/v2/cart/cart.api";
import { CART_SETTING } from "../../../../setting/setting";
import ICart from "../../../../core/v2/interface/i.cart";
import { useEffect, useState } from "react";
import { dispatchEvent } from "../../../../core/v2/util/use.event";
import { currencyMask } from "../../../../core/v2/util/mask";
import { firstImageItemCart, hasVariantItemCart, variantItemCart } from "../../../../core/v2/cart/cart.util";
import { ImageSet } from "../ImageSet";;

export const CartProductsItem = ({ cart, user, placeholder }: any) => {
    const [newCart, setNewCart] = useState<ICart>(cart);

    async function handleIncrementQuantity(product: any, variant: any, quantity: any) {
        const newData: any = {
            data: {
                product: {
                    referencePath: product.referencePath,
                },
                quantity: quantity + 1,
            },
        };

        // PRODUCT VARIANT
        if (variant) {
            newData.variant = [...variant]
        }

        tagManager.addToCart(router.asPath, product, 1, user);

        const result = await setItemCart(CART_SETTING.merge(newData));

        if (result.status) {
            toast.success("Quantidade alterada com sucesso!");
            setNewCart(result.data);
        }
        else {
            return toast.error(result.error, {
                duration: 3000,
            });
        }
    }

    async function handleDecrementQuantity(product: any, variant: any, quantity: any) {
        const newData: any = {
            data: {
                product: {
                    referencePath: product.referencePath,
                },
                quantity: quantity - 1,
            },
        };

        // PRODUCT VARIANT
        if (variant) {
            newData.variant = [...variant]
        }

        tagManager.removeFromCart(router.asPath, product, 1);

        const result = await setItemCart(CART_SETTING.merge(newData));

        if (result.status) {
            toast.success("Quantidade alterada com sucesso!");
            setNewCart(result.data);
        }
        else {
            return toast.error(result.error, {
                duration: 3000,
            });
        }
    }

	async function handleDeleteProduct(product: any, variant: any) {
		const filteredCartItems = newCart.items.filter(
			(item: any) => item.id != product.id
		);

		setNewCart({ ...newCart, items: filteredCartItems || [] });

		toast.success("Produto excluído com sucesso!");

		tagManager.removeFromCart(router.asPath, product, product.quantity);

		const newData: any = {
			data: {
				product: {
					referencePath: product.referencePath,
				},
			},
		};

		// PRODUCT VARIANT
		if (variant) {
			newData.variant = [...variant]
		}

		const result = await delItemCart(CART_SETTING.merge(newData));

		if (filteredCartItems.length >= 1) {
			return setNewCart(result.data);
		}
		else {
			return router.reload();
		}
	}

    async function handleClearCart() {
        toast.success("Carrinho sendo limpo!");
        // toast.success("Produto excluído com sucesso!");

        newCart.items.forEach((product: any) => {
            tagManager.removeFromCart(router.asPath, product, product.quantity);
        });

        dispatchEvent("changeCart", null);

        const result = await clearCart(CART_SETTING);

        setNewCart(result.data);
    }

    useEffect(() => {
        setNewCart(cart);
    }, [cart])

    return (
        <div className={styles.cardProductsItem}>
            <header>
                <p>Item</p>
                <p>Quantidade</p>
                <p>Preço</p>
                <p></p>
            </header>
            <div className={styles.body}>
                {newCart?.items?.map((item: any) => (
                    <div className={styles.item} key={item.id}>
                        <div className={styles.product}>
                            <div className={styles.img}>
                                <ImageSet width={480} height={655} placeholder={placeholder} image={firstImageItemCart(item)} />
                            </div>
                            <div>
                                <div className={styles.productName}>
                                    {item?.product?.name}
                                </div>
                                <div className={styles.productDetails}>
                                    {hasVariantItemCart(item, 0) && (<p className={styles.productColor}>
                                        Cor: {variantItemCart(item, 0)}
                                    </p>)}
                                    {hasVariantItemCart(item, 1) && (<p className={styles.productColor}>
                                        Tamanho: {variantItemCart(item, 1)}
                                    </p>)}
                                </div>
                            </div>
                        </div>
                        <div className={styles.quantity}>
                            <div>
                                <span
                                    onClick={() =>
                                        handleDecrementQuantity(
                                            item?.product,
                                            item?.variant,
                                            item?.quantity
                                        )
                                    }
                                >
                                    -
                                </span>
                                <span className={styles.value}>{item?.quantity}</span>
                                <span
                                    onClick={() =>
                                        handleIncrementQuantity(
                                            item?.product,
                                            item?.variant,
                                            item?.quantity
                                        )
                                    }
                                >
                                    +
                                </span>
                            </div>
                        </div>
                        <div className={styles.price}>R${currencyMask(item?.total)},00</div>
                        <div className={styles.actions}>
                            <BiTrashAlt
                                onClick={() => handleDeleteProduct(item?.product, item?.variant)}
                                // onClick={() => handleClearCart()}
                            />
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
}