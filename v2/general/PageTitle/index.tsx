import styles from "./styles.module.scss";
import Link from "next/link";

import { IoIosArrowForward } from "react-icons/io";
import { useRouter } from "next/router";

export function PageTitle({ name, parents=null}: any) 
{
    const router = useRouter();
    
    return (
        <div className={styles.container}>
            <div className={styles.breadcrumb}>
                <p onClick={() => router.push("/")} className={styles.title}>Home</p>
                <IoIosArrowForward />
                {parents && parents.map((item:any) => (
                    <>
                        <p onClick={() => router.push(item.url)} className={styles.title}>{item.name}</p>
                        <IoIosArrowForward />
                    </>
                ))                    
                }
                <span>{name}</span>
            </div>
            <p className={styles.pageTitle}>{name}</p>
        </div>
    );
}