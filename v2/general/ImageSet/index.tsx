import Image from "next/image";
import styles from "./styles.module.scss";

interface ImageSetProps {
  image: {
    name: string;
    id: string;
    type: string;
    _url: string;
    _150x150: string;
    _300x300: string;
    _480x480: string;
    _1024x1024: string;
    _1920x1920: string;
  };
  width: number;
  height: number;
  aspectRatio?: string;
  className: string;
  setModal?: (state: boolean) => void;
}

export const ImageSet = ({
  image,
  width = 1920,
  aspectRatio = "2 / 3",
  setModal,
}: ImageSetProps) => {
  const placeholder = `/assets/product_placeholder.png`;

  const imageLoader = () => 
  {
    if(width <= 480 && (image?._300x300 || image?._480x480)) 
    {
        return image?._300x300 || image?._480x480;
    }
    else if(width <= 1024 && image?._1024x1024) 
    {
        return image?._1024x1024;
    }

    return image?._1920x1920 || image?._url || placeholder;
  };

  return (
    <div
      className={styles.imageSet}
      style={{ aspectRatio: aspectRatio }}
      onClick={() => setModal && setModal(true)}
    >
      <Image
        loader={imageLoader}
        src={placeholder}
        layout="fill"
        objectFit="cover"
        quality={100}
        alt={image?.name}
      />
    </div>
  );
};