import { useRouter } from "next/router";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { toast } from "react-hot-toast";
import styles from "./styles.module.scss";
import { GetStaticProps } from "next";
import { getTokenLoginAuth, loginAuth, loginTokenAuth } from "../../../../core/v2/auth/auth.api";
import { AUTH_SETTING } from "../../../../setting/setting";
import { AnimatedLoading } from "../AnimatedLoading";
import ErrorMessage from "../ErrorMessage";

export default function LoginForm() 
{
	const router = useRouter();

	const {
		register,
		formState: { errors },
		handleSubmit,
	} = useForm();

	const [animateLoading, setAnimateLoading] = useState(false);
	const [formType, setFormType] 			  = useState("none");
	const [emailForToken, setEmailForToken]   = useState("");
	const [tokenForLogin, setTokenForLogin]   = useState("");

	async function getEmailToken() 
	{
		const result = await getTokenLoginAuth(AUTH_SETTING.merge({
			email : emailForToken.toLowerCase(),
		}));

		toast.success(result.message);
	}

	async function loginWithToken() 
	{
		const result = await loginTokenAuth(AUTH_SETTING.merge({
			token : tokenForLogin,
		}));

		if(!result.status) 
		{
			return toast.error("Erro! Verifique dos campos digitados.");
		}

		toast.success("Login feito com sucesso!");

		//router.push("/carrinho");
		router.reload();
	}

	async function handleLoginWithEmailAndPassword(formData:any) 
	{
		setAnimateLoading(true);

		const result = await loginAuth(AUTH_SETTING.merge({
			login	 : formData.login.toLowerCase(),
			password : formData.password,
		}));

		setAnimateLoading(false);

		if(!result.status) 
		{
			return toast.error(result.error || "Erro ao fazer login.");
		}

		toast.success("Login feito com sucesso!");

		//router.push("/carrinho");
		router.reload();
	}

	return (
		<div className={styles.container}>
			<div className={styles.content}>
				{formType === "none" && (
				<>
					<div className={styles.title}>Use uma das opções para logar:</div>
					<button type="button" onClick={() => setFormType("onlyEmail")}>
					Entrar apenas com email
					</button>
					<button type="button" onClick={() => setFormType("withPassword")}>
					Entrar com email e senha
					</button>
				</>
				)}

				{formType === "onlyEmail" && (
				<>
					<div className={styles.title}>Por favor informe seu email:</div>
					<input
					style={{ textTransform: "lowercase" }}
					onChange={(e: any) => setEmailForToken(e.target.value)}
					type="email"
					name="email"
					placeholder="Digite o seu email"
					onKeyUp={(e) => {
						if (e.code === "Enter") {
						setFormType("loginWithToken");
						getEmailToken();
						}
					}}
					/>
					<div className={styles.buttons}>
					<button type="button" onClick={() => setFormType("none")}>
						voltar
					</button>
					<button
						onClick={() => {
						setFormType("loginWithToken");
						getEmailToken();
						}}
						type="button"
					>
						entrar
					</button>
					</div>
				</>
				)}

				{formType === "loginWithToken" && (
				<>
					<div className={styles.title}>
					Por favor informe o token que foi enviado para o seu email:
					</div>
					<input
					onChange={(e: any) => setTokenForLogin(e.target.value)}
					onKeyUp={(e) => e.code === "Enter" && loginWithToken()}
					name="email"
					placeholder="Digite seu token"
					/>
					<div className={styles.buttons}>
					<button type="button" onClick={() => setFormType("none")}>
						voltar
					</button>
					<button onClick={() => loginWithToken()} type="button">
						entrar
					</button>
					</div>
				</>
				)}

				{formType === "withPassword" && (
				<>
					<div className={styles.title}>
					Por favor informe seu email e senha:
					</div>
					<input
					style={{ textTransform: "lowercase" }}
					type="email"
					placeholder="Email"
					{...register("login", {
						required: "Este campo é obrigatório!",
						pattern: {
						value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
						message: "Email inválido",
						},
					})}
					/>
					{errors.login && <ErrorMessage message={errors.login.message} />}
					<input
					{...register("password", {
						required: "Este campo é obrigatório!",
						minLength: {
						value: 8,
						message: "Sua senha possui, no mínimo, 8 caracteres.",
						},
					})}
					type="password"
					placeholder="Senha"
					/>
					{errors.password && (
					<ErrorMessage message={errors.password.message} />
					)}
					<div className={styles.buttons}>
					<button type="button" onClick={() => setFormType("none")}>
						voltar
					</button>
					<button
						onClick={handleSubmit(handleLoginWithEmailAndPassword)}
						type="button"
					>
						{animateLoading ? <AnimatedLoading /> : "Entrar"}
					</button>
					</div>
				</>
				)}
			</div>
		</div>
	);
}