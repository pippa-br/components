import styles from "./styles.module.scss";
import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import { AiOutlineCaretDown, AiOutlineCaretUp } from "react-icons/ai";
import { useState } from "react";
import { toast } from "react-hot-toast";
import { checkoutCart, setAddressCart, setCouponCart, setCreditCardCart, setInstallmentsCart, setPaymentMethodCart } from "../../../../core/v2/cart/cart.api";
import { AUTH_SETTING, CART_SETTING, ORDER_SETTING } from "../../../../setting/setting";
import { setUserAuth } from "../../../../core/v2/auth/auth.api";
import ICart from "../../../../core/v2/interface/i.cart";
import IUser from "../../../../core/v2/interface/i.user";
import { dispatchEvent, onEvent } from "../../../../core/v2/util/use.event";
import { tagManager } from "../../../../core/v2/util/TagManager";
import { CheckoutShipping } from "../CheckoutShipping";
import { CheckoutPayment } from "../CheckoutPayment";
import { CheckoutInformation } from "../CheckoutInformation";
import { Stepper, Step, StepLabel, Typography } from "@material-ui/core";
import { firstImageItemCart, hasVariantItemCart, variantItemCart } from "../../../../core/v2/cart/cart.util";
import { currencyMask } from "../../../../core/v2/util/mask";
import { setShippingMethodCart } from "../../../../core/v2/cart/cart.api";
import { ImageSet } from "../ImageSet";
import ErrorMessage from "../ErrorMessage";

export const CheckoutForm = ({ account, logo, paymentMethods, redirectUrl, typeDoc }: any) => 
{
    const router = useRouter();
    const [cart, setCart] = useState<ICart>();
    const [user, setUser] = useState<IUser>();
    const [loadingCart, setLoadingCart] = useState(false);
    const [checkoutHeader, setCheckoutHeader] = useState(true);
    const [activeStep, setActiveStep] = useState<number>(0);
    const [skipped, setSkipped] = useState(new Set());
    const steps = getSteps();
    
    const {
        register,
        handleSubmit,
        setValue,
        formState: { errors },
    } = useForm();

    onEvent("changeCart", (data: any) => 
    {
        setCart(data);
    });

    onEvent("changeUser", (data: any) => 
    {
        setUser(data);
    });

    function getSteps() 
    {
        return ["Informações", "Frete", "Pagamento"];
    }

    function getStepContent(step: any) 
    {
        switch (step) 
        {
            case 0:
                return (
                  <CheckoutInformation
                    loadingCart={loadingCart}
                    cartAddress={cart}
                    typeDoc={typeDoc}
                    onSubmit={onSubmitCartAddress}
                    userData={user}
                  />
                );
            case 1:
                return (
                  <CheckoutShipping
                    loadingCart={loadingCart}
                    goBack={handleBack}
                    onSubmit={onSubmitCartShipping}
                    userCart={cart}
                  />
                );
            case 2:
                return (
                  <CheckoutPayment
                    setLoadingCart={setLoadingCart}
                    loadingCart={loadingCart}
                    onSubmit={onSubmitCheckout}
                    onCreditCardInstallment={handleCreditCardInstallment}
                    onSubmitPayment={onSubmitCartPaymentMethod}
                    paymentMethods={paymentMethods}
                    goBack={handleBack}
                    userCart={cart}
                  />
                );
            default:
              return "Unknown step";
        }
    }

    const isStepSkipped = (step: any) => 
    {
        return skipped.has(step);
    };

    const handleNext = () => 
    {
        let newSkipped = skipped;
        
        if(isStepSkipped(activeStep)) 
        {
            newSkipped = new Set(newSkipped.values());
            newSkipped.delete(activeStep);
        }

        setActiveStep((prevActiveStep) => prevActiveStep + 1);
        setSkipped(newSkipped);
    };

    const handleBack = () => 
    {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    async function handleCreditCardInstallment(installment: any) 
    {
        const newData = {
            data: installment,
        };

        setLoadingCart(true);

        const result = await setInstallmentsCart(CART_SETTING.merge(newData));

        setLoadingCart(false);
      
        if(result.status) 
        {
            setCart(result.data);
        } 
        else 
        {
            toast.error("Ocorreu um erro! Verifique se foi selecionado um parcelamento válido.");
        }        
    }

    const onSubmitCartAddress = async (data: any) => 
    {      
        setLoadingCart(true);

        if(data.cpf)
        {
            const newUserData = {
                document: {
                    referencePath: user?.referencePath,
                },
                data: {
                    cpf: data?.cpf,
                    name: data?.name,
                },
            };

            const result1 = await setUserAuth(AUTH_SETTING.merge(newUserData));

            if(!result1.status) 
            {
                setLoadingCart(false);
                return toast.error(result1.error);
            }

            setUser(result1.data);
        }

        const newData = {
            data: {
                //cpf: data.cpf,
                //name: data.name,
                //phone: data.phone,
                zipcode: data.cep,
                street: data.street,
                housenumber: data.housenumber,
                complement: data.complement,
                district: data.district,
                city: data.city,
                state: data.state,
                country: { id: "br", label: "Brasil", value: "br" },
            },
        };

        const result = await setAddressCart(CART_SETTING.merge(newData));

        if(result.status) 
        {
            setCart(result.data);
            handleNext();
        } 
        else 
        {
            toast.error("Ocorreu um erro! Verifique se os campos foram preenchidos corretamente.");
        }

        setLoadingCart(false);
    };

    const onSubmitCartShipping = async (data: any) => 
    {
        if(!data.id) 
        {
            return toast.error("Selecione uma das formas de entrega!");
        }

        const newData = {
            data: data,
        };

        setLoadingCart(true);

        const result = await setShippingMethodCart(CART_SETTING.merge(newData));

        setLoadingCart(false);

        if(result.data.shipping) 
        {
            setCart(result.data);
            handleNext();
        }
        else 
        {
            toast.error("Ocorreu um erro! Verifique se foi selecionado um método válido.");
        }
    };

    const onSubmitCartPaymentMethod = async (data: any) => 
    {
        const newData = {
            data: data,
        };

        setLoadingCart(true);

        const result = await setPaymentMethodCart(CART_SETTING.merge(newData));

        setLoadingCart(false);

        if(result.data.paymentMethod) 
        {
            setCart(result.data);
        } 
        else 
        {
            toast.error("Ocorreu um erro! Verifique se foi selecionado um método válido.");
        }      
    };

    const onSubmitCheckout = async (data: any) => 
    {
        const creditCardData = {
            data: {
                cardnumber: data.cardnumber,
                owner: data.owner,
                cvv: data.cvv,
                expirydate: data.expirydate,
                docType: { value: "cpf", label: "CPF", id: "cpf", type: "individual" },
                _cpf: data.cpf,
                sameAddressShiping: data.sameAddressShiping,
                address: {
                    zipcode: data?.cep || "",
                    street: data?.street || "",
                    housenumber: data?.housenumber || "",
                    complement: data?.complement || "",
                    district: data?.district || "",
                    city: data?.city || "",
                    state: data?.state || "",
                    country: { id: "br", label: "Brasil", value: "br", selected: true },
                },
            },
        };        

        if(cart?.paymentMethod?.value == "credit_card") 
        {
            setLoadingCart(true);

            const result = await setCreditCardCart(CART_SETTING.merge(creditCardData));

            setLoadingCart(false);

            if(result?.data?.creditCard?.owner) 
            {
                setCart(result.data);
            } 
            else 
            {                
                return toast.error("Erro. Verifique os dados do cartão.");
            }
        }

        setLoadingCart(true);
        
        const result2 = await checkoutCart(ORDER_SETTING.merge({
            cart : {
                referencePath : cart?.referencePath
            }
        }));

        setLoadingCart(false);

        if(!result2.status) 
        {
            toast.error(result2.error);
        } 
        else 
        {
            toast.success("Pedido sendo processado! Estamos te redirecionando...", 
            {
                duration: 2000,
            });

            await dispatchEvent("changeCart", null);
            
            router.push(redirectUrl + '/' + result2.data.id + '?success=true');
        }        
    };

    const onApplyCoupon = async (data: any) => 
    {
        const newData = {
            _code : data.coupon,
        };

        setLoadingCart(true);

        const result = await setCouponCart(CART_SETTING.merge(newData));

        setLoadingCart(false);

        if(result.error) 
        {
            toast.error(result.error);
        }

        setCart(result.data);        
    };

    return (
        <>
        <div className={styles.checkoutForm}>
            <div className={styles.content}>
                <div className={styles.checkoutStepper}>
                    <img
                      onClick={() => router.push("/")}
                      className={styles.logo}
                      src={logo}
                      alt={account?.name}
                    />
                    <Stepper activeStep={activeStep}>
                      {steps.map((label, index) => {
                        const stepProps = {};
                        const labelProps = {};
                        return (
                          <Step key={label} {...stepProps}>
                            <StepLabel {...labelProps}>{label}</StepLabel>
                          </Step>
                        );
                      })}
                    </Stepper>
                    <div>
                      {activeStep === steps.length ? (
                        <div>
                          <Typography className={styles.instructions}>
                            All steps completed - you&apos;re finished
                          </Typography>
                        </div>
                      ) : (
                        <div>
                          <Typography className={styles.instructions}>
                            {getStepContent(activeStep)}
                          </Typography>
                        </div>
                      )}
                    </div>
                </div>
                <div className={styles.checkoutCart}>
                    <div className={styles.checkoutHeader}>
                        <p className={styles.title}>
                          Resumo do pedido:{" "}
                          {!checkoutHeader && (
                            <AiOutlineCaretDown
                              onClick={() => setCheckoutHeader(true)}
                              className={styles.noActive}
                            />
                          )}
                          {checkoutHeader && (
                            <AiOutlineCaretUp
                              onClick={() => setCheckoutHeader(false)}
                              className={styles.active}
                            />
                          )}
                        </p>                
                    </div>   
                    {checkoutHeader && <>
                        <div className={styles.body}>
                          {cart?.items?.length > 0 &&
                            cart?.items.map((item: any) => (
                              <div key={item?.id} className={styles.item}>
                                <div className={styles.product}>
                                  <div className={styles.img}>
                                    <ImageSet width={480} height={655} image={firstImageItemCart(item)} />
                                  </div>
                                  <div className={styles.info}>
                                    <div className={styles.productName}>
                                      {item.product.name}
                                    </div>
                                    {hasVariantItemCart(item, 0) && (
                                      <p className={styles.productColor}>
                                        Cor: {variantItemCart(item, 0)}
                                      </p>
                                    )}
                                    {hasVariantItemCart(item, 1) && (
                                      <p className={styles.productColor}>
                                        Tamanho: {variantItemCart(item, 1)}
                                      </p>
                                    )}
                                    <p className={styles.productQuantity}>
                                      Quantidade: {item.quantity}
                                    </p>
                                  </div>
                                </div>
                                <div className={styles.price}>{currencyMask(item.total)}</div>
                              </div>
                            ))}
                        </div>  
                        <div className={styles.checkoutCoupon}>
                          <label>Cupom:</label>
                          <div>
                            <input
                              defaultValue={cart?.coupon?._code}
                              {...register("coupon", {
                                validate: (value) => value.length >= 5 || "Cupom inválido!",
                              })}
                              type="text"
                              placeholder="Digite aqui seu cupom"
                            />

                            <button type="button" onClick={handleSubmit(onApplyCoupon)}>
                              Aplicar Cupom
                            </button>
                          </div>
                          {errors.coupon && <ErrorMessage message={errors.coupon.message} />}
                        </div>
                        <div className={styles.checkoutResume}>
                          <div className={styles.checkoutSubtotal}>
                            <p>
                              Subtotal <span>{currencyMask(cart?.totalItems)}</span>
                            </p>
                            <p>
                              Desconto <span>{currencyMask(cart?.totalDiscount)}</span>
                            </p>
                            <p>
                              Frete
                              <span>{currencyMask(cart?.shipping?.value || 0)}</span>
                            </p>
                            <p>
                              Juros
                              <span>{currencyMask(cart?.totalInterest || 0)}</span>
                            </p>
                          </div>
                          <div className={styles.checkoutTotal}>
                            <p>
                              Total <span>{currencyMask(cart?.total)}</span>
                            </p>
                          </div>
                        </div>
                    </>}                                                                     
                </div>              
            </div>
            <div className={styles.security}>
                  <p><strong>Pagamento Seguro SSL</strong><br></br>Sua criptografia é protegida por criptografia SSL de 256 bits.</p>
                  <img src="/assets/seguranca.png" alt="Segurança" />
            </div>
        </div>        
        </>
    );
};
