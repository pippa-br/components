import styles from "./styles.module.scss";
import { useRouter } from "next/router";
import InputMask from "react-input-mask";
import { useForm } from "react-hook-form";
import { AiOutlineExclamationCircle } from "react-icons/ai";
import { useEffect, useState } from "react";
import { toast } from "react-hot-toast";
import { clearCart } from "../../../../core/v2/cart/cart.api";
import { AUTH_SETTING, CART_SETTING } from "../../../../setting/setting";
import { logoutAuth, setUserAuth } from "../../../../core/v2/auth/auth.api";
import { validateCpf } from "../../../../core/v2/util/validate";
import { AnimatedLoading } from "../AnimatedLoading";
import { buscaCep } from "../../../../core/v2/util/util";
import IUser from "../../../../core/v2/interface/i.user";
import ErrorMessage from "../ErrorMessage";

export const CheckoutInformation = ({userData, onSubmit, loadingCart, typeDoc}: any) => 
{
    const [address, setAddress] = useState();
    const [newUserData, setNewUserData] = useState<IUser>();
    const [loadingUserAddress, setLoadingUserAddress] = useState(false);
    const router = useRouter();

    const 
    {
        register,
        handleSubmit,
        setValue,
        formState: { errors },
    } = useForm();

    useEffect(() => 
    {
        setNewUserData(userData);
    }, [userData])

    /*async function handleLogout() 
    {
    	  await clearCart(CART_SETTING);
    	  await logoutAuth(AUTH_SETTING);
    	  router.push("/");
    	  toast.success("Logout feito com sucesso!");
    }*/

    /*const onSubmitCartUser = async (data: any) => 
    {
		    if(!validateCpf(data.cpf)) 
        {
			      return toast.error("CPF inválido!");
		    }

        const newData = {
            document: {
                referencePath: userData.referencePath,
            },
            data: data,
        };

        const result = await setUserAuth(AUTH_SETTING.merge(newData));

        setNewUserData(result);
    };*/

    return (
      <div className={styles.checkoutInformation}>
        <p className={styles.title}>Informações de contato</p>
        { newUserData?.name && (
          <div className={styles.avatar}>
            <img src="/assets/avatar.jpeg" alt="Avatar" />
            <div className={styles.info}>
                <p className={styles.nameEmail}>
                  {newUserData?.name} ({newUserData?.email})
                </p>
            </div>
          </div>
        )}
        {(!newUserData?.name || !newUserData[typeDoc]) && (
          <>
            <form
              className={styles.form}
            >
              <div className={styles.formItem}>
                <label>Nome completo</label>
                <input
                  defaultValue={newUserData?.name}
                  type="text"
                  placeholder="Ex: Maria Jose"
                  autoComplete="new-password"
                  {...register("name", {
                    validate: (value) =>
                      value.length >= 8 ||
                      "Seu nome deve possuir 8 caracteres no mínimo!",
                  })}
                />
                {errors.name && <ErrorMessage message={errors.name?.message} />}
              </div>
              <div className={styles.formItem}>
                <label>CPF</label>
                <InputMask
                  {...register("cpf", {
                    validate: (value) => validateCpf(value) || "CPF inválido!",
                  })}
                  defaultValue={newUserData?.cpf}
                  autoComplete="new-off"
                  mask="999.999.999-99"
                  placeholder="Ex: 123.456.789-10"
                  maskChar=""
                />
                {errors.cpf && <ErrorMessage message={errors.cpf?.message} />}
              </div>                      
            </form>
          </>
        )}
        <p className={styles.title}>Endereço de entrega</p>
        <form className={styles.form}>
          <div className={styles.formItem}>
            <label>CEP</label>
            <InputMask
              mask="99999-999"
              placeholder="Ex: 12345-678"
              autoComplete="new-password"
              defaultValue={userData?.address?.zipcode}
              maskChar=""
              onKeyUp={(e: any) =>
                e.target.value.length === 9 &&
                buscaCep(e.target.value, setAddress, setValue)
              }
              {...register("cep", { required: "Este campo é obrigatório!" })}
            />
            {errors.cep && <ErrorMessage message={errors.cep?.message} />}
          </div>
          <div className={styles.formItem}>
            <label>Rua</label>
            <input
              type="text"
              placeholder="Ex: Rua Guaranabara"
              autoComplete="new-password"
              defaultValue={userData?.address?.street}
              {...register("street", { required: "Este campo é obrigatório!" })}
            />
            {errors.street && <ErrorMessage message={errors.street?.message} />}
          </div>
          <div className={styles.formItem}>
            <label>Número</label>
            <input
              type="text"
              defaultValue={userData?.address?.housenumber}
              autoComplete="new-password"
              placeholder="Ex: 17"
              {...register("housenumber", {
                required: "Este campo é obrigatório!",
              })}
            />
            {errors.housenumber && <ErrorMessage message={errors.housenumber?.message} />}
          </div>
          <div className={styles.formItem}>
            <label>Complemento</label>
            <input
              type="text"
              autoComplete="new-password"
              defaultValue={userData?.address?.complement}
              placeholder="Ex: Ap 15B"
              {...register("complement")}
            />
            {errors.complement && <ErrorMessage message={errors.complement?.message} />}
          </div>
          <div className={styles.formItem}>
            <label>Bairro</label>
            <input
              type="text"
              autoComplete="new-password"
              defaultValue={userData?.address?.district}
              placeholder="Ex: Dom Bosco"
              {...register("district", {
                required: "Este campo é obrigatório!",
              })}
            />
            {errors.district && <ErrorMessage message={errors.district?.message} />}
          </div>
          <div className={styles.formItem}>
            <label>Cidade</label>
            <input
              type="text"
              placeholder="Ex: São Paulo"
              defaultValue={userData?.address?.city}
              autoComplete="new-password"
              {...register("city", { required: "Este campo é obrigatório!" })}
            />
            {errors.city && <ErrorMessage message={errors.city?.message} />}
          </div>
          <div className={styles.formItem}>
            <label>Estado</label>
            <input
              type="text"
              autoComplete="new-password"
              defaultValue={userData?.address?.state}
              placeholder="Ex: São Paulo"
              {...register("state", { required: "Este campo é obrigatório!" })}
            />
            {errors.state && <ErrorMessage message={errors.state?.message} />}
          </div>
          <div className={styles.buttons}>
            {!loadingCart && (
              <button
                className={styles.cinza}
                type="button"
                onClick={() => router.back()}
              >
                Voltar a loja
              </button>
            )}
            <button
              className={styles.submitPerfilButton}
              type="button"
              disabled={loadingCart && true}
              onClick={handleSubmit(onSubmit)}
            >
              {loadingCart ? <AnimatedLoading /> : "Próximo"}
            </button>
          </div>
        </form>
      </div>
    );
};
