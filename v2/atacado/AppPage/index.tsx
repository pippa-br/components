import type { AppProps } from "next/app";
import NextNProgress from "nextjs-progressbar";

//GLOBAL STYLES
import { Toaster } from "react-hot-toast";
import { Header } from "../Header";

function AppPage({ Component, pageProps }: AppProps) 
{
    return (
        <>
            <NextNProgress
              color="var(--theme-color);"
              startPosition={0.3}
              stopDelayMs={200}
              height={4}
              showOnShallow={true}
              options={{ easing: "ease", speed: 500 }}
            />
            <Header
              user={pageProps.user}
              cart={pageProps.cart}
              account={pageProps.account}
            />
            <Component {...pageProps} />
            <Toaster
              position="top-center"
              reverseOrder={true}
              containerStyle={{
              fontSize: 16,
              }}
              toastOptions={{
              duration: 2500,
              }}
            />
        </>
    );
}

export { AppPage };
