import router from "next/router";
import { useState } from "react";
import { AiOutlineCloseCircle } from "react-icons/ai";
import { FiTrash } from "react-icons/fi";
import { toast } from "react-hot-toast";
import { clearCart, delItemCart, validateCart } from "../../../../core/v2/cart/cart.api";
import { firstImageItemCart, variantItemCart } from "../../../../core/v2/cart/cart.util";
import { currencyMask } from "../../../../core/v2/util/mask";
import { tagManager } from "../../../../core/v2/util/TagManager";
import { dispatchEvent } from "../../../../core/v2/util/use.event";
import { CART_SETTING } from "../../../../setting/setting";
import { AnimatedLoading } from "../../general/AnimatedLoading";
import styles from "./styles.module.scss";
import { ImageSet } from "../../general/ImageSet";

export const CartModal: React.FC<any> = ({ openModal, cart, user }) => 
{
	const [loadingCart, setLoadingCart] = useState(false);
	const [newCart, setNewCart] = useState(cart);

	async function handleDeleteProduct(product: any, variant: any) 
	{
		const newData = {
			data: {
				product: {
				referencePath: product.referencePath,
				},
				variant: variant,
			},
		};

		const result = await delItemCart(CART_SETTING.merge(newData));

		setNewCart(result.data);

		dispatchEvent("changeCart", result.data);

		toast.success("Produto excluído com sucesso!");
	}

	const onSubmitCheckout = async () => 
	{
		setLoadingCart(true);

		const result = await validateCart(CART_SETTING);

		setLoadingCart(false);

		if(!result.status) 
		{
			let message = '';

			if(typeof result.error == 'object')
			{
				for(const key in result.error)
				{
					for(const item of result.error[key])
					{
						message += item + '\n';
					}	
				}
			}
			else
			{
				message = result.error;
			}

			return toast.error(message,
			{
				duration: 3000,
			});
		} 
		else 
		{
			openModal(false);
			router.push(`/checkout`);
		}
	};

	async function handleClearCart() 
	{
		toast.success("Carrinho sendo limpo!");

		if(newCart)
		{
			newCart.items.forEach((product:any) => {
				tagManager.removeFromCart(router.asPath, product, product.quantity);
			});	
		}

		dispatchEvent("changeCart", null);

		const result = await clearCart(CART_SETTING);

		setNewCart(result.data);
	}

	return (
		<div className={styles.cartModal} onClick={() => openModal(false)}>
			<div className={styles.content} onClick={(e) => e.stopPropagation()}>
				<div className={styles.top}>
					<AiOutlineCloseCircle
						onClick={() => {
						openModal(false);
						}}
						className={styles.cartCloseModal}
					/>
					<p>
						Seu carrinho (
						{newCart?.items?.length > 1
						? newCart?.items?.length + " itens"
						: newCart?.items?.length + " item"}
						)
					</p>
				</div>
				<div className={styles.cart}>
					{newCart?.items?.map((item: any) => (
						<>
						<div className={styles.cartProduct} key={item.id}>
							<div className={styles.image}>
								<ImageSet image={firstImageItemCart(item)} />
							</div>
							<div className={styles.info}>
							<p className={styles.productName}>{item?.product?.name}</p>
							<p className={styles.productColor}>
								Cor: {variantItemCart(item, 0)}
							</p>
							<p className={styles.productSize}>
								Tamanho: {variantItemCart(item, 1)}
							</p>
							<p className={styles.productQuantity}>
								Quantidade: {item.quantity}
							</p>
							<p className={styles.productPrice}>
								Preço: {currencyMask(item.total)}
							</p>
							{/*
							<table className={styles.productSize}>
								<tbody>
								{item?.quantityTable.data &&
									objectParser(item?.quantityTable.data).map(
									(objectName: any, index: any) => (
										<tr key={"sizeP-" + index}>
										<td>{objectName}</td>
										<td>
											{item?.quantityTable.data[objectName].quantity}
										</td>
										</tr>
									)
									)}
								<tr>
									<td>Total</td>
									<td>{item.quantity}</td>
								</tr>
								</tbody>
							</table>
									*/}

							{/* <p className={styles.productQuantity}>
								Quantidade: <span>{item.quantity}</span>
							</p> */}
							</div>
							<FiTrash
							onClick={() =>
								handleDeleteProduct(item.product, item?.variant)
							}
							/>
						</div>
						<div className={styles.line}></div>
						</>
					))}

					<p className={styles.subtotal}>
						Subtotal: <span>{currencyMask(newCart?.totalItems || 0)}</span>
					</p>
					<div className={styles.line}></div>

					<button
						type="button"
						onClick={() => handleClearCart()}
						className={styles.actionButton}
					>
						Limpar carrinho
					</button>

					<div className={styles.line}></div>

					<button
						type="button"
						onClick={() => openModal(false)}
						className={styles.actionButton}
					>
						Continuar comprando
					</button>

					<div className={styles.line}></div>

					{user && cart?.items?.length > 0 && (
						<button type="button" onClick={() => onSubmitCheckout()}>
							{loadingCart ? <AnimatedLoading /> : <>Checkout</>}
						</button>
					)}
				</div>
			</div>
		</div>
	);
};
