import styles from "./styles.module.scss";
import { GiShoppingCart } from "react-icons/gi";
import { useState } from "react";
import { CartModal } from "../CartModal";
import router from "next/router";
import { AiOutlineMenu } from "react-icons/ai";
import { FiLogOut } from "react-icons/fi";
import { HeaderMenuModal } from "../HeaderMenuModal";
import { clearEvent, dispatchEvent, onEvent } from "../../../../core/v2/util/use.event";
import { AUTH_SETTING, CART_SETTING } from "../../../../setting/setting";
import { getCart } from "../../../../core/v2/cart/cart.api";
import { getLoggedAuth, logoutAuth } from "../../../../core/v2/auth/auth.api";
import ICart from "../../../../core/v2/interface/i.cart";
import IUser from "../../../../core/v2/interface/i.user";

export const Header = ({account}:any) => 
{
  	const [isCartModalOpen, setIsCartModalOpen] = useState(false);
  	const [isMenuModalOpen, setIsMenuModalOpen] = useState(false);
  	const [user, setUser] = useState<IUser>();
	const [cart, setCart] = useState<ICart>();

	// IMPORTANTE
	clearEvent();

	getLoggedAuth(AUTH_SETTING, (data:any) => 
	{
		dispatchEvent('changeUser', data);
	});

	getCart(CART_SETTING, (data:any) => 
	{
		dispatchEvent('changeCart', data);
	});
  
	onEvent("changeCart", (data:any) => 
	{
		setCart(data);
	});
  
	onEvent("changeUser", (data:any) => 
	{
		  setUser(data);
	});

  	async function handleLogout() 
	{
    	await logoutAuth(AUTH_SETTING);
    	router.push("/login");
  	}

  	return (
		<>
		<header className={styles.header}>
			<div className={styles.content}>
			<AiOutlineMenu
				onClick={() => setIsMenuModalOpen(true)}
				className={styles.burguerMenu}
			/>
			<div className={styles.logo} onClick={() => router.push("/home")}>
				<img src={account?.logoLogin?._url} alt="" />
			</div>
			<div className={styles.menu}>
				<li onClick={() => router.push("/home")}>Produtos</li>
				<li onClick={() => router.push("/loja/devolucoes")}>Devoluções</li>
				<li onClick={() => router.push("/loja/politica-de-privacidade")}>Política de privacidade</li>
				<li onClick={() => router.push("/meus-dados")}>Meus dados</li>
				<li onClick={() => router.push("/meus-pedidos")}>Meus pedidos</li>
				<FiLogOut
				className={styles.logoutIcon}
				onClick={() => handleLogout()}
				title="Fazer logout"
				/>
				<div
				className={styles.cartIcon}
				onClick={() => setIsCartModalOpen(!isCartModalOpen)}
				>
				<GiShoppingCart />
				<span>{cart?.items?.length || 0}</span>
				</div>
			</div>
			</div>
		</header>
		{isCartModalOpen && (
			<CartModal openModal={setIsCartModalOpen} cart={cart} user={user} />
		)}
		{isMenuModalOpen && <HeaderMenuModal setModal={setIsMenuModalOpen} />}
		</>
  	);
};
