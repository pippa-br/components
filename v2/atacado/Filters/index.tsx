import styles from "./styles.module.scss";
import Select from "react-select";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

export const Filters = ({ filters, categories, onCategoryClick, onColorClick, onSizeClick }: any) => 
{
    const [defaultColorFilter, setDefaultColorFilter] = useState();
    const [defaultSizeFilter, setDefaultSizeFilter] = useState();
    const [defaultCategoryFilter, setDefaultCategoryFilter] = useState();
    const router = useRouter();
    const querys = useRouter().query;

    const getCategoryOptions = (categories: any) => 
    {
        const newOptions = categories.map((category: any) => 
        {
            return {
                value : category.name,
                label : category.name,
                model : category,
            };
        });

        return newOptions;
    };

  const customSelectStyles: any = {
    control: (base: any, state: any) => ({
      ...base,
      borderRadius: "none",
      borderColor: state.isFocused
        ? "var(--theme-color)"
        : "var(--theme-color)",
      boxShadow: state.isFocused ? "none" : null,
      "&:hover": {
        borderColor: "var(--theme-color)",
      },
      color: "#000",
      backgroundColor: state.hasValue ? "var(--theme-color)" : null,
    }),
    singleValue: (styles: any) => ({ ...styles, color: "#fff" }),
    indicatorsContainer: (styles: any, state: any) => ({
      ...styles,

      color: state.hasValue ? "#fff" : "#000",
    }),
    indicatorSeparator: () => null,
  };

  useEffect(() => {
    if (querys.color || querys.size || querys.category) {
      setDefaultColorFilter(
        filters?.colors?.items.filter(
          (color: any) => color?.value == querys.color
        )
      );
      setDefaultSizeFilter(
        filters?.sizes?.items.filter((size: any) => size?.value == querys.size)
      );
      setDefaultCategoryFilter(
        getCategoryOptions(
          categories.filter(
            (category: any) => category.name == router.query.category
          )
        )
      );
    } else {
      setDefaultColorFilter(undefined);
      setDefaultSizeFilter(undefined);
      setDefaultCategoryFilter(undefined);
    }
  }, [router]);

  return (
    <div className={styles.container}>
      <Select
        placeholder="Cores"
        options={filters?.colors?.items}
        styles={customSelectStyles}
        isClearable
        value={defaultColorFilter || null}
        onChange={(e: any) => onColorClick(e)}
      />
      <Select
        placeholder="Tamanhos"
        options={filters?.sizes?.items}
        styles={customSelectStyles}
        isClearable
        value={defaultSizeFilter || null}
        onChange={(e: any) => onSizeClick(e)}
      />
      <Select
        placeholder="Categorias"
        options={getCategoryOptions(categories)}
        styles={customSelectStyles}
        isClearable
        value={defaultCategoryFilter}
        onChange={(e: any) => onCategoryClick(e)}
      />
    </div>
  );
};
