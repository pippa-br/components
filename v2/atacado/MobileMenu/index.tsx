import { useRouter } from "next/router";
import styles from "./styles.module.scss";

export const MobileMenu = () => {
  const router = useRouter();

  return (
    <div className={styles.container}>
      <div className={styles.content}>
        <img
          // onClick={() => dispatch(toggleMobileMenu())}
          src="/assets/icons/times-circle.svg"
          className={styles.closeMobileMenuButton}
          alt="close"
        />
        <div className={styles.mobileMenu}>
          <p
            onClick={() => {
              router.push("/perfil");
              // dispatch(toggleMobileMenu());
            }}
          >
            <img className="menu-item" src="/assets/icons/user.svg" alt="" />
            Fazer login
          </p>
        </div>
      </div>
    </div>
  );
};
