import styles from "./styles.module.scss";
import React, { useState } from "react";
import { BiCartAlt } from "react-icons/bi";
import { AddToCartModal } from "../AddToCartModal";
import { currencyMask } from "../../../../core/v2/util/mask";
import { findDuplicates } from "../../../../core/v2/util/util";
import { ImageSet } from "../../general/ImageSet";
import {
  firstProductImage,
  productPrice,
} from "../../../../core/v2/product/product.util";

export const ProductItem: React.FC<any> = ({ product, productInfo = true }) => {
  const [modal, setModal] = useState(false);

  return (
    <>
      {firstProductImage(product) ? (
        <div className={styles.productItem}>
          <ImageSet
            className={styles.productImage}
            setModal={setModal}
            width={600}
            height={1500}
            image={firstProductImage(product)}
          />
          <p className={styles.productName}>{product?.name}</p>
          <p className={styles.productSKU}>Referência: {product?.code}</p>
          {productInfo ? (
            <>
              <div className={styles.productSize}>
                {findDuplicates(product?.variant[1]?.items)?.map(
                  (size: any) => (
                    <p key={size.id}>{size?.label}</p>
                  )
                )}
              </div>
              <div className={styles.productColor}>
                {product?.variant[0]?.items?.map((color: any) => (
                  <p key={color.id}>{color.label}</p>
                ))}
              </div>
            </>
          ) : null}

          <p className={styles.productPrice}>{productPrice(product)}</p>
          <button type="button" onClick={() => setModal(true)}>
            <BiCartAlt /> Adicionar ao carrinho
          </button>
        </div>
      ) : null}

      {modal && <AddToCartModal setModal={setModal} product={product} />}
    </>
  );
};
