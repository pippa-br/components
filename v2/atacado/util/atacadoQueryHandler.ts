export const atacadoQueryHandler = (
  router: any,
  categories: any,
  colors: any,
  sizes: any
) => {
  const filters: any = [];
  const colorSizeFilters: any = [];

  if (!router.query) {
    return filters;
  }

  const colorValue = colors?.filter(
    (color: any) => color.value == router.query.color
  );
  const sizeValue = sizes?.filter(
    (size: any) => size.value == router.query.size
  );

  if (router.query.category) {
    const pageCategory = categories.filter(
      (category: any) => category.name == router.query.category
    );

    colorSizeFilters.push({
      field: "indexes.categoriesxcolorxsize",
      operator: "combine",
      value: [
        {
          referencePath: pageCategory[0].referencePath,
        },
      ],
    });
  }

  if (router.query.color) {
    colorSizeFilters.push({
      field: "indexes.categoriesxcolorxsize",
      operator: "combine",
      value: [...colorValue],
    });
  }

  if (router.query.size) {
    colorSizeFilters.push({
      field: "indexes.categoriesxcolorxsize",
      operator: "combine",
      value: [...sizeValue],
    });
  }

  if (router.query.color || router.query.size || router.query.category) {
    filters.push(...colorSizeFilters);
  }

  return filters;
};
