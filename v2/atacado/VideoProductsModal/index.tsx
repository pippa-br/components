import { useState } from "react";
import { ProductItem } from "../ProductItem";
import styles from "./styles.module.scss";


export const VideoProductsModal: React.FC<any> = ({
  openModal,
  video,
  cart,
  user,
}) => {
  return (
    <div className={styles.videoProductsModal}>
      <div className={styles.content}>
        <span
          className={styles.closeVideoProductModal}
          onClick={() => openModal(false)}
        >
          X
        </span>
        <div className={styles.productsList}>
          {video.products.map((product: any) => (
            <ProductItem
              priceViewer={video?.store?.priceViewer}
              key={product.id}
              product={product}
              cart={cart}
              user={user}
            />
          ))}
        </div>
      </div>
    </div>
  );
};
