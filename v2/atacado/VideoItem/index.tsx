/* eslint-disable @next/next/no-html-link-for-pages */
import styles from "./styles.module.scss";
import React from "react";
import moment from "moment";

export const VideoItem: React.FC<any> = ({ video }) => {
  const newDate = moment(video?.date, "DD-MM-YYYY HH:mm");
  const diff = moment();
  const newDate2 = moment(video?.date, "DD/MM/YYYY").format("DD/MM/YYYY");
  const newLiveDate = moment(video?.date, "DD/MM/YYYY").format("DD/MM");
  const newLiveDate2 = moment(video?.date, "DD/MM/YYYY HH:mm:ss").hours();

  return (
    <div className={styles.storeLiveItem}>
      <a href={`/video/${video.id}`} className={styles.storeImage}>
        <img src={video?.thumb?._url} alt="Próximo video" />
        {/* <span>
          {moment.duration(newDate.diff(diff, "hours"), "hours").humanize(true)}
        </span> */}
        <div>
          {/* <p>{video?.store?.name}</p> */}
          {/*  <p className={styles.date}>
            {newLiveDate} às {newLiveDate2}h
          </p> */}

          <p className={styles.date}>{video?.name}</p>

          <div
            className={styles.description}
            dangerouslySetInnerHTML={{ __html: video?.description }}
          />
        </div>
      </a>
      {/* <p className={styles.storeName}>
        {video?.store?.name}
        <span>{newDate2}</span>
      
      </p>
      <div
        className={styles.storeDescription}
        dangerouslySetInnerHTML={{ __html: video?.description }}
      ></div> */}
    </div>
  );
};
