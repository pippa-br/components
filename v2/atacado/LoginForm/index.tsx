import styles from "./styles.module.scss";
import { useRouter } from "next/router";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { toast } from "react-hot-toast";
import { loginAuth } from "../../../../core/v2/auth/auth.api";
import { AUTH_SETTING } from "../../../../setting/setting";
import { AnimatedLoading } from "../../general/AnimatedLoading";
import ErrorMessage from "../../general/ErrorMessage";

export const LoginForm = ({ account }: any) => 
{
	const router = useRouter();
	const [animateLoading, setAnimateLoading] = useState(false);

	const {
		register,
		formState: { errors },
		handleSubmit,
	} = useForm<any>();

	const loginForm = async (formData: any) => 
	{
		setAnimateLoading(true);

		const result = await loginAuth(AUTH_SETTING.merge(formData).merge({
			where: [
			{
				field    : "status",
				operator : "==",
				value    : true,
			}]
		}));

		if(result.status === false) 
		{
			setAnimateLoading(false);
			return toast.error(result.error || "Erro ao fazer login.");
		}

		setAnimateLoading(false);

		router.push("/");

		toast.success("Login feito com sucesso!");
	};

	return (
		<div className={styles.loginPage}>
			<div className={styles.content}>
				<div className={styles.left}>
				<img onClick={() => router.push("/login")}
					className={styles.logo}
					src={account.logoLogin._url}
					alt="Logo"
				/>
				<p className={styles.title}>Login</p>
				<p className={styles.subtitle}>Entre na sua conta</p>
				<form>
					<div className={styles.formItem}>
					<label>Email</label>
					<input
						type="email"
						{...register("login", {
						required: "Este campo é obrigatório!",
						pattern: {
							value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
							message: "Email inválido",
						},
						})}
					/>
					{errors.login && <ErrorMessage message={errors.login.message} />}
					</div>
					<div className={styles.formItem}>
					<label>Senha</label>
					<input
						type="password"
						{...register("password", {
						required: "Este campo é obrigatório!",
						minLength: {
							value: 8,
							message: "Sua senha possui, no mínimo, 8 caracteres.",
						},
						})}
					/>
					{errors.password && (
						<ErrorMessage message={errors.password.message} />
					)}
					</div>
				</form>
				<div className={styles.formForgot}>
					<a href="/esqueci-a-senha">Esqueci a senha</a>
				</div>
				<div className={styles.buttons}>
					<button type="button" onClick={handleSubmit(loginForm)}>
					{animateLoading ? <AnimatedLoading /> : "Entrar"}
					</button>
				</div>
				<span className={styles.loginButton}>
					Ainda não tem conta?
					<a href="/cadastro">Clique aqui e faça seu cadastro</a>
				</span>
				</div>
				<div className={styles.right}>
					<img src={account.background_login[0]._url} alt="Imagem" />
				</div>
			</div>
		</div>
	);
};
