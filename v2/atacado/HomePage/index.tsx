import { useState, useEffect } from "react";
import type { GetStaticProps, NextPage } from "next";
import { useRouter } from "next/router";
import InfiniteScroll from "react-infinite-scroll-component";
import withHeader from "../../../../utils/withHeader";
import styles from "./styles.module.scss";
import { Filters } from "../Filters";
import { ProductList } from "../ProductList";
import {
  CATEGORY_SETTING,
  COLOR_VARIANT_SETTING,
  PRODUCT_SETTING,
  SIZE_VARIANT_SETTING,
} from "../../../../setting/setting";
import {
  collectionDocument,
  getDocument,
} from "../../../../core/v2/document/document.api";
import { calls } from "../../../../core/v2/util/call.api";
import { collectionProduct } from "../../../../core/v2/product/product.api";

const HomePage = ({ categories, filters }:any) => 
{
    const router = useRouter();
    const [loadingNewProducts, setLoadingNewProducts] = useState(false);
    const [category, setCategory] = useState<any>();
    const [color, setColor] = useState<any>();
    const [size, setSize] = useState<any>();
    const [orderBy, setOrderBy] = useState<any>("postdate");
    const [asc, setAsc] = useState<any>(false);
    const [newProducts, setNewProducts] = useState<any>([]);
    const [hasMore, setHasMore] = useState(true);
    const [reloadProducts, setReloadProducts] = useState(new Date());
    const [searchInput, setSearchInput] = useState("");

    // RESET PRODUTOS AO TROCAR DE LOJA
    useEffect(() => 
    {
        setNewProducts([]);
        setReloadProducts(new Date());
    }, [router.asPath]);

    useEffect(() => 
    {
        async function fetchAPI() 
        {
            setLoadingNewProducts(true);
            const result = await loadProducts();
            setNewProducts([...newProducts, ...result.collection]);
            setLoadingNewProducts(false);
        }
        fetchAPI();
    }, [reloadProducts]);

    async function categoryClick(e:any) 
    {
        const querys = router.query;
      
        if(e)
        {
            router.push({ query: { ...querys, category: e.value } })
            setCategory(e.model);
        }
        else
        {
            setCategory(null);
            router.push({ query: { ...querys, category: '' } })
        }

        setNewProducts([]);
        setHasMore(true);
        setReloadProducts(new Date());
    }

    async function colorClick(e:any) 
    {
        const querys = router.query;
      
        if(e)
        {
            setColor(e);
            router.push({ query: { ...querys, color: e.value } })            
        }
        else
        {
            setColor(null);
            router.push({ query: { ...querys, color: '' } })
        }

        setNewProducts([]);
        setHasMore(true);
        setReloadProducts(new Date());
    }

    async function sizeClick(e:any) 
    {
        const querys = router.query;
      
        if(e)
        {
            setSize(e);
            router.push({ query: { ...querys, size: e.value } })            
        }
        else
        {
            setSize(null);
            router.push({ query: { ...querys, size: '' } })
        }

        setNewProducts([]);
        setHasMore(true);
        setReloadProducts(new Date());
    }

    async function handleSearchedProducts(input:any) 
    {
        setSearchInput(input);
        setNewProducts([]);
        setHasMore(true);
        setReloadProducts(new Date());
    }

    async function handleLoadMoreProducts() 
    {
        setReloadProducts(new Date());
    }

    async function loadProducts() 
    {
        const params: any = {
            accid: "default",
            appid: "product",
            colid: "documents",
            perPage: 24,
            orderBy: orderBy,
            asc: asc,
            map: true,
            mapItems: {
                referencePath: "default/product/grids/ZUnonLswnK67KE4lnDmg",
            },
            where: [{
                field: "published",
                operator: "==",
                value: true,
            }]
        };

        if(category && category?.id) 
        {
            params.where.push({
              field: "indexes.categoriesxcolorxsize",
              operator: "combine",
              value: [{
                referencePath: category.referencePath,
              }],
          });
        }

        if(color && color?.id) 
        {
            params.where.push({
              field: "indexes.categoriesxcolorxsize",
              operator: "combine",
              value: [color],
          });
        }

        if(size && size?.id) 
        {
            params.where.push({
              field: "indexes.categoriesxcolorxsize",
              operator: "combine",
              value: [size],
          });
        }

        if(searchInput && searchInput.length > 1) 
        {
            params.where.push({
                field: "search",
                operator: "array-contains",
                value: searchInput.toLowerCase(),
            });
        }

        if(newProducts && newProducts.length > 0) 
        {
            params.startAfter = {
              referencePath: newProducts[newProducts.length - 1]?.referencePath,
            };
        }

      const result: any = await collectionProduct(PRODUCT_SETTING.merge(params));

      setHasMore(result.total != result.count);

      return result;
    }

  return (
    <div className={styles.homePage}>
      <div className={styles.content}>
        <Filters categories={categories} filters={filters} onCategoryClick={categoryClick} onColorClick={colorClick} onSizeClick={sizeClick}/>
        <InfiniteScroll
              dataLength={newProducts?.length}
              next={handleLoadMoreProducts}
              hasMore={hasMore}
              loader={
                <div className={styles.loadingWarning}>
                  <span>Carregando mais produtos...</span>
                </div>
              }
              endMessage={
                <div className={styles.loadingWarning}>
                  <span className="noMoreProductsToLoad">
                    Todos os produtos foram carregados
                  </span>
                </div>
              }
            >
              <div className={styles.productsList}>
                {newProducts.length > 0 &&
                  <ProductList products={newProducts} />
                }
                {!loadingNewProducts && newProducts.length == 0 && (
                  <p className={styles.noProducts}>
                    Não há produtos para essa busca.
                  </p>
                )}
              </div>
          </InfiniteScroll>      
      </div>
    </div>
  );
};

const getStaticProps: GetStaticProps = () => withHeader(async (props: any) => 
{
    const [categories, sizes, colors] = await calls(      
        collectionDocument(CATEGORY_SETTING),
        getDocument(SIZE_VARIANT_SETTING),
        getDocument(COLOR_VARIANT_SETTING)
    );

    return {
        props: {
            categories: categories?.collection || [],
            filters: {
                sizes: sizes?.data || null,
                colors: colors?.data || null,
            },
        },
        revalidate: 5,
    };
  });

  export { getStaticProps as GetStaticProps, HomePage}
