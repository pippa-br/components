import styles from "./styles.module.scss";
import router from "next/router";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { toast } from "react-hot-toast";
import { addUserAuth, loginAuth } from "../../../../core/v2/auth/auth.api";
import { AUTH_SETTING } from "../../../../setting/setting";
import { AnimatedLoading } from "../../general/AnimatedLoading";
import { validateCNPJ } from "../../../../core/v2/util/validate";
import InputMask from "react-input-mask";
import { buscaCep } from "../../../../core/v2/util/util";
import ErrorMessage from "../../general/ErrorMessage";

export const SignInForm = ({ account }: any) => {
  const [animateLoading, setAnimateLoading] = useState(false);
  const [address, setAddress] = useState<any>();
  const [checked, setChecked] = useState(true);
  const [status, setStatus] = useState(false);

  const handleChangeCheckbox = (event: any) => {
    setChecked(event.target.checked);
  };

  const {
    register,
    formState: { errors },
    handleSubmit,
    watch,
    setValue,
    reset,
  } = useForm<any>();

  const passwordWatcher = watch("password", "");

  const registerForm = async (formData: any) => {
    setAnimateLoading(true);

    const newData = {
      cnpj: formData.cnpj,
      confirmPassword: formData.confirmPassword,
      consent: formData.consent,
      email: formData.email,
      name: formData.name,
      password: formData.password,
      phone: formData.phone,
      storeName: formData.storeName,
      address: {
        zipcode: formData.cep,
        street: formData.street,
        housenumber: formData.housenumber,
        complement: formData.complement,
        district: formData.district,
        city: formData.city,
        state: formData.state,
        country: { id: "br", label: "Brasil", value: "br", selected: true },
      },
    };

    const result = await addUserAuth(
      AUTH_SETTING.merge({
        accid: "default",
        appid: "users",
        platform: "ecom",
        form: {
          referencePath: "default/users/forms/5BMnB6DwQDHZ0ir8zatM",
        },
        data: newData,
      })
    );

    setAnimateLoading(false);

    if (result.status === false) {
      return toast.error(result.error);
    }

    setStatus(true);

    /*const result2 = await loginAuth(AUTH_SETTING.merge({
				login: formData.email,
				password: formData.password,
				where: [
				{
					field    : "status",
					operator : "==",
					value    : true,
				}]								
			}));

			if(result2.status === false) 
			{
				setAnimateLoading(false);
				return toast.error(result2.error || "Erro ao fazer login.");
			}*/

    //reset();

    //router.push("/");
  };

  return (
    <div className={styles.registerPage}>
      <div className={styles.content}>
        <div className={styles.left}>
          <img
            onClick={() => router.push("/login")}
            className={styles.logo}
            src={account.logoLogin._url}
            alt="Logo"
          />
          {status ? (
            <>
              <p className={styles.statusInfo}>
                Sua conta foi criada com sucesso e está em analise! Em breve
                você receberá um e-mail de confirmação!
              </p>
            </>
          ) : (
            <>
              <p className={styles.title}>Faça seu cadastro</p>
              <p className={styles.subtitle}>Preencha os campos abaixo</p>
              <form>
                <div className={styles.formItem}>
                  <label>Nome</label>
                  <input
                    type="text"
                    {...register("name", {
                      required: "Este campo é obrigatório!",
                    })}
                  />
                  {errors.name && (
                    <ErrorMessage message={errors.name.message} />
                  )}
                </div>

                <div className={styles.formItem}>
                  <label>Celular (whatsapp)</label>
                  <InputMask
                    mask="(99) 99999-9999"
                    maskChar=""
                    {...register("phone", {
                      validate: (value) =>
                        value.length >= 15 || "Verifique seu celular!",
                    })}
                  />
                  {errors.phone && (
                    <ErrorMessage message={errors.phone.message} />
                  )}
                </div>
                <div className={styles.formItem}>
                  <label>Email</label>
                  <input
                    type="email"
                    {...register("email", {
                      required: "Este campo é obrigatório!",
                      pattern: {
                        value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                        message: "Email inválido",
                      },
                    })}
                  />
                  {errors.email && (
                    <ErrorMessage message={errors.email.message} />
                  )}
                </div>
                <div className={styles.formItem}>
                  <label>CNPJ</label>
                  <InputMask
                    {...register("cnpj", {
                      validate: (value) =>
                        validateCNPJ(value) || "CNPJ inválido!",
                    })}
                    mask="99.999.999/9999-99"
                    maskChar=""
                  />
                  {errors.cnpj && (
                    <ErrorMessage message={errors.cnpj.message} />
                  )}
                </div>
                <div className={styles.formItem}>
                  <label>Nome da loja</label>
                  <input
                    type="text"
                    {...register("storeName", {
                      required: "Este campo é obrigatório!",
                    })}
                  />
                  {errors.storeName && (
                    <ErrorMessage message={errors.storeName.message} />
                  )}
                </div>
                <div className={styles.formItem}>
                  <label>CEP</label>
                  <InputMask
                    mask="99999-999"
                    maskChar=""
                    onKeyUp={(e: any) =>
                      e.target.value.length === 9 &&
                      buscaCep(e.target.value, setAddress, setValue)
                    }
                    {...register("cep", {
                      required: "Este campo é obrigatório!",
                    })}
                  />
                  {errors.cep && <ErrorMessage message={errors.cep.message} />}
                </div>
                <div className={styles.formItem}>
                  <label>Endereço</label>
                  <input
                    defaultValue={address?.logradouro}
                    type="text"
                    {...register("street", {
                      required: "Este campo é obrigatório!",
                    })}
                  />
                  {/* {errors.street && (
									<ErrorMessage message={errors.street.message} />
								)} */}
                </div>
                <div className={styles.formItem}>
                  <label>Número</label>
                  <input
                    type="text"
                    autoComplete="new-password"
                    {...register("housenumber", {
                      required: "Este campo é obrigatório!",
                    })}
                  />
                  {errors.housenumber && (
                    <ErrorMessage message={errors.housenumber.message} />
                  )}
                </div>
                <div className={styles.formItem}>
                  <label>Complemento</label>
                  <input type="text" {...register("complement")} />
                  {errors.complement && (
                    <ErrorMessage message={errors.complement.message} />
                  )}
                </div>
                <div className={styles.formItem}>
                  <label>Bairro</label>
                  <input
                    defaultValue={address?.bairro}
                    type="text"
                    {...register("district", {
                      required: "Este campo é obrigatório!",
                    })}
                  />
                  {/* {errors.district && (
									<ErrorMessage message={errors.district.message} />
								)} */}
                </div>
                <div className={styles.formItem}>
                  <label>Cidade</label>
                  <input
                    defaultValue={address?.localidade}
                    type="text"
                    {...register("city", {
                      required: "Este campo é obrigatório!",
                    })}
                  />
                  {/* {errors.city && <ErrorMessage message={errors.city.message} />} */}
                </div>
                <div className={styles.formItem}>
                  <label>Estado</label>
                  <input
                    defaultValue={address?.uf}
                    type="text"
                    {...register("state", {
                      required: "Este campo é obrigatório!",
                    })}
                  />
                  {/* {errors.state && <ErrorMessage message={errors.state.message} />} */}
                </div>

                <div className={styles.formItem}>
                  <label>Senha</label>
                  <input
                    type="password"
                    {...register("password", {
                      required: "Este campo é obrigatório!",
                      minLength: {
                        value: 8,
                        message:
                          "Sua senha deve possuir, no mínimo, 8 caracteres.",
                      },
                    })}
                  />
                  {errors.password && (
                    <ErrorMessage message={errors.password.message} />
                  )}
                </div>
                <div className={styles.formItem}>
                  <label>Confirme sua senha</label>
                  <input
                    type="password"
                    {...register("confirmPassword", {
                      required: "Este campo é obrigatório!",
                      validate: (value) =>
                        value === passwordWatcher ||
                        "As senhas precisam ser iguais!",
                    })}
                  />
                  {errors.confirmPassword && (
                    <ErrorMessage message={errors.confirmPassword.message} />
                  )}
                </div>
              </form>

              <div className={styles.buttons}>
                <button type="button" onClick={handleSubmit(registerForm)}>
                  {animateLoading ? <AnimatedLoading /> : "Criar conta"}
                </button>
              </div>
              <span className={styles.loginButton}>
                Já tem uma conta?
                <a href="/login">Clique aqui</a>
              </span>
            </>
          )}
        </div>
        <div className={styles.right}>
          <img src={account.background_login[0]._url} alt="Imagem" />
        </div>
      </div>
    </div>
  );
};
