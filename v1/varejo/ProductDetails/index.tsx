import styles from "./styles.module.scss";
import { currencyMask } from "../../../utils/currencyMask";
import Slider from "react-slick";
import Select from "react-select";
import { useEffect, useState } from "react";
import InputMask from "react-input-mask";
import { AiFillHeart, AiOutlineHeart, AiOutlinePlus } from "react-icons/ai";
import { getImagesVariant } from "../../../utils/getVariantImages";
import ShippingPlugin from "../../../plugin/shipping.plugin";
import { AnimatedLoading } from "../AnimatedLoading";
import EventPlugin from "../../../core/core/plugin/event/event.plugin";
import { toast } from "react-hot-toast";
import CartPlugin from "../../../plugin/cart.plugin";
import { useRouter } from "next/router";
import { productInstallmentsCalc } from "../../../core/core/util/product.util";
import { SizeTableModal } from "../SizeTableModal";
import { tagManager } from "../../../core/core/util/TagManager";
import FavoritePlugin from "../../../plugin/favorite.plugin";
import { getVariantByValue } from "../../../utils/getVariantByValue";

const shippingPlugin = new ShippingPlugin();
const cartPlugin = new CartPlugin();
const favoritePlugin = new FavoritePlugin();

export function ProductDetails({ product, isFavorited, user }: any) {

  const [newProduct, setNewProduct] = useState(product);
  const [variantValue] = useState(product.variant[0].items[0]);
  const [isProductFavorited, setIsProductFavorited] = useState(isFavorited);
  const [productSizeTable, setProductSizeTable] = useState(false);

  const router = useRouter();

  const [productQuantity, setProductQuantity] = useState(1);
  const [destination, setDestination] = useState("");
  const [loadingShipping, setLoadingShipping] = useState(false);
  const [loadingAddToCart, setLoadingAddToCart] = useState(false);
  const [shippingMethods, setShippingMethods] = useState([]);
  const [variantColor, setVariantColor] = useState();
  const [variantSize, setVariantSize] = useState();

  function handleIncrementProductQuantity() {
    setProductQuantity(productQuantity + 1);
  }

  function handleDecrementProductQuantity() {
    if (productQuantity > 0) {
      setProductQuantity(productQuantity - 1);
    }
  }

  const sliderOptions = {
    customPaging: function (i: any) {
      return (
        <div className="thumb-img">
          <img
            src={getImagesVariant(newProduct, variantColor?.value)[i]?._url}
            alt=""
          />
        </div>
      );
    },
    dots: true,
    dotsClass: "slick-dots slick-thumb",
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  async function handleCalculateShipping() {
    setLoadingShipping(true);

    const result = await shippingPlugin.calculateShipping({
      destination: destination,
      weight: newProduct.weight,
    });

    if (result.status) {
      setShippingMethods(result.collection);
    }

    setLoadingShipping(false);
  }

  const handleAddToCart = async () => {
    setLoadingAddToCart(true);

    const newData = {
      data: {
        product: {
          referencePath: newProduct.referencePath,
        },
        variant: [variantColor, variantSize],
        quantity: productQuantity,
      },
    };
    let salePrice = null;
    let price = null;
    if (product.priceTable.data[variantValue.value]) {
      salePrice =
        product.priceTable.data[variantValue.value].promotionalPrice > 0
          ? product.priceTable.data[variantValue.value].promotionalPrice
          : null;
      price = product.priceTable.data[variantValue.value].price;
    } else {
      salePrice =
        product.priceTable.data._default.promotionalPrice > 0
          ? product.priceTable.data[variantValue.value].promotionalPrice
          : null;
      price = product.priceTable.data._default.price;
    }
    product.price = price;
    product.salePrice = salePrice;
    tagManager.addToCart(router.asPath, product, 1);

    const result = await cartPlugin.setItemCart(newData);

    if (result.status == false) {
      setLoadingAddToCart(false);
      return toast.error(result.error, {
        autoClose: 2000,
      });
    }

    setLoadingAddToCart(false);

    EventPlugin.dispatch("changeCart", result.data);

    toast.success("Produto adiconado com sucesso!");

    router.push("/carrinho");
  };

  async function handleFavoriteProduct(user: any, product: any) {
    const newData = {
      document: {
        referencePath: product?.referencePath,
      },
      user: {
        referencePath: user?.referencePath,
      },
    };

    const result = await favoritePlugin.setFavorite(newData);

    if (result?.status == false) {
      return toast.error("Oops! Ocorreu um erro. Tente novamente mais tarde  ");
    }

    setIsProductFavorited(result);

    toast.success("Produto favoritado!");
  }

  async function handleUnfavoriteProduct(user: any, product: any) {
    const newData = {
      document: {
        referencePath: product?.referencePath,
      },
      user: {
        referencePath: user?.referencePath,
      },
    };

    const result = await favoritePlugin.delFavorite(newData);

    if (result?.status == false) {
      return toast.error("Oops! Ocorreu um erro. Tente novamente mais tarde  ");
    }

    setIsProductFavorited({ status: false });

    toast.success("Produto retidado dos favoritos!");
  }

  useEffect(() => {
    setNewProduct(product);
    setVariantColor(getVariantByValue(product, product.color));
    setVariantSize(product?.variant[1]?.items[0]);
  }, [product]);

  return (
    <>
      <div className={styles.productDetails}>
        <div className={styles.productCarousel}>
          <Slider {...sliderOptions} className="productSlider">
            {getImagesVariant(newProduct, variantColor?.value)?.map(
              (image: any) => (
                <div key={image?.id} className="productSlide">
                  <img src={image?._url} alt={newProduct.name} />
                </div>
              )
            )}
          </Slider>
        </div>
        <div className={styles.productInfo}>
          <p className={styles.productName}>{newProduct?.name}</p>
          <div className={styles.productPrice}>
            {newProduct?.priceTable?.data?._default?.promotionalPrice > 0 ? (
              <>
                <span className={styles.sale}>
                  {currencyMask(newProduct?.priceTable?.data?._default?.price)}
                </span>
                <span className={styles.price}>
                  {currencyMask(
                    newProduct?.priceTable?.data?._default?.promotionalPrice
                  )}
                </span>
              </>
            ) : (
              <span className={styles.price}>
                {currencyMask(newProduct?.priceTable?.data?._default?.price)}
              </span>
            )}
          </div>
          <p className={styles.productInstallments}>
            {newProduct?.priceTable?.data?._default?.promotionalPrice > 0
              ? productInstallmentsCalc(
                  newProduct?.priceTable?.data?._default?.promotionalPrice,
                  6,
                  90
                )
              : productInstallmentsCalc(
                  newProduct?.priceTable?.data?._default?.price,
                  6,
                  90
                )}
          </p>
          <div
            className={styles.productDescription}
            dangerouslySetInnerHTML={{ __html: newProduct?.description }}
          />

          <div className={styles.productColorSize}>
            <div className={styles.productColor}>
              <span>Cor:</span>
              <Select
                classNamePrefix="react-select"
                placeholder="Cores"
                options={newProduct?.variant[0]?.items}
				defaultValue={getVariantByValue(newProduct, newProduct.color)}
                onChange={(e) => setVariantColor(e)}
              />
            </div>
            <div className={styles.productSize}>
              <span>Tamanho:</span>
              <Select
                classNamePrefix="react-select"
                placeholder="Tamanhos"
                options={newProduct.variant[1].items}
                onChange={(e) => setVariantSize(e)}
              />
            </div>
            <div className={styles.productQuantity}>
              <span>Quantidade:</span>
              <div>
                <span onClick={handleDecrementProductQuantity}>-</span>
                <span className={styles.value}>{productQuantity}</span>
                <span onClick={handleIncrementProductQuantity}>+</span>
              </div>
            </div>
          </div>
          {/* <div className={styles.productStock}>
          <p>Estoque:</p>
          <p className={styles.warning}>
            Disponível - <span>Última peça! Aproveite agora.</span>
          </p>
        </div> */}
          <div className={styles.productActions}>
            <div>
              {user && (
                <>
                  <span>Lista de desejos</span>
                  {isProductFavorited?.status ? (
                    <AiFillHeart
                      onClick={() => handleUnfavoriteProduct(user, product)}
                    />
                  ) : (
                    <AiOutlineHeart
                      onClick={() => handleFavoriteProduct(user, product)}
                    />
                  )}
                </>
              )}
            </div>
            <button
              type="button"
              disabled={loadingAddToCart}
              onClick={() => handleAddToCart()}
            >
              {loadingAddToCart ? <AnimatedLoading /> : "Adicionar ao carrinho"}
            </button>
          </div>
          <div className={styles.productShipping}>
            <InputMask
              mask="99999-999"
              maskChar=""
              onChange={(e: any) => setDestination(e.target.value)}
              placeholder="CEP 00000-000"
            />

            <button
              type="button"
              disabled={loadingShipping}
              onClick={() => handleCalculateShipping()}
            >
              {loadingShipping ? <AnimatedLoading /> : "Entrar"}
            </button>
            <div className={styles.shippingMethods}>
              {shippingMethods?.length >= 1 &&
                shippingMethods?.map(
                  (shippingMethod: any) =>
                    shippingMethod?.type?.label != "Retirada no Local" && (
                      <div className={styles.shipping} key={shippingMethod.id}>
                        <p>{shippingMethod?.name}:</p>
                        <p>{shippingMethod?.label}</p>
                        <p>{currencyMask(shippingMethod?.value)}</p>
                      </div>
                    )
                )}
            </div>
          </div>
          <div
            className={styles.productLink}
            onClick={() => setProductSizeTable(true)}
          >
            <span>Tabela de medidas</span>
            <AiOutlinePlus />
          </div>
          <div
            className={styles.productLink}
            onClick={() => router.push("/duvidas")}
          >
            <span>Política de troca</span>
            <AiOutlinePlus />
          </div>
          {/* <div className={styles.productText}>
          <span>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua.
          </span>
        </div> */}
        </div>
      </div>
      {productSizeTable && newProduct?.measures?.table && (
        <SizeTableModal
          table={newProduct?.measures?.table}
          setModal={setProductSizeTable}
        />
      )}
    </>
  );
}
