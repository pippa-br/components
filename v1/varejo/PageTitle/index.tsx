import styles from "./styles.module.scss";
import Link from "next/link";

export function PageTitle({ name }: any) {
  return (
    <div className={styles.container}>
      <p className={styles.pageTitle}>{name}</p>
      <div className={styles.breadcrumb}>
        <Link href="/">Home</Link>
        <img src="/assets/icons/angle-right-solid.svg" alt="Separador" />
        <span>{name}</span>
      </div>
    </div>
  );
}
