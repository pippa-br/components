import router from "next/router";
import { AiOutlineFacebook, AiOutlineInstagram } from "react-icons/ai";
import styles from "./styles.module.scss";

export const Footer = ({ account }: any) => (
  <footer className={styles.footer}>
    <div className={styles.content}>
      <div className={styles.left}>
        <img src="/assets/logo_white.png" alt="Logo" className={styles.logo} />
        <p>
          © 2021 Todos os direitos reservados à AO DENIM MODAS CNPJ:
          42.386.454/0001-92
        </p>
        <p>
          R. Juazeiro do Norte, 373 - Cidade Jardim Cumbica, Guarulhos - SP,
          07180-230
        </p>
        <img src="/assets/tipos-de-pagamento.png" alt="Tipos de pagamento" />
      </div>
      <div className={styles.right}>
        <div className={styles.column}>
          <p className={styles.title}>Sobre</p>
          <p className={styles.item} onClick={() => router.push("/sobre-nos")}>
            A marca
          </p>
          <p
            className={styles.item}
            onClick={() => router.push("/sustentabilidade")}
          >
            Sustentabilidade
          </p>
          <p className={styles.item}>
            <AiOutlineFacebook
              onClick={() => window.open(account?.facebook, "_blank")}
            />
            <AiOutlineInstagram
              onClick={() => window.open(account?.instagram, "_blank")}
            />
          </p>
        </div>
        <div className={styles.column}>
          <p className={styles.title}>Ajuda</p>
          <p className={styles.item} onClick={() => router.push("/duvidas")}>
            Dúvidas
          </p>
        </div>
        <div className={styles.column}>
          <p className={styles.title}>Conta</p>
          <p className={styles.item}>Minha conta</p>
          <p className={styles.item} onClick={() => router.push("/perfil")}>
            Meus Pedidos
          </p>
          <p
            className={styles.item}
            onClick={() => router.push("/lista-de-desejos")}
          >
            Meus Desejos
          </p>
          <p className={styles.item} onClick={() => router.push("/login")}>
            Cadastrar
          </p>
        </div>
      </div>
    </div>
  </footer>
);
