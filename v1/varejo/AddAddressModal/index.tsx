import styles from "./styles.module.scss";
import { useRouter } from "next/router";
import InputMask from "react-input-mask";
import { useForm } from "react-hook-form";
import {
  AiOutlineExclamationCircle,
  AiOutlineCloseCircle,
} from "react-icons/ai";
import { useState } from "react";
import { buscaCep } from "../../../utils/getCEP";
import { toast } from "react-hot-toast";
import { AnimatedLoading } from "../AnimatedLoading";
import AddressPlugin from "../../../plugin/address.plugin";

type AddressProps = {
  bairro: string;
  cep: string;
  complemento: string;
  localidade: string;
  logradouro: string;
  uf: string;
};

type AddressToEditProps = {
  name?: string;
  address?: {
    city?: string;
    complement?: string;
    district?: string;
    housenumber?: string;
    state?: string;
    street?: string;
    zipcode?: string;
  };
  referencePath?: string;
};

const addressPlugin = new AddressPlugin();

export const AddAddressModal = ({
  setOpenModal,
  edit = false,
  addressData = false || ({} as AddressToEditProps),
  userData,
}: any) => {
  const [address, setAddress] = useState<AddressProps>();
  const [loadingUserAddress, setLoadingUserAddress] = useState(false);
  const router = useRouter();

const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm();

  const onSubmit = async (data: any) => {
    toast.success("Endereço salvo com sucesso!");
    const newData = {
      data: {
        name: data.name,
        client: {
          referencePath: userData.referencePath,
        },
        address: {
          zipcode: data.cep,
          street: data.street,
          housenumber: data.housenumber,
          complement: data.complement,
          district: data.district,
          city: data.city,
          state: data.state,
          country: { id: "br", label: "Brasil", value: "br" },
        },
      },
    };

    await setLoadingUserAddress(true);
    // await addressPlugin.addDocumentFront(newData);
    await setLoadingUserAddress(false);
    router.reload();
  };

  const onSubmitEdit = async (data: any) => {
    toast.success("Endereço alterado com sucesso!");
    const newData = {
      document: {
        referencePath: addressData.referencePath,
      },
      data: {
        name: data.name,
        client: {
          referencePath: userData.referencePath,
        },
        address: {
          zipcode: data.cep,
          street: data.street,
          housenumber: data.housenumber,
          complement: data.complement,
          district: data.district,
          city: data.city,
          state: data.state,
          country: { id: "br", label: "Brasil", value: "br" },
        },
      },
    };
    await setLoadingUserAddress(true);
    // const result = await addressPlugin.setDocumentFront(newData);
    router.reload();
  };

  return (
    <div className={styles.addAddressModal} onClick={() => setOpenModal(false)}>
      <div className={styles.content} onClick={(e) => e.stopPropagation()}>
        <AiOutlineCloseCircle onClick={() => setOpenModal(false)} />
        <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
          <div className={styles.formItem}>
            <label>Nome do endereço</label>
            <input
              defaultValue={edit ? addressData?.name : null}
              autoComplete="new-off"
              placeholder="Ex: Casa, Apartamento, Empresa"
              {...register("name", { required: "Este campo é obrigatório!" })}
            />
            {errors.name && (
              <span className={styles.errorMessage}>
                <AiOutlineExclamationCircle />
                {errors.name?.message}
              </span>
            )}
          </div>
          <div className={styles.formItem}>
            <label>CEP</label>
            <InputMask
              mask="99999-999"
              placeholder="Ex: 12345-678"
              autoComplete="new-password"
              maskChar=""
              defaultValue={edit ? addressData?.address?.zipcode : null}
              onKeyUp={(e: any) =>
                e.target.value.length === 9 &&
                buscaCep(e.target.value, setAddress, setValue)
              }
              {...register("cep", { required: "Este campo é obrigatório!" })}
            />

            {errors.cep && (
              <span className={styles.errorMessage}>
                <AiOutlineExclamationCircle />
                {errors.cep?.message}
              </span>
            )}
          </div>

          <div className={styles.formItem}>
            <label>Rua</label>
            <input
              defaultValue={
                edit ? addressData?.address?.street : address?.logradouro
              }
              type="text"
              placeholder="Ex: Rua Guaranabara"
              autoComplete="new-password"
              {...register("street", { required: "Este campo é obrigatório!" })}
            />
            {errors.street && (
              <span className={styles.errorMessage}>
                <AiOutlineExclamationCircle />
                {errors.street?.message}
              </span>
            )}
          </div>
          <div className={styles.formItem}>
            <label>Número</label>
            <input
              type="text"
              defaultValue={edit ? addressData?.address?.housenumber : null}
              autoComplete="new-password"
              placeholder="Ex: 17"
              {...register("housenumber", {
                required: "Este campo é obrigatório!",
              })}
            />
            {errors.housenumber && (
              <span className={styles.errorMessage}>
                <AiOutlineExclamationCircle />
                {errors.housenumber?.message}
              </span>
            )}
          </div>
          <div className={styles.formItem}>
            <label>Complemento</label>
            <input
              type="text"
              defaultValue={edit ? addressData?.address?.complement : null}
              autoComplete="new-password"
              placeholder="Ex: Ap 15B"
              {...register("complement")}
            />
            {errors.complement && (
              <span className={styles.errorMessage}>
                <AiOutlineExclamationCircle />
                {errors.complement?.message}
              </span>
            )}
          </div>
          <div className={styles.formItem}>
            <label>Bairro</label>
            <input
              defaultValue={
                edit ? addressData?.address?.district : address?.bairro
              }
              type="text"
              autoComplete="new-password"
              placeholder="Ex: Dom Bosco"
              {...register("district", {
                required: "Este campo é obrigatório!",
              })}
            />
            {errors.district && (
              <span className={styles.errorMessage}>
                <AiOutlineExclamationCircle />
                {errors.district?.message}
              </span>
            )}
          </div>
          <div className={styles.formItem}>
            <label>Cidade</label>
            <input
              defaultValue={
                edit ? addressData?.address?.city : address?.localidade
              }
              type="text"
              placeholder="Ex: São Paulo"
              autoComplete="new-password"
              {...register("city", { required: "Este campo é obrigatório!" })}
            />
            {errors.city && (
              <span className={styles.errorMessage}>
                <AiOutlineExclamationCircle />
                {errors.city?.message}
              </span>
            )}
          </div>
          <div className={styles.formItem}>
            <label>Estado</label>
            <input
              defaultValue={edit ? addressData?.address?.state : address?.uf}
              type="text"
              autoComplete="new-password"
              placeholder="Ex: São Paulo"
              {...register("state", { required: "Este campo é obrigatório!" })}
            />
            {errors.state && (
              <span className={styles.errorMessage}>
                <AiOutlineExclamationCircle />
                {errors.state?.message}
              </span>
            )}
          </div>

          <button
            className={styles.submitPerfilButton}
            type="button"
            disabled={loadingUserAddress && true}
            onClick={edit ? handleSubmit(onSubmitEdit) : handleSubmit(onSubmit)}
          >
            {loadingUserAddress ? <AnimatedLoading /> : "Salvar"}
          </button>
        </form>
      </div>
    </div>
  );
};
