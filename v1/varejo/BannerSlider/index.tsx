import router from "next/router";
import { useEffect, useRef, useState } from "react";

export const BannerSlider = ({
  sliderArray,
  sliderMobile,
  sliderDesk,
}: any) => {
  const [x, setX] = useState(0);

  const nextSlide = () => {
    x === -100 * (sliderArray.length - 1) ? setX(0) : setX(x - 100);
  };

  const prevSlide = () => {
    x === 0 ? setX(-100 * (sliderArray.length - 1)) : setX(x + 100);
  };

  const marker = useRef<any>(null);
  useEffect(() => {
    const itemEls: any = document.querySelectorAll(".indicator .content span");
    if (itemEls[Math.abs(x / 100)] !== undefined && marker.current !== null) {
      marker.current.style.top = itemEls[Math.abs(x / 100)].offsetTop + "px";
      marker.current.style.height =
        itemEls[Math.abs(x / 100)].offsetHeight + "px";
    }
  }, [x]);

  return (
    <div className={`banner-slider`}>
      {sliderArray?.map((item: any, index: any) => {
        return (
          <div
            className="slide"
            key={index}
            style={{ transform: `translate(${x}%)` }}
          >
            <img
              onClick={() =>
                item?.url && window.open(item?.url, item?.target?.value)
              }
              className="slide-img-desk"
              src={item?.desktop?._url}
              alt="banner"
            />
            <img
              onClick={() =>
                item?.url && window.open(item?.url, item?.target?.value)
              }
              className="slide-img-mobile"
              src={item?.mobile?._url}
              alt="banner"
            />
          </div>
        );
      })}
      <div className="indicator">
        <div className="content">
          {sliderArray?.map((indicator: any, index: any) => (
            <span
              className={index == Math.abs(x / 100) ? "select" : ""}
              key={index}
            >
              0{index + 1}
            </span>
          ))}
          <div
            id="marker"
            ref={marker}
            style={{ top: 0, height: "34px" }}
          ></div>
          <div id="marker-background"></div>
        </div>
      </div>
      <img
        className="prevSlide"
        onClick={prevSlide}
        src="../../assets/icons/angle-left-solid.svg"
        alt=""
      />
      <img
        className="nextSlide"
        onClick={nextSlide}
        src="../../assets/icons/angle-right-solid.svg"
        alt=""
      />
    </div>
  );
};
