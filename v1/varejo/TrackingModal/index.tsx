import { AiOutlineCloseCircle } from "react-icons/ai";
import styles from "./styles.module.scss";

export function TrackingModal({ trackingCode, trackingStatus, setModal }: any) {

  return (
    <div className={styles.trackingModal}>
      <div className={styles.content}>
        <AiOutlineCloseCircle onClick={() => setModal(false)} />
        <p className={styles.message}>
          Código de rastreio: <span>{trackingCode}</span>
        </p>
        <table>
          <tbody>
            {trackingStatus.map((item: any, index: any) => (
              <tr key={`${index}tracking`}>
                <td>
                  Status: <span>{item.status}</span>
                </td>
                <td>
                  Data: <span>{item.data}</span>
                </td>
                <td>
                  Hora: <span>{item.hora}</span>
                </td>
                {item.local && (
                  <td>
                    Local: <span>{item.local}</span>
                  </td>
                )}
                {/* {item.origem && (
                  <td>
                    Origem: <span> {item.origem}</span>
                  </td>
                )} */}
                {item.destino && (
                  <td>
                    Destino: <span>{item.destino}</span>
                  </td>
                )}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}
