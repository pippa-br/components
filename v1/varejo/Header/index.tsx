import styles from "./styles.module.scss";
import { useEffect, useState } from "react";
import router, { useRouter } from "next/router";
import { MobileMenu } from "../MobileMenu";
// import { tagManager } from "../../../core/core/util/TagManager";
import { tagManager } from "../../../../core/v2/util/TagManager";
import {
  AiOutlineHeart,
  AiOutlineShoppingCart,
  AiOutlineUser,
} from "react-icons/ai";
import { ProductItemSkeletonCart } from "../../../v2/varejo/ProductItemSkeletonCart";
import { SvgCart } from "../../../../theme/svgCart";
// import EventPlugin from "../../../core/core/plugin/event/event.plugin";

export const Header = ({ categories, account, cart }: any) => {
  const [stickyHeader, setStickyHeader] = useState("normal");
  const [isMobileMenuOpen, setIsMobileMenuOpen] = useState(false);

  const [newCart, setNewCart] = useState(cart);
	const [loadingProducts, setLoadingProducts] = useState(true);

  EventPlugin.on("changeCart").subscribe((event: any) => {
    setNewCart(event);
  });

  const location = useRouter().pathname;

  useEffect(() => {
    if (account && account.gtm) {
      tagManager.initialize(account.gtm);
    }
    window.addEventListener("scroll", () => {
      if (window.scrollY >= 300) {
        setStickyHeader("sticky");
      } else {
        setStickyHeader("normal");
      }
    });

    setLoadingProducts(false)
  }, []);

  return (
    <>
      <div
        className={styles.header}
        style={
          location == "/"
            ? {
              position: "absolute",
              background: "transparent",
              color: "#fff",
            }
            : {
              color: "#000",
            }
        }
      >
        <div className={styles.content}>
          <div className={styles.top}>
            <li onClick={() => router.push("/")}>
              {location == "/" ? (
                <>
                  <img
                    className={styles.logo}
                    src="/assets/logo_white.png"
                    alt=""
                  />
                  <img
                    className={styles.logoColor}
                    src="/assets/logo.png"
                    alt=""
                  />
                </>
              ) : (
                <img
                  className={styles.logoOthers}
                  src="/assets/logo.png"
                  alt=""
                />
              )}
            </li>
            <div className={styles.otherPages}>
              <li onClick={() => router.push("/sobre-nos")}>Sobre nós</li>
              <li onClick={() => router.push("/sustentabilidade")}>
                Sustentabilidade
              </li>
              <li onClick={() => router.push("/duvidas")}>Dúvidas ?</li>
            </div>
          </div>
          <div className={styles.bottom}>
            <div className={styles.nav}>
              <nav>
                <li onClick={() => router.push("/loja?orderBy=postdate")}>
                  Novidades
                </li>
                <li className={styles.subMenu}>
                  Roupas
                  <ul>
                    {categories &&
                      categories.map((category: any) => (
                        <li
                          onClick={() =>
                            router.push(`/categoria/${category.name}`)
                          }
                          key={category.id}
                        >
                          {category.name}
                        </li>
                      ))}
                  </ul>
                </li>
              </nav>
              {/* <span className={styles.separator}>|</span>
                <div className={styles.pages}>
                  <a href="/best">Best</a>
                  <a href="/evento">Evento</a>
                  <a href="/blog">Blog</a>
                  <a href="/lookbook">LookBook</a>
                </div> */}
            </div>
            <div className={styles.menu}>
              <li onClick={() => router.push("/lista-de-desejos")}>
                <AiOutlineHeart />
              </li>
              <li
                className={styles.cartIcon}
                onClick={() => router.push("/carrinho")}
              >
                {loadingProducts ? (
                <>
                  {Array.from({ length: 24 }).map((product, index) => (
                    <ProductItemSkeletonCart key={`productSkeleton-${index}`} />
                  ))}
                </>
                ) : (
                <>
                  <SvgCart />
                </>
								)}
              </li>
              <li onClick={() => router.push("/login")}>
                <AiOutlineUser />
              </li>
            </div>
          </div>
          <div className={styles.mobileMenu}>
            <li onClick={() => router.push("/")}>
              <img
                className={styles.mobileHeaderLogo}
                src="/assets/logo.png"
                alt=""
              />
            </li>
            <li
              className={styles.mobileCart}
              onClick={() => router.push("/carrinho")}
            >
              <AiOutlineShoppingCart />
              <span>{newCart?.items?.length || 0}</span>
            </li>
            <img
              onClick={() => setIsMobileMenuOpen(true)}
              className={styles.mobileMenuNav}
              src="/assets/icons/bars-solid.svg"
              alt=""
            />
          </div>
        </div>
      </div>
      {stickyHeader != "normal" && (
        <div className={styles.headerSticky}>
          <div className={styles.content}>
            <li onClick={() => router.push("/")}>
              <img className={styles.logo} src="/assets/logo.png" alt="" />
            </li>
            <div className={styles.nav}>
              <nav>
                <li onClick={() => router.push("/loja?orderBy=postdate")}>
                  Novidades
                </li>
                <li className={styles.subMenu}>
                  Roupas
                  <ul>
                    {categories &&
                      categories.map((category: any) => (
                        <li
                          onClick={() =>
                            router.push(`/categoria/${category.name}`)
                          }
                          key={category.id}
                        >
                          {category.name}
                        </li>
                      ))}
                  </ul>
                </li>
              </nav>
              {/* <span className={styles.separator}>|</span>
              <div className={styles.pages}>
                <a href="/best">Best</a>
                <a href="/evento">Evento</a>
                <a href="/blog">Blog</a>
                <a href="/lookbook">LookBook</a>
              </div> */}
            </div>
            <div className={styles.menu}>
              <li onClick={() => router.push("/lista-de-desejos")}>
                <AiOutlineHeart />
              </li>
              <li
                className={styles.cartIcon}
                onClick={() => router.push("/carrinho")}
              >
                <AiOutlineShoppingCart />
                <span>{newCart?.items?.length || 0}</span>
              </li>
              <li onClick={() => router.push("/login")}>
                <AiOutlineUser />
              </li>
            </div>
            <div className={styles.mobileMenu}>
              <li onClick={() => router.push("/")}>
                <img
                  className={styles.mobileHeaderLogo}
                  src="/assets/logo.png"
                  alt=""
                />
              </li>
              <li
                className={styles.mobileCart}
                onClick={() => router.push("/carrinho")}
              >
                <AiOutlineShoppingCart />
                <span>{newCart?.items?.length || 0}</span>
              </li>
              <img
                onClick={() => setIsMobileMenuOpen(true)}
                className={styles.mobileMenuNav}
                src="/assets/icons/bars-solid.svg"
                alt=""
              />
            </div>
          </div>
        </div>
      )}
      {isMobileMenuOpen && (
        <MobileMenu categories={categories} handleModal={setIsMobileMenuOpen} />
      )}
    </>
  );
};
