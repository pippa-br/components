import { useRouter } from "next/router";
import { AiOutlineCloseCircle } from "react-icons/ai";
import styles from "./styles.module.scss";

export const MobileMenu = ({ handleModal, categories }: any) => {
  const router = useRouter();

  return (
    <div className={styles.container} onClick={() => handleModal(false)}>
      <div className={styles.content} onClick={(e) => e.stopPropagation()}>
        <AiOutlineCloseCircle
          onClick={() => handleModal(false)}
          className={styles.closeMobileMenuButton}
        />
        <div className={styles.mobileMenu}>
          <p
            onClick={() => {
              router.push("/");
              handleModal(false);
            }}
          >
            Home
          </p>
          <p
            onClick={() => {
              router.push("/loja?orderBy=postdate");
              handleModal(false);
            }}
          >
            Novidades
          </p>
          {categories &&
            categories.map((category: any) => (
              <p
                key={category.id}
                onClick={() => {
                  router.push(`/categoria/${category.name}`);
                  handleModal(false);
                }}
              >
                {category.name}
              </p>
            ))}
          <p
            onClick={() => {
              router.push(`/perfil`);
              handleModal(false);
            }}
          >
            Conta
          </p>
          <p
            onClick={() => {
              router.push("/lista-de-desejos");
              handleModal(false);
            }}
          >
            Lista de desejos
          </p>
          <p
            onClick={() => {
              router.push("/duvidas");
              handleModal(false);
            }}
          >
            Dúvidas
          </p>
          <p
            onClick={() => {
              router.push("/sobre-nos");
              handleModal(false);
            }}
          >
            Sobre Nós
          </p>
          <p
            onClick={() => {
              router.push("/sustentabilidade");
              handleModal(false);
            }}
          >
            Sustentabilidade
          </p>
        </div>
      </div>
    </div>
  );
};
