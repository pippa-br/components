import Slider from "react-slick";
import { useRef } from "react";
import styles from "./styles.module.scss";
import { ProductItem } from "../ProductItem";

export const NewestProductsSlider = ({ products }: any) => {
  const customSlider = useRef<any>();

  var sliderOpts = {
    infinite: true,
    arrows: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 720,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
        },
      },
    ],
  };

  const nextSliderHandle = () => {
    customSlider.current.slickNext();
  };

  const prevSliderHandle = () => {
    customSlider.current.slickPrev();
  };

  return (
    <div className={styles.newestSlider}>
      <div className={styles.content}>
        <div className={styles.top}>
          <p className={styles.title}>Novidades</p>
          <div className={styles.sliderArrows}>
            <img
              className={styles.backIcon}
              onClick={prevSliderHandle}
              src="/assets/icons/angle-left-solid.svg"
              alt=""
            />
            <img
              className={styles.nextIcon}
              onClick={nextSliderHandle}
              src="/assets/icons/angle-right-solid.svg"
              alt=""
            />
          </div>
        </div>
        <p className={styles.text}>
          {/* Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. */}
        </p>

        <Slider
          {...sliderOpts}
          className="newestProductSlider"
          ref={customSlider}
        >
          {products.map((product: any) => (
            <div className="slide" key={product.code}>
              <ProductItem product={product} />
            </div>
          ))}
        </Slider>
      </div>
    </div>
  );
};
