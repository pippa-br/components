import styles from "./styles.module.scss";
import { BsWhatsapp } from "react-icons/bs";

export const WhatsAppIcon = ({ account }: any) => {
  return (
    <div
      className={styles.whatsAppIcon}
      onClick={() =>
        window.open(
          `https://api.whatsapp.com/send?phone=${account?.whatsapp}&text=Olá! Preciso de um auxílio!`,
          "_blank"
        )
      }
    >
      <BsWhatsapp />
    </div>
  );
};
