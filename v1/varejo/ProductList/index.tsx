import { ProductItem } from "../ProductItem";
import styles from "./styles.module.scss";

export const ProductList = ({ products }: any) => {
  return (
    <>
      {products && products[0] ? (
        <div className={styles.productList}>
          {products.map((product: any) => (
            <ProductItem key={product.sku} product={product} />
          ))}
        </div>
      ) : (
        <p className={styles.noProducts}>Não foram encontrados produtos.</p>
      )}
    </>
  );
};
