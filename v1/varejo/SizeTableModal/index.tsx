import { AiOutlineCloseCircle } from "react-icons/ai";
import styles from "./styles.module.scss";

export function SizeTableModal({ table, setModal }: any) {
  return (
    <div className={styles.trackingModal}>
      <div className={styles.content}>
        <AiOutlineCloseCircle onClick={() => setModal(false)} />
        <p className={styles.message}>Tabela de medidas</p>
        <table>
          <thead>
            <tr>
              {table[0].items.map((head: any) => (
                <td key={head.id}>{head.label}</td>
              ))}
            </tr>
          </thead>
          <tbody>
            {table.map((row: any) => (
              <tr key={row.id}>
                {row.items.map((column: any) => (
                  <td key={column.id}>{column.value}</td>
                ))}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}
