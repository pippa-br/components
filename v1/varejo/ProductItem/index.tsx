import styles from "./styles.module.scss";
import { useRouter } from "next/router";
import { currencyMask } from "../../../utils/currencyMask";
import { productInstallmentsCalc } from "../../../core/core/util/product.util";

type ProductImageProps = {
  _url: string;
};

type ProductCategoryProps = {
  name: string;
  id: string;
  referencePath: string;
};

type ProductProps = {
  name: string;
  images: ProductImageProps[];
  categories: ProductCategoryProps[];
  color: {
    value: string;
    id: string;
    label: string;
  };
  referencePath: string;
  code: string;
};

type ProductItemProps = {
  sale?: boolean;
  product: ProductProps;
};

export function ProductItem({ sale, product }: any) {
  const router = useRouter();
  const query = useRouter().query;

  return (
    <div
      className={styles.container}
      onClick={() => router.push(`/produto/${product?.pid}?color=${product?.levelVariant.value}`)}
    >
      <div className={styles.productImage}>
        {sale && <span>Promo</span>}
        <img
          src={
            product?.images?.data?.[`${query.color}`]?.images[0]?._url ||
            product?.images?.data?.[product?.variant[0]?.items[0]?.value]
              ?.images[0]?._url
          }
          alt={product?.name}
        />
      </div>
      <p className={styles.productName}>{product?.name}</p>
      {product?.priceTable && (
        <p className={styles.productInstallments}>
          {product?.priceTable?.data?._default?.promotionalPrice > 0 ||
          product?.promotionalPrice > 0
            ? productInstallmentsCalc(
                product?.priceTable?.data?._default?.promotionalPrice ||
                  product?.promotionalPrice,
                6,
                90
              )
            : productInstallmentsCalc(
                product?.priceTable?.data?._default?.price || product?.price,
                6,
                90
              )}
        </p>
      )}

      <div className={styles.productColor}>
        {product?.variant[0]?.items?.map((color: any) => (
          <p key={color.id}>{color?.label}</p>
        ))}
      </div>
      {product?.priceTable && (
        <p className={styles.productPrice}>
          {currencyMask(
            product?.priceTable?.data?._default?.price || product?.price
          )}
        </p>
      )}
    </div>
  );
}
