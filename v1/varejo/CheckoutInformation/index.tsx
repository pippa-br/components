import styles from "./styles.module.scss";
import { useRouter } from "next/router";
import InputMask from "react-input-mask";
import { useForm } from "react-hook-form";
import { AiOutlineExclamationCircle } from "react-icons/ai";
import { useState } from "react";
import { buscaCep } from "../../../utils/getCEP";
import { toast } from "react-hot-toast";
import { validateCpf } from "../../../utils/validateCpf";
import AuthPlugin from "../../../plugin/auth.plugin";
import CartPlugin from "../../../plugin/cart.plugin";
import { AnimatedLoading } from "../AnimatedLoading";

const authPlugin = new AuthPlugin();
const cartPlugin = new CartPlugin();

export const CheckoutInformation = ({
  userData,
  onSubmit,
  cart,
  loadingCart,
}: any) => {
  const [address, setAddress] = useState();
  const [newUserData, setNewUserData] = useState(userData);
  const [loadingUserAddress, setLoadingUserAddress] = useState(false);
  const router = useRouter();

  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm();

  async function handleLogout() {
    await authPlugin.logoutFront();
    router.push("/");
    toast.success("Logout feito com sucesso!");
  }

  const onSubmitCartUser = async (data: any) => {
    if (!validateCpf(data.cpf)) {
      return toast.error("CPF inválido!");
    }

    const newData = {
      document: {
        referencePath: userData.referencePath,
      },
      data: data,
    };

    const result = await authPlugin.setUserFront(newData);

    setNewUserData(result);
  };

  return (
    <div className={styles.checkoutInformation}>
      <p className={styles.title}>Informações de contato</p>
      <div className={styles.avatar}>
        <img src="/assets/avatar.jpeg" alt="Avatar" />
        <div className={styles.info}>
          <p className={styles.nameEmail}>
            {userData?.name} ({userData?.email})
          </p>
          <p className={styles.logout} onClick={() => handleLogout()}>
            Sair
          </p>
        </div>
      </div>
      {(newUserData?.name == undefined || newUserData?.cpf == undefined) && (
        <>
          <form
            className={styles.form}
            onSubmit={handleSubmit(onSubmitCartUser)}
          >
            <div className={styles.formItem}>
              <label>Nome completo</label>
              <input
                defaultValue={newUserData?.name}
                type="text"
                autoComplete="new-password"
                {...register("name", {
                  validate: (value) =>
                    value.length >= 8 ||
                    "Seu nome deve possuir 8 caracteres no mínimo!",
                })}
              />
              {errors.name && (
                <span className={styles.errorMessage}>
                  <AiOutlineExclamationCircle />
                  {errors.name?.message}
                </span>
              )}
            </div>
            <div className={styles.formItem}>
              <label>CPF</label>
              <InputMask
                {...register("cpf2", {
                  validate: (value) => validateCpf(value) || "CPF inválido!",
                })}
                defaultValue={newUserData?.cpf}
                autoComplete="new-off"
                mask="999.999.999-99"
                placeholder="Ex: 123.456.789-10"
                maskChar=""
              />
              {errors.cpf2 && (
                <span className={styles.errorMessage}>
                  <AiOutlineExclamationCircle />
                  {errors.cpf2?.message}
                </span>
              )}
            </div>

            <div className={styles.formItem}>
              <label>Data de nascimento</label>
              <InputMask
                mask="99/99/9999"
                autoComplete="new-password"
                placeholder="Data de nascimento"
                defaultValue={newUserData?.birthday}
                maskChar=""
                {...register("birthday", {
                  validate: (value) =>
                    value.length >= 10 || "Verifique sua data de nascimento!",
                })}
              />
              {errors.birthday && (
                <span className={styles.errorMessage}>
                  <AiOutlineExclamationCircle />
                  {errors.birthday?.message}
                </span>
              )}
            </div>
            <div className={styles.formItemCheckbox}>
              <div>
                <input
                  autoComplete="new-password"
                  type="checkbox"
                  defaultChecked={userData?.newsletter}
                  {...register("newsletter")}
                />
                <label>quero receber e-mails com promoções</label>
              </div>
            </div>
          </form>
        </>
      )}
      <p className={styles.title}>Endereço de entrega</p>
      <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
        <div className={styles.formItem}>
          <label>CEP</label>
          <InputMask
            mask="99999-999"
            placeholder="Ex: 12345-678"
            autoComplete="new-password"
            defaultValue={cart?.address?.zipcode}
            maskChar=""
            onKeyUp={(e: any) =>
              e.target.value.length === 9 &&
              buscaCep(e.target.value, setAddress, setValue)
            }
            {...register("cep", { required: "Este campo é obrigatório!" })}
          />
          {errors.cep && (
            <span className={styles.errorMessage}>
              <AiOutlineExclamationCircle />
              {errors.cep?.message}
            </span>
          )}
        </div>
        <div className={styles.formItem}>
          <label>Rua</label>
          <input
            type="text"
            placeholder="Ex: Rua Guaranabara"
            autoComplete="new-password"
            defaultValue={cart?.address?.street}
            {...register("street", { required: "Este campo é obrigatório!" })}
          />
          {errors.street && (
            <span className={styles.errorMessage}>
              <AiOutlineExclamationCircle />
              {errors.street?.message}
            </span>
          )}
        </div>
        <div className={styles.formItem}>
          <label>Número</label>
          <input
            type="text"
            defaultValue={cart?.address?.housenumber}
            autoComplete="new-password"
            placeholder="Ex: 17"
            {...register("housenumber", {
              required: "Este campo é obrigatório!",
            })}
          />
          {errors.housenumber && (
            <span className={styles.errorMessage}>
              <AiOutlineExclamationCircle />
              {errors.housenumber?.message}
            </span>
          )}
        </div>
        <div className={styles.formItem}>
          <label>Complemento</label>
          <input
            type="text"
            autoComplete="new-password"
            defaultValue={cart?.address?.complement}
            placeholder="Ex: Ap 15B"
            {...register("complement")}
          />
          {errors.complement && (
            <span className={styles.errorMessage}>
              <AiOutlineExclamationCircle />
              {errors.complement?.message}
            </span>
          )}
        </div>
        <div className={styles.formItem}>
          <label>Bairro</label>
          <input
            type="text"
            autoComplete="new-password"
            defaultValue={cart?.address?.district}
            placeholder="Ex: Dom Bosco"
            {...register("district", {
              required: "Este campo é obrigatório!",
            })}
          />
          {errors.district && (
            <span className={styles.errorMessage}>
              <AiOutlineExclamationCircle />
              {errors.district?.message}
            </span>
          )}
        </div>
        <div className={styles.formItem}>
          <label>Cidade</label>
          <input
            type="text"
            placeholder="Ex: São Paulo"
            defaultValue={cart?.address?.city}
            autoComplete="new-password"
            {...register("city", { required: "Este campo é obrigatório!" })}
          />
          {errors.city && (
            <span className={styles.errorMessage}>
              <AiOutlineExclamationCircle />
              {errors.city?.message}
            </span>
          )}
        </div>
        <div className={styles.formItem}>
          <label>Estado</label>
          <input
            type="text"
            autoComplete="new-password"
            defaultValue={cart?.address?.state}
            placeholder="Ex: São Paulo"
            {...register("state", { required: "Este campo é obrigatório!" })}
          />
          {errors.state && (
            <span className={styles.errorMessage}>
              <AiOutlineExclamationCircle />
              {errors.state?.message}
            </span>
          )}
        </div>
        <div className={styles.buttons}>
          {!loadingCart && (
            <button
              className={styles.cinza}
              type="button"
              onClick={() => router.push("/carrinho")}
            >
              Voltar ao carrinho
            </button>
          )}
          <button
            className={styles.submitPerfilButton}
            type="button"
            disabled={loadingCart && true}
            onClick={handleSubmit(onSubmit)}
          >
            {loadingCart ? <AnimatedLoading /> : "Próximo"}
          </button>
        </div>
      </form>
    </div>
  );
};
