import styles from "./styles.module.scss";
import { useState } from "react";
import { AiOutlineCheckCircle } from "react-icons/ai";
import { BsCircle } from "react-icons/bs";
import { AnimatedLoading } from "../AnimatedLoading";
import router from "next/router";

export const CheckoutShipping = ({
  cart,
  goBack,
  loadingCart,
  onSubmit,
}: any) => {
  const [selectedShippingMethod, setSelectedShippingMethod] = useState(
    cart.shipping || {
      label: "",
      type: { label: "" },
    }
  );

  const [selectedShippingRetirada, setSelectedShippingRetirada] = useState(
    cart.shipping || {
      label: "",
      type: { label: "" },
    }
  );

  const [selectedFinalShipping, setSelectedFinalShipping] = useState(
    cart.shipping || {
      label: "",
      type: { label: "" },
    }
  );

  return (
    <div className={styles.checkoutShipping}>
      <div className={styles.shippingOption}>
        <p>
          <b>Enviar para:</b> {cart.address.street},Nº{" "}
          {cart.address.housenumber}, {cart.address.district},{" "}
          {cart.address.city}, {cart.address.state}. CEP: {cart.address.zipcode}{" "}
        </p>
      </div>
      <p className={styles.title}>Selecione o método de entrega:</p>
      <div className={styles.shippingMethods}>
        {cart?.shippingMethods?.map(
          (shipping: any, index: any) =>
            shipping?.type?.label == "Correios" && (
              <p
                onClick={() => {
                  setSelectedFinalShipping(shipping);
                  setSelectedShippingMethod(shipping);
                }}
                className={
                  shipping.label == selectedShippingMethod.label
                    ? `${styles.shippingOption} ${styles.active}`
                    : styles.shippingOption
                }
                key={`${index}-shipping`}
              >
                <BsCircle className={styles.noCheck} />
                <AiOutlineCheckCircle className={styles.check} />{" "}
                {shipping?.label}
              </p>
            )
        )}
        {/* <p
          onClick={() =>
            setSelectedShippingMethod({
              label: "",
              type: { label: "Retirada no Local" },
            })
          }
          className={
            selectedShippingMethod.type.label == "Retirada no Local"
              ? `${styles.shippingOption} ${styles.active}`
              : styles.shippingOption
          }
        >
          <BsCircle className={styles.noCheck} />
          <AiOutlineCheckCircle className={styles.check} /> Retirar no local -
          Após 24h da confirmação do pedido
        </p> */}
      </div>

      {/* {selectedShippingMethod?.type?.label == "Retirada no Local" && (
        <>
          <p className={styles.title}>Selecione o local de retirada:</p>
          <div className={styles.shippingMethods}>
            {cart?.shippingMethods.map(
              (shipping: any, index: any) =>
                shipping?.type?.label == "Retirada no Local" && (
                  <p
                    onClick={() => {
                      setSelectedFinalShipping(shipping);
                      setSelectedShippingRetirada(shipping);
                    }}
                    className={
                      shipping.label == selectedShippingRetirada.label
                        ? `${styles.shippingOption} ${styles.active}`
                        : styles.shippingOption
                    }
                    key={`${index}-shippingRetirada`}
                  >
                    <BsCircle className={styles.noCheck} />
                    <AiOutlineCheckCircle className={styles.check} />
                    {shipping?.name}
                  </p>
                )
            )}
          </div>
        </>
      )} */}
      <div className={styles.buttons}>
        {!loadingCart && (
          <button
            className={styles.cinza}
            type="button"
            onClick={() => router.push("/carrinho")}
          >
            Voltar ao carrinho
          </button>
        )}
        {loadingCart ? null : (
          <button
            className={styles.cinza}
            type="button"
            onClick={() => goBack()}
          >
            Voltar
          </button>
        )}
        <button
          disabled={loadingCart && true}
          type="button"
          onClick={() => onSubmit(selectedFinalShipping)}
        >
          {loadingCart ? <AnimatedLoading /> : "Próximo"}
        </button>
      </div>
    </div>
  );
};
