import styles from "./styles.module.scss";
import { currencyMask } from "../../../utils/currencyMask";
import { useState } from "react";
import { BiTrashAlt } from "react-icons/bi";
import CartPlugin from "../../../plugin/cart.plugin";
import { toast } from "react-hot-toast";
import { tagManager } from "../../../core/core/util/TagManager";
import router from "next/router";
import EventPlugin from "../../../core/core/plugin/event/event.plugin";
import { firstImageItemCart } from "../../../core/core/util/cart.util";

const cartPlugin = new CartPlugin();

function CartProductsTable({ cart }: any) {
  const [newCart, setNewCart] = useState(cart);


  //console.error('-----', cart);

  async function handleIncrementQuantity(
    product: any,
    variant: any,
    quantity: any
  ) {
    const newData = {
      data: {
        product: {
          referencePath: product.referencePath,
        },
        variant: [...variant],
        quantity: quantity + 1,
      },
    };
    tagManager.addToCart(router.asPath, product, 1);
    
    const result = await cartPlugin.setItemCart(newData);
    
    if (result.status) {
      toast.success("Quantidade alterada com sucesso!");
      setNewCart(result.data);
    } else {
      return toast.error(result.error, {
        autoClose: 3000,
      });
    }
  }

  async function handleDecrementQuantity(
    product: any,
    variant: any,
    quantity: any
  ) {
    const newData = {
      data: {
        product: {
          referencePath: product.referencePath,
        },
        variant: [...variant],
        quantity: quantity - 1,
      },
    };

    tagManager.removeFromCart(router.asPath, product, 1);

    const result = await cartPlugin.setItemCart(newData);

    if (result.status) {
      toast.success("Quantidade alterada com sucesso!");
      setNewCart(result.data);
    } else {
      return toast.error(result.error, {
        autoClose: 3000,
      });
    }
  }

  async function handleDeleteProduct(product: any, variant: any) {
    const filteredCartItems = newCart.items.filter(
      (item: any) => item.id != product.id
    );

    setNewCart({ ...newCart, items: filteredCartItems || [] });

    toast.success("Produto excluído com sucesso!");

    tagManager.removeFromCart(router.asPath, product, product.quantity);
    const newData = {
      data: {
        product: {
          referencePath: product.referencePath,
        },
        variant: [...variant],
      },
    };

    const result = await cartPlugin.delItemCart(newData);

    if (filteredCartItems.length >= 1) {
      return setNewCart(result.data);
    } else {
      return router.reload();
    }
  }

  async function handleCheckoutCart() {
    // setLoadingCheckoutOrder(true);
    // if (!validateCpf(userData?.cpf)) {
    //   setLoadingCheckoutOrder(false);
    //   return toast.error(
    //     "CPF inválido! Verifique se seus dados cadastrais estão completos.",
    //     {
    //       autoClose: 3000,
    //     }
    //   );
    // }
    // if (hasSubProduct() && newCart.attachment == null) {
    //   setLoadingCheckoutOrder(false);
    //   return toast.error(
    //     "Receita é obrigatória para a compra de Lentes com Grau!"
    //   );
    // }
    // if (cantBuySubProduct()) {
    //   setLoadingCheckoutOrder(false);
    //   return toast.error("Não é possível fechar o pedido somente com Lentes.");
    // }
    // const result = await cartPlugin.checkoutCart();
    // if (result.status == false) {
    //   toast.error(result.error);
    // } else {
    //   toast.success("Pedido sendo processado! Estamos te redirecionando...", {
    //     autoClose: 5000,
    //   });
    //   router.push(`/pedido/${result.data.id}`);
    // }
    // setLoadingCheckoutOrder(false);
  }

  async function handleClearCart() {
    toast.success("Carrinho sendo limpo!");

    newCart.items.forEach((product: any) => {
      tagManager.removeFromCart(router.asPath, product, product.quantity);
    });

    await EventPlugin.dispatch("changeCart", null);

    const result = await cartPlugin.clearCart();

    setNewCart(result.data);
  }

  return (
    <div className={styles.cartProductsTable}>
      {newCart?.items?.length > 0 ? (
        <>
          <header>
            <p>Item</p>
            <p>Quantidade</p>
            <p>Preço</p>
            <p></p>
          </header>
          <div className={styles.body}>
            {newCart?.items?.map((item: any) => (
              <div className={styles.item} key={item.id}>
                <div className={styles.product}>
                  <div className={styles.img}>
                    <img
                      src={firstImageItemCart(item)}
                      alt={item?.product?.name}
                    />
                  </div>
                  <div>
                    <div className={styles.productName}>
                      {item?.product?.name}
                    </div>
                    <div className={styles.productDetails}>
                      Cor: {item?.variant[0]?.label} / Tamanho:{" "}
                      {item?.variant[1]?.label}
                    </div>
                  </div>
                </div>
                <div className={styles.quantity}>
                  <div>
                    <span
                      onClick={() =>
                        handleDecrementQuantity(
                          item?.product,
                          item?.variant,
                          item?.quantity
                        )
                      }
                    >
                      -
                    </span>
                    <span className={styles.value}>{item?.quantity}</span>
                    <span
                      onClick={() =>
                        handleIncrementQuantity(
                          item?.product,
                          item?.variant,
                          item?.quantity
                        )
                      }
                    >
                      +
                    </span>
                  </div>
                </div>
                <div className={styles.price}>{currencyMask(item?.total)}</div>
                <div className={styles.actions}>
                  <BiTrashAlt
                    onClick={() =>
                      handleDeleteProduct(item?.product, item?.variant)
                    }
                  />
                </div>
              </div>
            ))}
          </div>
          <div className={styles.total}>
            <p>Total</p>
            <p>{currencyMask(newCart?.totalItems)}</p>
          </div>
        </>
      ) : (
        <p className={styles.noProducts}>
          Você não possui produtos no carrinho!
        </p>
      )}

      <div className={styles.buttons}>
        <button type="button" onClick={() => router.push("/loja")}>
          Continuar comprando
        </button>
        {newCart?.items?.length > 0 && (
          <>
            <button type="button" onClick={() => handleClearCart()}>
              Limpar Carrinho
            </button>
            <button type="button" onClick={() => router.push("/checkout")}>
              Concluir compra
            </button>
          </>
        )}
      </div>
    </div>
  );
}

export default CartProductsTable;
