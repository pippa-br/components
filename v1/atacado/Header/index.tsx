import styles from "./styles.module.scss";
import { GiShoppingCart } from "react-icons/gi";
import { useState } from "react";
import { CartModal } from "../CartModal";
import EventPlugin from "../../../core/core/plugin/event/event.plugin";
import router from "next/router";
import { AiOutlineMenu } from "react-icons/ai";
import { FiLogOut } from "react-icons/fi";
import AuthPlugin from "../../../plugin/auth.plugin";
import { HeaderMenuModal } from "../HeaderMenuModal";

const authPlugin = new AuthPlugin();

export const Header: React.FC<any> = ({ user, cart }) => {
  const [isCartModalOpen, setIsCartModalOpen] = useState(false);
  const [isMenuModalOpen, setIsMenuModalOpen] = useState(false);
  const [newCart, setNewCart] = useState(cart);

  EventPlugin.on("changeCart").subscribe((event) => {
    setNewCart(event);
  });

  async function handleLogout() {
    await authPlugin.logoutFront();
    router.push("/login");
  }

  return (
    <>
      <header className={styles.header}>
        <div className={styles.content}>
          <AiOutlineMenu
            onClick={() => setIsMenuModalOpen(true)}
            className={styles.burguerMenu}
          />
          <div className={styles.logo} onClick={() => router.push("/")}>
            <img src="/assets/logo_colored.png" alt="" />
          </div>
          <div className={styles.menu}>
            <li onClick={() => router.push("/")}>Produtos</li>
            <li onClick={() => router.push("/devolucoes")}>Devoluções</li>
            <li onClick={() => router.push("/politica-de-privacidade")}>
              Política de privacidade
            </li>
            <li onClick={() => router.push("/meus-dados")}>Meus dados</li>
            <li onClick={() => router.push("/meus-pedidos")}>Meus pedidos</li>
            <FiLogOut
              className={styles.logoutIcon}
              onClick={() => handleLogout()}
              title="Fazer logout"
            />
            <div
              className={styles.cartIcon}
              onClick={() => setIsCartModalOpen(!isCartModalOpen)}
            >
              <GiShoppingCart />
              <span>{newCart?.items?.length || 0}</span>
            </div>
          </div>
        </div>
      </header>
      {isCartModalOpen && (
        <CartModal openModal={setIsCartModalOpen} cart={newCart} user={user} />
      )}
      {isMenuModalOpen && <HeaderMenuModal setModal={setIsMenuModalOpen} />}
    </>
  );
};
