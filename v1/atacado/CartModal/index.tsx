import router from "next/router";
import { useState } from "react";
import { AiOutlineCloseCircle } from "react-icons/ai";
import { FaWhatsapp } from "react-icons/fa";
import { FiTrash } from "react-icons/fi";
import { toast } from "react-hot-toast";
import EventPlugin from "../../../core/core/plugin/event/event.plugin";
import {
  firstImageItemCart,
  variantItemCart,
} from "../../../core/core/util/cart.util";
import CartPlugin from "../../../plugin/cart.plugin";
import { currencyMask } from "../../../utils/currencyMask";
import { objectParser } from "../../../utils/objectParser";
import { tagManager } from "../../../core/core/util/TagManager";
import { AnimatedLoading } from "../AnimatedLoading";
import styles from "./styles.module.scss";

const cartPlugin = new CartPlugin();

export const CartModal: React.FC<any> = ({ openModal, cart, user }) => {
  const [loadingCart, setLoadingCart] = useState(false);
  const [newCart, setNewCart] = useState(cart);

  async function handleDeleteProduct(product: any, variant: any) {
    const newData = {
      data: {
        product: {
          referencePath: product.referencePath,
        },
        variant: variant,
      },
    };

    const result = await cartPlugin.delItemCart(newData);

    setNewCart(result.data);

    await EventPlugin.dispatch("changeCart", result.data);

    toast.success("Produto excluído com sucesso!");
  }

  const onSubmitCheckout = async () => {
    setLoadingCart(true);

    const result = await cartPlugin.checkoutCart();

    await EventPlugin.dispatch("changeCart", null);

    if (result.status == false) {
      toast.error(result.error);
    } else {
      toast.success("Pedido sendo processado! Estamos te redirecionando...", {
        autoClose: 3000,
      });
      router.push(`/conta/pedido/${result.data.id}`);
    }

    openModal(false);

    setLoadingCart(false);
  };

  async function handleClearCart() {
    toast.success("Carrinho sendo limpo!");

    newCart.items.forEach((product) => {
      tagManager.removeFromCart(router.asPath, product, product.quantity);
    });

    await EventPlugin.dispatch("changeCart", null);

    const result = await cartPlugin.clearCart();

    setNewCart(result.data);
  }

  return (
    <div className={styles.cartModal} onClick={() => openModal(false)}>
      <div className={styles.content} onClick={(e) => e.stopPropagation()}>
        <div className={styles.top}>
          <AiOutlineCloseCircle
            onClick={() => {
              openModal(false);
            }}
            className={styles.cartCloseModal}
          />
          <p>
            Seu carrinho (
            {newCart?.items?.length > 1
              ? newCart?.items?.length + " itens"
              : newCart?.items?.length + " item"}
            )
          </p>
        </div>
        <div className={styles.cart}>
          {newCart?.items.map((item: any) => (
            <>
              <div className={styles.cartProduct} key={item.id}>
                <div className={styles.image}>
                  <img src={firstImageItemCart(item)} alt="" />
                </div>
                <div className={styles.info}>
                  <p className={styles.productName}>{item?.product?.name}</p>
                  <p className={styles.productColor}>
                    Cor: {variantItemCart(item, 0)}
                  </p>
                  <p className={styles.productSize}>
                    Tamanho: {variantItemCart(item, 1)}
                  </p>
                  <p className={styles.productQuantity}>
                    Quantidade: {item.quantity}
                  </p>
                  <p className={styles.productPrice}>
                    Preço: {currencyMask(item.total)}
                  </p>
                  {/*
                  <table className={styles.productSize}>
                    <tbody>
                      {item?.quantityTable.data &&
                        objectParser(item?.quantityTable.data).map(
                          (objectName: any, index: any) => (
                            <tr key={"sizeP-" + index}>
                              <td>{objectName}</td>
                              <td>
                                {item?.quantityTable.data[objectName].quantity}
                              </td>
                            </tr>
                          )
                        )}
                      <tr>
                        <td>Total</td>
                        <td>{item.quantity}</td>
                      </tr>
                    </tbody>
                  </table>
						  */}

                  {/* <p className={styles.productQuantity}>
                    Quantidade: <span>{item.quantity}</span>
                  </p> */}
                </div>
                <FiTrash
                  onClick={() =>
                    handleDeleteProduct(item.product, item?.variant)
                  }
                />
              </div>
              <div className={styles.line}></div>
            </>
          ))}

          <p className={styles.subtotal}>
            Subtotal: <span>{currencyMask(newCart?.totalItems || 0)}</span>
          </p>
          <div className={styles.line}></div>

          <button
            type="button"
            onClick={() => handleClearCart()}
            className={styles.actionButton}
          >
            Limpar carrinho
          </button>

          <div className={styles.line}></div>

          <button
            type="button"
            onClick={() => openModal(false)}
            className={styles.actionButton}
          >
            Continuar comprando
          </button>
          <div className={styles.line}></div>

          {cart?.items?.length > 0 && (
            <button
              type="button"
              onClick={() => {
                router.push("/checkout");
                openModal(false);
              }}
            >
              {loadingCart ? <AnimatedLoading /> : <>Finalizar a compra </>}
            </button>
          )}
        </div>
      </div>
    </div>
  );
};
