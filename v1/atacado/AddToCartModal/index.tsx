import { useRef, useState } from "react";
import { useForm } from "react-hook-form";
import {
  AiOutlineCloseCircle,
  AiOutlineLeft,
  AiOutlineRight,
} from "react-icons/ai";
import Slider from "react-slick";
import { toast } from "react-hot-toast";
import CartPlugin from "../../../plugin/cart.plugin";
import { findDuplicates } from "../../../utils/findDuplicates";
import { AnimatedLoading } from "../AnimatedLoading";
import styles from "./styles.module.scss";
import NumericInput from "react-numeric-input";
import EventPlugin from "../../../core/core/plugin/event/event.plugin";

const cartPlugin = new CartPlugin();

export function AddToCartModal({ setModal, product }: any) {
  const [animateLoading, setAnimateLoading] = useState(false);
  const [variantValue, setVariantValue] = useState(
    product?.variant[0]?.items[0]?.value
  );
  const slider = useRef<any>(null);
  const { register, handleSubmit, setValue } = useForm<any>();

  const sliderSettings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: <AiOutlineRight />,
    prevArrow: <AiOutlineLeft />,
  };

  function getImagesVariant(i: any) {
    const images = product?.images?.data[variantValue]?.images;
    return images;
  }

  const handleAddToCart = async (formData: any) => {
    setAnimateLoading(true);

    const variantColor = product?.variant[0]?.items?.filter(
      (color: any) => color.value == variantValue
    );

    const sizesQuantity = formData?.data?.filter(
      (size: any) => size != undefined
    );

    const totalQuantity = sizesQuantity.reduce(function (acc: any, value: any) {
      return acc + (Number(value[`${Object.keys(value)}`]?.quantity) || 0);
    }, 0);

    const finalQuantityTable = sizesQuantity.reduce(
      (acc: any, value: any) => ({
        ...acc,
        [`${variantColor[0].value}-${Object.keys(value)[0]}`]: {
          ...value[Object.keys(value)[0]],
        },
      }),
      {}
    );
    /* TESTE */
    const sizesTable = product?.variant[1]?.items?.filter((size: any) => {
      for (let i = 0; i < sizesQuantity.length; i++) {
        if (Object.keys(sizesQuantity[i])[0] == size.label) {
          return size;
        }
      }
    });

    if (totalQuantity == 0) {
      setAnimateLoading(false);
      return toast.error("Adicione a quantidade de um produto!");
    }

    const newData = {
      data: {
        product: {
          referencePath: product.referencePath,
        },
        quantityTable: {
			data : finalQuantityTable,
			variant: [{items:variantColor}, {items:sizesTable}],
		},
      },
    };

    const result = await cartPlugin.setItemsCart(newData);

    if (result.status == false) {
      setAnimateLoading(false);
      return toast.error(result.error, {
        autoClose: 2000,
      });
    }

    setAnimateLoading(false);

    //Clean all Inputs
    document
      .querySelectorAll(".react-numeric-input input")
      .forEach((value: any) => {
        value.setValue("");
      });

    EventPlugin.dispatch("changeCart", result.data);

    toast.success("Produto adiconado com sucesso!");
  };

  return (
    <div className={styles.addToCartModal} onClick={() => setModal(false)}>
      <div className={styles.content} onClick={(e) => e.stopPropagation()}>
        <AiOutlineCloseCircle
          onClick={() => {
            setModal(false);
          }}
          className={styles.productCloseModal}
        />
        <div className={styles.productSelect}>
          <div className={styles.productImages}>
            <Slider {...sliderSettings} ref={slider} className="productSlider">
              {getImagesVariant(0)?.map((image: any) => (
                <div key={image?.id} className="productSlide">
                  <img src={image?._url} alt={product.name} />
                </div>
              ))}
            </Slider>
          </div>
          <div className={styles.productInfo}>
            <p className={styles.productName}>{product?.name}</p>
            <form>
              <div className={styles.formItem}>
                <label>Selecione a cor:</label>
                <div className={styles.colorSelector}>
                  {product?.variant[0]?.items.map((color: any) => (
                    <span
                      onClick={() => {
                        slider.current.slickGoTo(0);
                        setVariantValue(color.value);
                      }}
                      className={
                        color.value == variantValue
                          ? `${styles.colorItem} ${styles.active}`
                          : `${styles.colorItem}`
                      }
                      key={color.id}
                    >
                      {color.label}
                    </span>
                  ))}
                </div>
              </div>
              <div className={styles.formItem}>
                <label>Insira as quantidades abaixo:</label>
                <div className={styles.formInputs}>
                  {findDuplicates(product?.variant[1]?.items)?.map(
                    (size: any, index: any) => (
                      <div className={styles.formInputItem} key={size?.id}>
                        <span>{size?.label}</span>
                        <div className={styles.divider}></div>
                        <NumericInput
                          {...register(`data.${index}`)}
                          style={{ width: "100%" }}
                          // format={(num: any) => Number(num)}
                          // strict
                          min={0}
                          onChange={(e: any) =>
                            setValue(`data.${index}`, {
                              [size?.value]: { quantity: e },
                            })
                          }
                          mobile={true}
                        />
                      </div>
                    )
                  )}
                </div>
              </div>
            </form>
            <button type="button" onClick={handleSubmit(handleAddToCart)}>
              {animateLoading ? <AnimatedLoading /> : "Adicionar ao carrinho"}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
