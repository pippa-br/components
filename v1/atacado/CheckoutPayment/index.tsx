import styles from "./styles.module.scss";
import Select from "react-select";
import { useState } from "react";
import { AiOutlineCheckCircle } from "react-icons/ai";
import { paymentMethods } from "../../../utils/paymentMethods";
import InputMask from "react-input-mask";
import { useForm } from "react-hook-form";
import { AiOutlineExclamationCircle } from "react-icons/ai";
import { buscaCep } from "../../../utils/getCEP";
import ccValid from "card-validator";
import { AnimatedLoading } from "../AnimatedLoading";
import { validateCpf } from "../../../utils/validateCpf";
import { BsCircle } from "react-icons/bs";
import CartPlugin from "../../../plugin/cart.plugin";
import router from "next/router";

const cartPlugin = new CartPlugin();

type AddressProps = {
  bairro: string;
  cep: string;
  complemento: string;
  localidade: string;
  logradouro: string;
  uf: string;
};

export const CheckoutPayment = ({
  userCart,
  loadingCart,
  setLoadingCart,
  onSubmitPayment,
  onSubmit,
  goBack,
}: any) => {
  const [address, setAddress] = useState<AddressProps>();
  const [loadingUserCreditCard, setLoadingUserCreditCard] = useState(false);
  const [selectedPaymentAddressMethod, setSelectedPaymentAddressMethod] =
    useState(true);

  const customSelectStyles: any = {
    control: (base: any, state: any) => ({
      ...base,
      width: "100%",
      height: "45px",
      borderRadius: "4px",
      borderColor: state.isFocused
        ? "var(--border-color)"
        : "var(--border-color)",
      boxShadow: state.isFocused ? "none" : "",
      "&:hover": {
        borderColor: "var(--border-color)",
      },
      color: "#000",
    }),
    singleValue: (styles: any) => ({ ...styles, color: "#000" }),
    indicatorSeparator: () => "",
  };

  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm();
  register("sameAddressShiping", { value: selectedPaymentAddressMethod });

  const [selectedPaymentMethod, setSelectedPaymentMethod] = useState(
    userCart.paymentMethod || {
      id: "",
      label: "",
      value: "",
    }
  );

  async function handleCreditCardInstallment(installment: any) {
    const newData = {
      data: installment,
    };

    setLoadingCart(true);

    const result = await cartPlugin.setCartInstallments(newData);

    setLoadingCart(false);
  }

  return (
    <div className={styles.checkoutPayment}>
      <div className={styles.shippingOption}>
        <p>
          <b>Enviar para:</b> {userCart?.address?.street},Nº{" "}
          {userCart?.address?.housenumber}, {userCart?.address?.district},{" "}
          {userCart?.address?.city}, {userCart?.address?.state}. CEP:{" "}
          {userCart?.address?.zipcode}{" "}
        </p>
      </div>
      <div className={styles.shippingOption}>
        <p>
          <b>Método:</b> {userCart?.shipping?.label}
        </p>
      </div>
      <p className={styles.title}>Forma de pagamento:</p>
      <div className={styles.paymentMethods}>
        {paymentMethods.map((payment) => (
          <p
            onClick={() => {
              onSubmitPayment(payment);
              setSelectedPaymentMethod(payment);
            }}
            className={
              payment.label == selectedPaymentMethod.label
                ? `${styles.paymentOption} ${styles.active}`
                : styles.paymentOption
            }
            key={payment.id}
          >
            <BsCircle className={styles.noCheck} />
            <AiOutlineCheckCircle className={styles.check} />
            {payment.label}
          </p>
        ))}
      </div>

      {selectedPaymentMethod.label == "Transferência" && (
        <>
          <p className={styles.title}>Selecione qual banco irá transferir:</p>
          <div className={styles.shippingMethods}>
            {paymentMethods.map(
              (payment) =>
                payment.label == "Transferência" && (
                  <p
                    onClick={() => {
                      onSubmitPayment(payment);
                      setSelectedPaymentMethod(payment);
                    }}
                    className={
                      payment.label == selectedPaymentMethod.label
                        ? `${styles.paymentOption} ${styles.active}`
                        : styles.paymentOption
                    }
                    key={payment.id}
                  >
                    <BsCircle className={styles.noCheck} />
                    <AiOutlineCheckCircle className={styles.check} />
                    {payment.bank_name} | Ag:{payment.bank_agency} | C/C:{" "}
                    {payment.bank_account}
                  </p>
                )
            )}
          </div>
        </>
      )}

      {selectedPaymentMethod.label == "Cartão de Credito" && (
        <>
          <p className={styles.title}>Preencha os dados abaixo:</p>
          <form className={styles.form} onSubmit={() => handleSubmit(onSubmit)}>
            <div className={styles.formItem}>
              <label>Nome do titular do cartão</label>
              <input
                type="text"
                autoComplete="new-off"
                placeholder="Ex: José da Silva"
                {...register("owner", {
                  required: "Este campo é obrigatório!",
                })}
              />
              {errors.owner && (
                <span className={styles.errorMessage}>
                  <AiOutlineExclamationCircle />
                  {errors.owner?.message}
                </span>
              )}
            </div>
            <div className={styles.formItem}>
              <label>CPF</label>
              <InputMask
                {...register("cpf", {
                  validate: (value) => validateCpf(value) || "CPF inválido!",
                })}
                autoComplete="new-off"
                mask="999.999.999-99"
                placeholder="Ex: 123.456.789-10"
                maskChar=""
              />
              {errors.cpf && (
                <span className={styles.errorMessage}>
                  <AiOutlineExclamationCircle />
                  {errors.cpf?.message}
                </span>
              )}
            </div>
            <div className={styles.formItem}>
              <label>Número do cartão</label>
              <InputMask
                mask="9999999999??????"
                formatChars={{
                  "9": "[0-9]",
                  "?": "[0-9]",
                }}
                autoComplete="new-off"
                maskChar=""
                placeholder="Ex: 1234567809101112"
                {...register("cardnumber", {
                  validate: (value) =>
                    ccValid.number(value.toString()).isValid === true ||
                    "Insira um número de cartão válido!",
                })}
              />
              {errors.cardnumber && (
                <span className={styles.errorMessage}>
                  <AiOutlineExclamationCircle />
                  {errors.cardnumber?.message}
                </span>
              )}
            </div>
            <div className={styles.formItem}>
              <label>Data de expiração</label>
              <InputMask
                mask="99/99"
                placeholder="Ex: 03/24"
                autoComplete="new-off"
                maskChar=""
                {...register("expirydate", {
                  validate: (value) =>
                    value.length >= 5 ||
                    "Sua data de expiração deve ser do tipo DD/AA",
                })}
              />
              {errors.expirydate && (
                <span className={styles.errorMessage}>
                  <AiOutlineExclamationCircle />
                  {errors.expirydate?.message}
                </span>
              )}
            </div>
            <div className={styles.formItem}>
              <label>CVV</label>
              <InputMask
                mask="999?"
                placeholder="Ex: 123"
                formatChars={{
                  "9": "[0-9]",
                  "?": "[0-9]",
                }}
                autoComplete="new-off"
                maskChar=""
                {...register("cvv", {
                  validate: (value) =>
                    value.length >= 3 ||
                    "Seu CVV tem que possuir no mínimo 3 números",
                })}
              />
              {errors.cvv && (
                <span className={styles.errorMessage}>
                  <AiOutlineExclamationCircle />
                  {errors.cvv?.message}
                </span>
              )}
            </div>
            <div className={styles.selectOption}>
              <label>Parcelas</label>
              <Select
                placeholder="Quantidade de parcelas"
                options={userCart.installments}
                styles={customSelectStyles}
                isSearchable={false}
                defaultValue={userCart?.installment || null}
                onChange={handleCreditCardInstallment}
              />
            </div>

            <p className={styles.formItemTitle}>Endereço de cobrança:</p>
            <p
              onClick={() => {
                setSelectedPaymentAddressMethod(true);
              }}
              className={
                selectedPaymentAddressMethod
                  ? `${styles.paymentOption} ${styles.active}`
                  : styles.paymentOption
              }
            >
              <BsCircle className={styles.noCheck} />
              <AiOutlineCheckCircle className={styles.check} />
              Usar o endereço de entrega
            </p>
            <p
              onClick={() => {
                setSelectedPaymentAddressMethod(false);
              }}
              className={
                !selectedPaymentAddressMethod
                  ? `${styles.paymentOption} ${styles.active}`
                  : styles.paymentOption
              }
            >
              <BsCircle className={styles.noCheck} />
              <AiOutlineCheckCircle className={styles.check} />
              Usar um endereço diferente
            </p>

            {!selectedPaymentAddressMethod && (
              <>
                <div className={styles.formItem}>
                  <label>CEP</label>
                  <InputMask
                    mask="99999-999"
                    placeholder="Ex: 12345-678"
                    autoComplete="new-off"
                    maskChar=""
                    onKeyUp={(e: any) =>
                      e.target.value.length === 9 &&
                      buscaCep(e.target.value, setAddress, setValue)
                    }
                    {...register("cep", {
                      validate: (value) =>
                        value.length >= 9 || "Seu CEP deve possuir 8 números",
                    })}
                  />

                  {errors.cep && (
                    <span className={styles.errorMessage}>
                      <AiOutlineExclamationCircle />
                      {errors.cep?.message}
                    </span>
                  )}
                </div>
                <div className={styles.formItem}>
                  <label>Rua</label>
                  <input
                    defaultValue={address?.logradouro}
                    type="text"
                    placeholder="Ex: Rua Guaranabara"
                    autoComplete="new-off"
                    {...register("street", {
                      required: "Este campo é obrigatório!",
                    })}
                  />
                  {errors.street && (
                    <span className={styles.errorMessage}>
                      <AiOutlineExclamationCircle />
                      {errors.street?.message}
                    </span>
                  )}
                </div>
                <div className={styles.formItem}>
                  <label>Número</label>
                  <input
                    type="text"
                    autoComplete="new-off"
                    placeholder="Ex: 17"
                    {...register("housenumber", {
                      required: "Este campo é obrigatório!",
                    })}
                  />
                  {errors.housenumber && (
                    <span className={styles.errorMessage}>
                      <AiOutlineExclamationCircle />
                      {errors.housenumber?.message}
                    </span>
                  )}
                </div>
                <div className={styles.formItem}>
                  <label>Complemento</label>
                  <input
                    type="text"
                    autoComplete="new-off"
                    placeholder="Ex: Ap 15B"
                    {...register("complement", {})}
                  />
                  {errors.complement && (
                    <span className={styles.errorMessage}>
                      <AiOutlineExclamationCircle />
                      {errors.complement?.message}
                    </span>
                  )}
                </div>
                <div className={styles.formItem}>
                  <label>Bairro</label>
                  <input
                    defaultValue={address?.bairro}
                    type="text"
                    autoComplete="new-off"
                    placeholder="Ex: Dom Bosco"
                    {...register("district", {
                      required: "Este campo é obrigatório!",
                    })}
                  />
                  {errors.district && (
                    <span className={styles.errorMessage}>
                      <AiOutlineExclamationCircle />
                      {errors.district?.message}
                    </span>
                  )}
                </div>
                <div className={styles.formItem}>
                  <label>Cidade</label>
                  <input
                    defaultValue={address?.localidade}
                    type="text"
                    placeholder="Ex: São Paulo"
                    autoComplete="new-off"
                    {...register("city", {
                      required: "Este campo é obrigatório!",
                    })}
                  />
                  {errors.city && (
                    <span className={styles.errorMessage}>
                      <AiOutlineExclamationCircle />
                      {errors.city?.message}
                    </span>
                  )}
                </div>
                <div className={styles.formItem}>
                  <label>Estado</label>
                  <input
                    defaultValue={address?.uf}
                    type="text"
                    placeholder="Ex: São Paulo"
                    autoComplete="new-off"
                    {...register("state", {
                      required: "Este campo é obrigatório!",
                    })}
                  />
                  {errors.state && (
                    <span className={styles.errorMessage}>
                      <AiOutlineExclamationCircle />
                      {errors.state?.message}
                    </span>
                  )}
                </div>{" "}
              </>
            )}
          </form>
        </>
      )}
      <div className={styles.buttons}>
        {!loadingCart && (
          <button
            className={styles.cinza}
            type="button"
            onClick={() => router.push("/")}
          >
            Voltar a loja
          </button>
        )}
        {loadingCart ? null : (
          <button
            className={styles.cinza}
            type="button"
            onClick={() => goBack()}
          >
            Voltar
          </button>
        )}
        <button
          disabled={loadingCart && true}
          type="button"
          onClick={
            selectedPaymentMethod?.value == "credit_card"
              ? handleSubmit(onSubmit)
              : () => onSubmit(selectedPaymentMethod)
          }
        >
          {loadingCart ? <AnimatedLoading /> : "Próximo"}
        </button>
      </div>
    </div>
  );
};
