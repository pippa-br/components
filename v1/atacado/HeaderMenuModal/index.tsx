import router from "next/router";
import { AiOutlineCloseCircle } from "react-icons/ai";
import AuthPlugin from "../../../plugin/auth.plugin";
import {AiFillHome} from "react-icons/ai"

import styles from "./styles.module.scss";

const authPlugin = new AuthPlugin();

export const HeaderMenuModal = ({ setModal }: any) => {
  async function handleLogout() {
    await authPlugin.logoutFront();
    router.push("/login");
  }

  return (
    <div className={styles.menuModal} onClick={() => setModal(false)}>
      <div className={styles.content} onClick={(e) => e.stopPropagation()}>
        <AiOutlineCloseCircle onClick={() => setModal(false)} />
        <li
          onClick={() => {
            router.push("/");
            setModal(false);
          }}
        >
          Home
        </li>
        <li
          onClick={() => {
            router.push("/");
            setModal(false);
          }}
        >
          Produtos
        </li>

        <li
          onClick={() => {
            router.push("/devolucoes");
            setModal(false);
          }}
        >
          Devoluções
        </li>
        <li
          onClick={() => {
            router.push("/politica-de-privacidade");
            setModal(false);
          }}
        >
          Política de privacidade
        </li>
        <li
          onClick={() => {
            router.push("/meus-dados");
            setModal(false);
          }}
        >
          Meus dados
        </li>
        <li
          onClick={() => {
            router.push("/meus-pedidos");
            setModal(false);
          }}
        >
          Meus pedidos
        </li>
        <li
          onClick={() => {
            handleLogout();
            setModal(false);
          }}
        >
          Logout
        </li>
      </div>
    </div>
  );
};
