import styles from "./styles.module.scss";
import { BiErrorCircle } from "react-icons/bi";

function ErrorMessage({ message }: any) {
  return (
    <div className={styles.errorMessage}>
      <BiErrorCircle />
      {message}
    </div>
  );
}

export default ErrorMessage;
