import styles from "./styles.module.scss";
import React, { useState } from "react";
import { BiCartAlt } from "react-icons/bi";
import { currencyMask } from "../../../utils/currencyMask";
import { AddToCartModal } from "../AddToCartModal";
import { findDuplicates } from "../../../utils/findDuplicates";
import { ImageSet } from "../ImageSet";

export const ProductItem: React.FC<any> = ({ product, productInfo = true }) => {
  const [modal, setModal] = useState(false);

  return (
    <>
      {product?.images?.data?.[product?.variant[0]?.items[0]?.value]?.images[0]
        ?._url ||
      product?.images?.data?.[
        product?.variant[0]?.items[0]?.value.toLowerCase()
      ]?.images[0]?._url ? (
        <div className={styles.productItem}>
          <div className={styles.productImage} onClick={() => setModal(true)}>
            <ImageSet
              image={
                product?.images?.data?.[product?.variant[0]?.items[0]?.value]
                  ?.images[0] ||
                product?.images?.data?.[
                  product?.variant[0]?.items[0]?.value.toLowerCase()
                ]?.images[0]
              }
            />
            {/* <img
              src={
                product?.images?.data?.[product?.variant[0]?.items[0]?.value]
                  ?.images[0]?._url ||
                product?.images?.data?.[
                  product?.variant[0]?.items[0]?.value.toLowerCase()
                ]?.images[0]?._url
              }
              alt=""
            /> */}
          </div>
          <p className={styles.productName}>{product?.name}</p>
          <p className={styles.productSKU}>Referência: {product?.code}</p>
          {productInfo ? (
            <>
              <div className={styles.productSize}>
                {findDuplicates(product?.variant[1]?.items)?.map(
                  (size: any) => (
                    <p key={size.id}>{size?.label}</p>
                  )
                )}
              </div>
              <div className={styles.productColor}>
                {product?.variant[0]?.items?.map((color: any) => (
                  <p key={color.id}>{color.label}</p>
                ))}
              </div>
            </>
          ) : null}

          <p className={styles.productPrice}>
            {product?.price
              ? currencyMask(product?.price)
              : currencyMask(product?.priceTable?.data?._default?.price)}
          </p>
          <button type="button" onClick={() => setModal(true)}>
            <BiCartAlt /> Adicionar ao carrinho
          </button>
        </div>
      ) : null}

      {modal && <AddToCartModal setModal={setModal} product={product} />}
    </>
  );
};
