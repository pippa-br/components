import { ImageSetProps } from "../../../interfaces/ImageSet.interface";

export const ImageSet = ({ image }: ImageSetProps) => {
  return (
    <img
      srcSet={`${image?._150x150} 320w, ${image?._300x300} 1140w, ${image?._1024x1024} 1500w`}
      src={image?._url}
      alt={image?.name}
    />
  );
};
