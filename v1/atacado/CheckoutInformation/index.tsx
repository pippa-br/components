import styles from "./styles.module.scss";
import { useRouter } from "next/router";
import InputMask from "react-input-mask";
import { useForm } from "react-hook-form";
import { AiOutlineExclamationCircle } from "react-icons/ai";
import { useState } from "react";
import { buscaCep } from "../../../utils/getCEP";
import { toast } from "react-hot-toast";
import { validateCpf } from "../../../utils/validateCpf";
import AuthPlugin from "../../../plugin/auth.plugin";
import CartPlugin from "../../../plugin/cart.plugin";
import { AnimatedLoading } from "../AnimatedLoading";

const authPlugin = new AuthPlugin();
const cartPlugin = new CartPlugin();

export const CheckoutInformation = ({
  userData,
  onSubmit,
  cartAddress,
  loadingCart,
}: any) => {
  const [address, setAddress] = useState();
  const [newUserData, setNewUserData] = useState(userData);
  const [loadingUserAddress, setLoadingUserAddress] = useState(false);
  const router = useRouter();

  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm();

  async function handleLogout() {
    await cartPlugin.clearCart();
    await authPlugin.logoutFront();
    router.push("/");
    toast.success("Logout feito com sucesso!");
  }

  const onSubmitCartUser = async (data: any) => {
    if (!validateCpf(data.cpf)) {
      return toast.error("CPF inválido!");
    }

    const newData = {
      document: {
        referencePath: userData.referencePath,
      },
      data: data,
    };

    const result = await authPlugin.setUserFront(newData);

    setNewUserData(result);
  };

  return (
    <div className={styles.checkoutInformation}>
      <p className={styles.title}>Endereço de entrega</p>
      <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
        <div className={styles.formItem}>
          <label>CEP</label>
          <InputMask
            mask="99999-999"
            placeholder="Ex: 12345-678"
            autoComplete="new-password"
            defaultValue={userData?.address?.zipcode}
            maskChar=""
            onKeyUp={(e: any) =>
              e.target.value.length === 9 &&
              buscaCep(e.target.value, setAddress, setValue)
            }
            {...register("cep", { required: "Este campo é obrigatório!" })}
          />
          {errors.cep && (
            <span className={styles.errorMessage}>
              <AiOutlineExclamationCircle />
              {errors.cep?.message}
            </span>
          )}
        </div>
        <div className={styles.formItem}>
          <label>Rua</label>
          <input
            type="text"
            placeholder="Ex: Rua Guaranabara"
            autoComplete="new-password"
            defaultValue={userData?.address?.street}
            {...register("street", { required: "Este campo é obrigatório!" })}
          />
          {errors.street && (
            <span className={styles.errorMessage}>
              <AiOutlineExclamationCircle />
              {errors.street?.message}
            </span>
          )}
        </div>
        <div className={styles.formItem}>
          <label>Número</label>
          <input
            type="text"
            defaultValue={userData?.address?.housenumber}
            autoComplete="new-password"
            placeholder="Ex: 17"
            {...register("housenumber", {
              required: "Este campo é obrigatório!",
            })}
          />
          {errors.housenumber && (
            <span className={styles.errorMessage}>
              <AiOutlineExclamationCircle />
              {errors.housenumber?.message}
            </span>
          )}
        </div>
        <div className={styles.formItem}>
          <label>Complemento</label>
          <input
            type="text"
            autoComplete="new-password"
            defaultValue={userData?.address?.complement}
            placeholder="Ex: Ap 15B"
            {...register("complement")}
          />
          {errors.complement && (
            <span className={styles.errorMessage}>
              <AiOutlineExclamationCircle />
              {errors.complement?.message}
            </span>
          )}
        </div>
        <div className={styles.formItem}>
          <label>Bairro</label>
          <input
            type="text"
            autoComplete="new-password"
            defaultValue={userData?.address?.district}
            placeholder="Ex: Dom Bosco"
            {...register("district", {
              required: "Este campo é obrigatório!",
            })}
          />
          {errors.district && (
            <span className={styles.errorMessage}>
              <AiOutlineExclamationCircle />
              {errors.district?.message}
            </span>
          )}
        </div>
        <div className={styles.formItem}>
          <label>Cidade</label>
          <input
            type="text"
            placeholder="Ex: São Paulo"
            defaultValue={userData?.address?.city}
            autoComplete="new-password"
            {...register("city", { required: "Este campo é obrigatório!" })}
          />
          {errors.city && (
            <span className={styles.errorMessage}>
              <AiOutlineExclamationCircle />
              {errors.city?.message}
            </span>
          )}
        </div>
        <div className={styles.formItem}>
          <label>Estado</label>
          <input
            type="text"
            autoComplete="new-password"
            defaultValue={userData?.address?.state}
            placeholder="Ex: São Paulo"
            {...register("state", { required: "Este campo é obrigatório!" })}
          />
          {errors.state && (
            <span className={styles.errorMessage}>
              <AiOutlineExclamationCircle />
              {errors.state?.message}
            </span>
          )}
        </div>
        <div className={styles.buttons}>
          {!loadingCart && (
            <button
              className={styles.cinza}
              type="button"
              onClick={() => router.push("/")}
            >
              Voltar a loja
            </button>
          )}
          <button
            className={styles.submitPerfilButton}
            type="button"
            disabled={loadingCart && true}
            onClick={handleSubmit(onSubmit)}
          >
            {loadingCart ? <AnimatedLoading /> : "Próximo"}
          </button>
        </div>
      </form>
    </div>
  );
};
